package controller
{
	import acrobat.collection.IAttachment;
	import acrobat.collection.INavigatorHost;
	
	import mx.collections.ArrayCollection;
	
	import utils.LogUtil;

	public class BreadCrumbsData extends ArrayCollection
	{
		public function set host(val:INavigatorHost):void {
			_host = val;
		}
		
		private var _host:INavigatorHost = null;
		
		public function BreadCrumbsData(source:Array=null) {
			super(source);
		}
		
		public function set currentDirectory(val:IAttachment):void {
			var rawData:Array = parentChain(val);
			var cookedData:Array = rawData.map(makeEntry);
			source = cookedData;			
		}
		
		private function parentChain(val:IAttachment):Array {
			var rval:Array = [];
			do {
				rval.unshift(val);
				if(val == null) break; 
				val = val.parent;
			} while (true);
			return rval;
		}
		
		private function makeEntry(val:IAttachment, a:*, b:*):Object {
			var label:String = val ? val.fileName : _host.collection.getLocalizedString("home", "HOME");
			var rval:Object = { label: label, folder:val };
			return rval;
		}
	}
}