package controller
{
	import acrobat.collection.IAttachment;
	import acrobat.collection.INavigatorHost;
	
	import flash.events.EventDispatcher;
	
	import mx.binding.utils.BindingUtils;
	import mx.collections.IList;
	import mx.events.CollectionEvent;
	
	import utils.BindingSupport;
	import utils.LogUtil;
	import utils.DataUtils;

	/**
	 *     maintains the navigator's sense of 'selection'
	 * 
	 *  synchronizes selection between navigator and host
	 *  (this must also get 'next' and 'prev' and send to host)
	 * 
	 *  when host changes selection, propogate out
	 *   (including changing controller.currentFolder if needed)
	 * 
	 *  when controller.currentItems changes, adjust selected items
	 * 
	 * 
	 */
	public class SelectionManager extends EventDispatcher
	{
		private var _bindingSupport:BindingSupport = new BindingSupport();
		
		public function SelectionManager() {
			super();
			
			BindingUtils.bindSetter(currentItemsChanged, this, ["mainController", "currentItems"]);
		}
		
		[Bindable]
		public function get mainController():Controller {return _mainController;}
		public function set mainController(value:Controller):void {_mainController = value;}
		private var _mainController:Controller;
		
		[Bindable]
		public function get host():INavigatorHost { return _host; }
		public function set host(value:INavigatorHost):void {
			if (_host != null) {
				_bindingSupport.dropChangeWatchers();
			} 
			_host = value;
			if (_host != null) {
				_bindingSupport.bindSetter(host_selectedItems, this, "host.selection");
			} else {
				// clear all the values for the watchers
				selectedItems = [];
			} 
		}
		private var _host:INavigatorHost;
		
		private function host_selectedItems(val:Array):void {
			selectedItems = val;
			if(_mainController && _mainController.mainNavigator) 
				_mainController.mainNavigator.selectedItems = val;
		}
		
		[Bindable]
		public function get selectedItem():IAttachment {return _selectedItem;}
		private function set selectedItem(value:IAttachment):void {
			_selectedItem = value;
			if (_selectedItem != null && mainController != null && _selectedItem.parent != null)
				mainController.currentFolder = _selectedItem.parent;
		}
		private var _selectedItem:IAttachment;		

		private var _selectedItems:Array = [];

		// need this to be named in case someone tries to pass in
		// the same array with different items in it...
		[Bindable ('selectedItemsChanged')]
		public function get selectedItems():Array {
			// slice here so that no-one can mess with it 
			return _selectedItems.slice();
		}
		
		public function set selectedItems(val:Array):void {
			// always deal with real arrays...
			if (val == null)
				val = [];
			// same items and order?
			if (DataUtils.arraysHaveSameItemsInOrder(val, _selectedItems)) 
				return;						

			_selectedItems = val.slice();

			// upate selected item
			selectedItem = _selectedItems.length > 0 ? _selectedItems[0] as IAttachment : null;				

			if (mainController.host) {
				mainController.host.selection = _selectedItems.slice();
				mainController.host.setNextPrevious(next, previous);
			}
			
			dispatchEvent(new Event("selectedItemsChanged"));
		}
		
		// only update the next/prev
		private function setHostNextPrev():void {
			if (mainController == null || mainController.host == null)
				return;
			mainController.host.setNextPrevious(next, previous);
		}
		
		// returns the inex of the selected item in the current items list
		private function get selectedIndex():int { 
			// check for error conditions
			if (_currentItems == null || selectedItem == null)
				return -1;
			return _currentItems.getItemIndex(selectedItem);
		}
		
		// returns the attachment that should be displayed when the user
		// clicks previous - just the previous in the sorted/filtered items list
		// that is not a folder
		private function get previous():IAttachment {
			// check for error conditions 
			if (_currentItems == null || selectedIndex == -1)
				return null;
			var desiredIndex:int = selectedIndex -1;
			while (desiredIndex >= 0) {
				var attachment:IAttachment = _currentItems.getItemAt(desiredIndex) as IAttachment; 
				if (!attachment.isFolder)
					return attachment;
				desiredIndex -= 1;
			}
			return null;
		}

		// returns the attachment that should be displayed when the user
		// clicks next - just the next in the sorted/filtered items list
		// that is not a folder
		private function get next():IAttachment {
			// check for error conditions 
			if (_currentItems == null || selectedIndex == -1)
				return null;
			var desiredIndex:int = selectedIndex +1;
			while (desiredIndex < _currentItems.length) {
				var attachment:IAttachment = _currentItems.getItemAt(desiredIndex) as IAttachment; 
				if (!attachment.isFolder)
					return attachment;
				desiredIndex += 1;
			}
			return null;
		}
		
		private var _currentItems:IList;

		// current items list changed
		private function currentItemsChanged(val:IList):void {
			if (val == _currentItems) 
				return;
			if (_currentItems)
				_currentItems.removeEventListener(CollectionEvent.COLLECTION_CHANGE, collectionChanged);
			_currentItems = val;
			if (_currentItems)
				_currentItems.addEventListener(CollectionEvent.COLLECTION_CHANGE, collectionChanged);
			// call once now as well...
			collectionChanged();				
		}
		
		// current items items changed
		private function collectionChanged(event:CollectionEvent = null):void {
			if (_currentItems && _selectedItems && _selectedItems.length > 0)
				selectedItems = DataUtils.trimToExisting(_currentItems, _selectedItems);
		}
	}
}