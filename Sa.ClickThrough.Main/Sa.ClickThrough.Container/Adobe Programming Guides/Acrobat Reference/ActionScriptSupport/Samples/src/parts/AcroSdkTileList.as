package parts
{	
	import flash.events.Event;
	import mx.events.ResizeEvent;
	import mx.controls.TileList;
	import mx.core.IFactory;
	import mx.binding.utils.BindingUtils;
	
	import utils.LogUtil;
	import utils.DataUtils;
	import controller.Controller;
	import controller.SelectionManager;
	
	public class AcroSdkTileList extends TileList
	{	
		private var _controller:Controller = Controller.instance;
		
		public function AcroSdkTileList() {
			super();
			BindingUtils.bindProperty(_controller.selectionManager, "selectedItems", this, "selectedItems");
		}
		
		public override function set itemRenderer(value:IFactory):void {
			super.itemRenderer = value;
		}
		
		override public function set selectedItems(items:Array):void {
			if(DataUtils.arraysHaveSameItemsInOrder(items, super.selectedItems)) 
				return;
			else if ((!collection || collection.length == 0) && (!items || items.length == 0)) {
				// There is a bug in ListBase where the _selectedItems is
				// never reset if an empty array is set while there is nothing
				// in the collection.  We treat this as a no-op.
				return;
			}
			else 
				super.selectedItems = items;
		}
	}
}