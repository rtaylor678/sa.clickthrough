package utils
{
	import mx.binding.utils.BindingUtils;
	import mx.binding.utils.ChangeWatcher;
	
	import utils.LogUtil;
	
	public class BindingSupport
	{
		// utility functions
		public function bindProperty(target:Object, prop:String, source:Object, chain:String, commitOnly:Boolean = false):ChangeWatcher {
			try {
				var rval:ChangeWatcher = BindingUtils.bindProperty(target,
					prop, source, chain.split('.'), commitOnly);
				_changeWatchers.push(rval);
				return rval;
			} catch (e:Error) {
				LogUtil.logger.error("{0}",e.getStackTrace());				
			}
			return null;
		}
		
		public function bindSetter(setter:Function, source:Object, chain:String, commitOnly:Boolean = false):ChangeWatcher {
			var rval:ChangeWatcher = BindingUtils.bindSetter(setter,source,chain.split('.'),commitOnly);
			_changeWatchers.push(rval);
			return rval;
		}
		
		private var _changeWatchers:Array = [];
		public function dropChangeWatchers():void {
			_changeWatchers.forEach(function(cw:ChangeWatcher, a:*, b:*):void {
				if(cw) {
					cw.unwatch();
					cw.setHandler(null);
					cw.reset(null);
				}
			});
			_changeWatchers = [];
		}
	}
}