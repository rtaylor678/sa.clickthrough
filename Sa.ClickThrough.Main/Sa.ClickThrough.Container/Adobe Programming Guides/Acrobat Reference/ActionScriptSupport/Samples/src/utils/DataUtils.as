package utils
{
	import flash.utils.ByteArray;
	
	import mx.collections.IList;
	
	/* class to hold your reusable (public static) data manipulation functions */
	public class DataUtils
	{	
		public static function trimToExisting(list:IList, items:Array):Array {
			var filterFunctor:Function = function (item:*,a:*,b:*):Boolean {
				return list.getItemIndex(item) != -1;
			};
			var rval:Array = items.filter(filterFunctor);
			return rval;
		}
		
		// is this the same set of items? order considered
		public static function arraysHaveSameItemsInOrder(a1:Array, a2:Array):Boolean {
			if (a1.length != a2.length)
				return false;
			for (var i:Number = 0; i < a1.length; i++)
				if (a1[i] != a2[i])
					return false;
			return true;
		}
	}
}