// ReadCADFileVC.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "ReadCADFileVC.h"

#include <acrobat.h>
#include <iac.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CReadCADFileVC

BEGIN_MESSAGE_MAP(CReadCADFileVC, CWinApp)

END_MESSAGE_MAP()


// CReadCADFileVC construction

CReadCADFileVC::CReadCADFileVC()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CReadCADFileVC object

CReadCADFileVC theApp;


// CReadCADFileVC initialization

BOOL CReadCADFileVC::InitInstance()
{
	CWinApp::InitInstance();

	// === Initialize OLE libraries. 
	if (!AfxOleInit())	{
		AfxMessageBox(TEXT("OLE initialization failed."));
		return FALSE;
	}

	CString strFilter = TEXT("3DXML (*.3dxml)|*.3dxml|ACIS (*.sat; *.sab)|*.sat; *.sab|\
						Autodesk Inventor (*.ipt; *.iam)|*.ipt; *.iam|\
						CADDS (*.pd; *._pd; *.cadds)|*.pd; *._pd; *.cadds|\
						CATIA V4 (*.model; *.dlv; *.exp; *.session)|*.model; *.dlv; *.exp; *.session|\
						CATIA V5 (*.catproduct; *.catpart)|*.catproduct; *.catpart|\
						CGR (*.cgr)|*.cgr|\
						I-DEAS (*.mf1; *.arc; *.unv; *.pkg)|*.mf1; *.arc; *.unv; *.pkg|\
						IGES (*.igs; *.iges)|*.igs; *.iges|\
						JTOpen (*.jt)|*.jt|\
						Lattice XVL (*.xv3; *.xv0)|*.xv3; *xv0|\
						OneSpace Designer (*.pkg; *.sdp; *.sdpc; *.sdw; *.sda; *.sds; *.ses; \
						*.bdl)|*.pkg; *.sdp; *.sdpc; *.sdw; *.sda; *.sds; *.ses; *.bdl|\
						Parasolid (*.x_t; *.x_b)|*.x_t; *.x_b|\
						PRC (*.prc; *.prd)|*.prc; *.prd|\
						Pro/ENGINEER (*.prt; *.xpr; *.asm; *.xas; *.neu)|*.prt; *.xpr; *.asm; *.xas; *.neu|\
						SolidWorks (*.sldasm; *.sldprt)|*.sldasm; *.sldprt|\
						STEP Exchange (*.stp; *.step)|*.stp; *.step|\
						Stereo Lithography (*.stl)|*.stl|\
						UGS NX (*.prt)|*.prt|\
						VRML(*.wrl; *.vrml)|*.wrl; *.vrml||");
	CFileDialog fileDlg(TRUE, NULL, NULL, OFN_OVERWRITEPROMPT, strFilter);
	fileDlg.GetOFN().lpstrTitle = TEXT("Select a native CAD file to convert");

	if(fileDlg.DoModal() == IDOK){
		CString strPathName = fileDlg.GetPathName();
		CString strFileName = fileDlg.GetFileName();
		CString strFileTitle = fileDlg.GetFileTitle();
		CString strWindowTitle = strFileTitle + TEXT(".pdf");
		CString strPdfFileName = strPathName.Left(strPathName.ReverseFind(TEXT('.'))) + TEXT(".pdf");

		COleException e;

		CAcroApp * pAcroApp = new CAcroApp();
		pAcroApp->CreateDispatch(TEXT("AcroExch.App"), &e);

		CAcroAVDoc * pAcroAvDoc = new CAcroAVDoc();
		pAcroAvDoc->CreateDispatch(TEXT("AcroExch.AVDoc"), &e);

		if(!pAcroAvDoc->Open(strPathName, strWindowTitle)) {
			CString strMsg = TEXT("Failed to open file ");
			strMsg += strFileName;
			strMsg += TEXT("!");
			AfxMessageBox(strMsg);

			delete pAcroAvDoc;

			pAcroApp->Exit();
			delete pAcroApp;
			return FALSE;
		}

		CAcroPDDoc acroPdDoc(pAcroAvDoc->GetPDDoc());
		acroPdDoc.Save(PDSaveFull, strPdfFileName);

		pAcroAvDoc->Close(1);
		delete pAcroAvDoc;
		pAcroApp->CloseAllDocs();
		pAcroApp->Exit();
		delete pAcroApp;

		CString strMsg = TEXT("CAD file \"");
		strMsg += strFileName;
		strMsg += TEXT("\" was converted to PDF successfully and saved as \"");
		strMsg += strWindowTitle;
		strMsg += TEXT("\" in the same directory.");
		AfxMessageBox(strMsg, MB_ICONINFORMATION);
	} 


	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
