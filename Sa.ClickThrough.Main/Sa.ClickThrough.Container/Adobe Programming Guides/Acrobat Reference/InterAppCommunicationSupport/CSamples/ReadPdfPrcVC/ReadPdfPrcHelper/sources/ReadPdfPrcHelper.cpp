/*********************************************************************

ADOBE SYSTEMS INCORPORATED
Copyright(C) 1998-2008 Adobe Systems Incorporated
All rights reserved.

NOTICE: Adobe permits you to use,modify,and distribute this file
in accordance with the terms of the Adobe license agreement
accompanying it. If you have received this file from a source other
than Adobe,then your use,modification,or distribution of it
requires the prior written permission of Adobe.

*********************************************************************/

// Acrobat Headers.
#ifndef MAC_PLATFORM
#include <PIHeaders.h>
#endif

#include <tinyxml.h>
#include <math.h>

#include <A3DSDK.h>
#include <A3DSDKTypes.h>
#include <A3DSDKBase.h>
#include <A3DSDKErrorCodes.h>

//////////////////////////////////////////////////////////////////////////
//
//	Constants/Declarations
//
//////////////////////////////////////////////////////////////////////////
extern "C" HINSTANCE gHINSTANCE;
#define BUF_LEN 1024

static const char* MyPluginExtensionName = "ADBE:ReadPdfPrcHelper";
static HWND msgHWnd = NULL;

A3DVoid doTheTest(A3DUTF8Char acFileName[], wchar_t* xmlFileName);

//////////////////////////////////////////////////////////////////////////
//
//	Dumping PRC in a physical file
//
//////////////////////////////////////////////////////////////////////////

A3DVoid saveStream(CosObj stream, const A3DUTF8Char* prcName)
{
	ASTCount iCount = 0;

	FILE* pf = fopen(prcName, "wb");
	if (pf) {
		ASStm stm = CosStreamOpenStm(stream, cosOpenFiltered);
		char sBuffer[BUF_LEN];

		do {
			iCount = ASStmRead(sBuffer, sizeof(char), BUF_LEN, stm);
			fwrite(sBuffer, sizeof(char), iCount, pf);
		} while (iCount == BUF_LEN);

		ASStmClose(stm);
		fclose(pf);
	}
}


void ExportFromPRC(wchar_t *pstrPathW)
{
	ASText asTextPath = ASTextFromUnicode(reinterpret_cast <ASUTF16Val *>(pstrPathW), 
		kUTF16HostEndian);

	ASPathName asPathName = ASFileSysCreatePathName(ASGetDefaultUnicodeFileSys(),
		ASAtomFromString("ASTextPath"), asTextPath, NULL);
	ASFile asFile = NULL;
	ASInt32 iRet = ASFileSysOpenFile(ASGetDefaultUnicodeFileSys(), asPathName, ASFILE_READ, &asFile);
	if(asFile != NULL && iRet == 0) {	
		PDDoc pdDoc = PDDocOpenFromASFile(asFile, NULL, true);

		PDPage pdPage = PDDocAcquirePage(pdDoc, 0);

		ASUns32 numAnnots = PDPageGetNumAnnots(pdPage);

		wchar_t *pstrDupW = _wcsdup(pstrPathW);
		wchar_t *pPos = wcsstr(pstrDupW, L".pdf");
		int iPos = (int)(pPos - pstrDupW + 1);
		wcscpy(pstrDupW + iPos, L"xml");

		bool bFound = false;

		for (ASUns32 ui = 0; ui < numAnnots; ui++) {
			PDAnnot pdAnnot = PDPageGetAnnot(pdPage, ui);
			if (PDAnnotIsValid(pdAnnot) && PDAnnotGetSubtype(pdAnnot) == ASAtomFromString("3D")) {
				CosObj objAnnot = PDAnnotGetCosObj(pdAnnot);
				CosObj obj3DD = CosDictGet(objAnnot, ASAtomFromString("3DD"));
				if (CosObjGetType(obj3DD) == CosStream) {
					CosObj obj = CosDictGet(obj3DD, ASAtomFromString("Subtype"));
					if (CosNameValue(obj) == ASAtomFromString("PRC")) {
						A3DUTF8Char acbuffer[A3D_MAX_BUFFER];
						sprintf(acbuffer, "C:\\%sprc", tmpnam(NULL));
						saveStream(obj3DD, acbuffer);
						bFound = true;
						doTheTest(acbuffer, pstrDupW);

						free(pstrDupW);
						_unlink(acbuffer);

						break;
					}
				}
			}
		}
		PDPageRelease(pdPage);
		PDDocClose(pdDoc);
		ASFileClose(asFile);

		if(!bFound) {
			TiXmlDocument *doc = new TiXmlDocument();
			doc->SetCondenseWhiteSpace(false);

			TiXmlComment* comment = new TiXmlComment();
			comment->SetValue("There is no PRC annotation in the selected PDF file!");
			doc->LinkEndChild(comment);

			char* tmpFileNameA = "C:\\trace.xml";
			wchar_t* tmpFileNameW = L"C:\\trace.xml";
			doc->SaveFile(tmpFileNameA);
			char strPathNameA[BUF_LEN];
			memset(strPathNameA, 0x00, BUF_LEN);

			WideCharToMultiByte(CP_UTF8, 0, pstrDupW, -1, 
								strPathNameA, BUF_LEN, NULL, NULL);
			MoveFileW(tmpFileNameW, pstrDupW);
			delete doc;
		}
	}
	ASFileSysReleasePath(ASGetDefaultUnicodeFileSys(), asPathName);
	ASTextDestroy(asTextPath);
}

/*-------------------------------------------------------
	WndProc Message Handlers
-------------------------------------------------------*/

/**
	WndProc for our message target. External applications
	can send us requests by sending the appropriate
	windows message.

	The communication is strictly one-way at the moment.
	If something goes awry, we fail quietly.
	@return the default window handling procedure
*/
LRESULT CALLBACK IACWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if(msg == WM_COPYDATA){
		PCOPYDATASTRUCT pCopyData;
		pCopyData = (COPYDATASTRUCT*)lParam;

		char strPathA[1024];
		memset(strPathA, 0x00, 1024);
		ASInt32 pathLen = (ASInt32)pCopyData->cbData;
		strncpy(strPathA, (char *)pCopyData->lpData, pathLen);

		int nLen = MultiByteToWideChar(CP_UTF8, 0, strPathA, -1, NULL, NULL);
		LPWSTR pstrPathW = new WCHAR[nLen];
		MultiByteToWideChar(CP_UTF8, 0, strPathA, -1, pstrPathW, nLen);

		ExportFromPRC(pstrPathW);

		delete []pstrPathW;
	}

    return DefWindowProc (hwnd, msg, wParam, lParam);
}

//////////////////////////////////////////////////////////////////////////
//
//	Core Handshake Callbacks
//
//////////////////////////////////////////////////////////////////////////

ACCB1 ASBool ACCB2 PluginExportHFTs()
{
	return true;
}

ACCB1 ASBool ACCB2 PluginImportReplaceAndRegister() 
{
	return true;
}

ACCB1 ASBool ACCB2 PluginLoad()
{
	WNDCLASS iacWndClass;

	iacWndClass.cbClsExtra    = 0;
    iacWndClass.hInstance     = gHINSTANCE;
    iacWndClass.style	     = CS_DBLCLKS;
    iacWndClass.hCursor	     = (HCURSOR)NULL;
    iacWndClass.hbrBackground = (HBRUSH)COLOR_BACKGROUND;
    iacWndClass.lpfnWndProc   = IACWndProc;
    iacWndClass.hIcon	     = (HICON)NULL;
    iacWndClass.lpszMenuName  = NULL;
    iacWndClass.cbWndExtra    = 0;
    iacWndClass.lpszClassName = TEXT("PRCIACMsgWin");

   	if (!RegisterClass(&iacWndClass))
		return false;

	// Create our hidden window to capture messages from the external app.
	msgHWnd = CreateWindow (TEXT("PRCIACMsgWin"), TEXT("Read PDF PRC Window"), WS_ICONIC,
								50, 50, 100, 100, 0, 0, gHINSTANCE, NULL);

	if (msgHWnd)
		ShowWindow (msgHWnd, SW_HIDE);
	else
		return false;

	return true;
}

ACCB1 ASBool ACCB2 PluginUnload()
{
	return true;
}

ASAtom GetExtensionName()
{
	return ASAtomFromString(MyPluginExtensionName);
}

ACCB1 ASBool ACCB2 PIHandshake(Uns32 handshakeVersion, void *handshakeData)
{
	if(handshakeVersion == HANDSHAKE_V0200) 
	{
		PIHandshakeData_V0200 *hsData = (PIHandshakeData_V0200 *)handshakeData;
		hsData->extensionName = GetExtensionName();
		hsData->exportHFTsCallback = (void*)ASCallbackCreateProto(PIExportHFTsProcType, &PluginExportHFTs);
		hsData->importReplaceAndRegisterCallback = (void*)ASCallbackCreateProto(PIImportReplaceAndRegisterProcType, &PluginImportReplaceAndRegister);
		hsData->initCallback = (void*)ASCallbackCreateProto(PIInitProcType, &PluginLoad);
		hsData->unloadCallback = (void*)ASCallbackCreateProto(PIUnloadProcType, &PluginUnload);
		return true;
	} 
	return false;
}

