// WritePdfPrcVC.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "WritePdfPrcVC.h"

#include <acrobat.h>
#include <iac.h>

#include <math.h>
#include <io.h>

#define A3D_API(returntype,name,params)\
typedef returntype (*PF##name) params;\
static PF##name name = NULL;

#include <A3DSDK.h>
#include <A3DSDKTypes.h>
#include <A3DSDKBase.h>
#include <A3DSDKErrorCodes.h>
#include <A3DSDKGeometry.h>
#include <A3DSDKTessellation.h>
#include <A3DSDKGraphics.h>
#include <A3DSDKStructure.h>
#include <A3DSDKRootEntities.h>
#include <A3DSDKRepItems.h>
#include <A3DSDKTessellation.h>
#include <A3DSDKMarkup.h>
#include <A3DSDKGlobalData.h>
#include <A3DSDKTexture.h>
#include <A3DSDKMisc.h>
#include <A3DSDKGeometryCrv.h>
#include <A3DSDKGeometrySrf.h>
#include <A3DSDKTopology.h>

#undef __A3DPRCSDK_H__
#undef __A3DPRCTYPES_H__
#undef __A3DPRCBASE_H__
#undef __A3DPRCERRORCODES_H__
#undef __A3DPRCGEOMETRY_H__
#undef __A3DPRCTESSELLATION_H__
#undef __A3DPRCGRAPHICS_H__
#undef __A3DPRCSTRUCTURE_H__
#undef __A3DPRCROOTENTITIES_H__
#undef __A3DPRCREPITEMS_H__
#undef __A3DPRCMARKUP_H__
#undef __A3DPRCGLOBALDATA_H__
#undef __A3DPRCTEXTURE_H__
#undef __A3DPRCMISC_H__
#undef __A3DPRCGEOMETRYCRV_H__
#undef __A3DPRCGEOMETRYSRF_H__
#undef __A3DPRCTOPOLOGY_H__

#undef A3D_API

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

const wchar_t *REG_INSTALL_KEY = TEXT("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\Acrobat.exe");
const wchar_t *PATH_KAY = TEXT("Path");

void A3DPRCFunctionPointersInitialize(HMODULE hModule)
{
#define A3D_API(returntype,name,params) name = (PF##name)GetProcAddress(hModule,#name);
#include <A3DSDK.h>
#include <A3DSDKTypes.h>
#include <A3DSDKBase.h>
#include <A3DSDKErrorCodes.h>
#include <A3DSDKGeometry.h>
#include <A3DSDKTessellation.h>
#include <A3DSDKGraphics.h>
#include <A3DSDKStructure.h>
#include <A3DSDKRootEntities.h>
#include <A3DSDKRepItems.h>
#include <A3DSDKTessellation.h>
#include <A3DSDKMarkup.h>
#include <A3DSDKGlobalData.h>
#include <A3DSDKTexture.h>
#include <A3DSDKMisc.h>
#include <A3DSDKGeometryCrv.h>
#include <A3DSDKGeometrySrf.h>
#include <A3DSDKTopology.h>
#undef A3D_API
}

HMODULE A3DPRCLoadLibrary()
{
	HMODULE hModuleA3DPRCSDK;
	
	long lRetCode;
	HKEY hkey;
	wchar_t acFilePath[MAX_PATH];;
	DWORD size = MAX_PATH;

	// Determine if a PDF viewer is installed.
	lRetCode = RegOpenKeyEx(HKEY_LOCAL_MACHINE, REG_INSTALL_KEY, 0, KEY_READ, &hkey);
	if (lRetCode == ERROR_SUCCESS) {

		// Get the path to the viewer executable and launch it.
		lRetCode = RegQueryValueEx (hkey, PATH_KAY, 0, 0, (LPBYTE)acFilePath, &size);
		RegCloseKey (hkey);

		wcscat(acFilePath, TEXT("A3DLIB.dll"));

#ifdef UNICODE 
		hModuleA3DPRCSDK = LoadLibraryExW(acFilePath, NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
#else
		hModuleA3DPRCSDK = LoadLibraryExA(acFilePath, NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
#endif
		if (hModuleA3DPRCSDK)
			return hModuleA3DPRCSDK;
	}

	return NULL;
}

void A3DPRCUnloadLibrary(HMODULE hModule)
{
	FreeLibrary(hModule);
}

// CWritePdfPrcVC

BEGIN_MESSAGE_MAP(CWritePdfPrcVC, CWinApp)
	
END_MESSAGE_MAP()


// CWritePdfPrcVC construction

CWritePdfPrcVC::CWritePdfPrcVC()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CWritePdfPrcVC object

CWritePdfPrcVC theApp;

/////////////////////////////////////////////////////////////////////////
//
// Build a PRC file on the fly
//
//////////////////////////////////////////////////////////////////////////
A3DSurfCylinder* createSurface(void)
{
	A3DSurfCylinder* pp = NULL;
	A3DSurfCylinderData sData;
	A3D_INITIALIZE_DATA(sData);
	A3D_INITIALIZE_DATA(sData.m_sParam);
	A3D_INITIALIZE_DATA(sData.m_sTrsf);

	sData.m_dRadius = 10.0;
	// A 40 mm high closed cylinder (360�) 
	// Make the cylinder parameterized between -1 and +1 in both directions ((0, 0) is the center)
	// The sUVDomain indicates the domain you want to use
	// Coefficients have to be set so that computed parameters are defined inside classical parameterization for cylinders [0.0, 360.0]
	sData.m_sParam.m_sUVDomain.m_sMin.m_dX = -1.0;
	sData.m_sParam.m_sUVDomain.m_sMin.m_dY = -1.0;
	sData.m_sParam.m_sUVDomain.m_sMax.m_dX = 1.0;
	sData.m_sParam.m_sUVDomain.m_sMax.m_dY = 1.0;
	A3DDouble dX = sData.m_sParam.m_sUVDomain.m_sMax.m_dX - sData.m_sParam.m_sUVDomain.m_sMin.m_dX;
	A3DDouble dY = sData.m_sParam.m_sUVDomain.m_sMax.m_dY - sData.m_sParam.m_sUVDomain.m_sMin.m_dY;
	sData.m_sParam.m_dUCoeffA = 360.0/dX;
	sData.m_sParam.m_dUCoeffB = 360.0/dX * sData.m_sParam.m_sUVDomain.m_sMin.m_dX;
	sData.m_sParam.m_dVCoeffA = 40.0/dY;
	sData.m_sParam.m_dVCoeffB = 40.0/dY * sData.m_sParam.m_sUVDomain.m_sMin.m_dY;
	sData.m_sParam.m_bSwapUV = FALSE;
	sData.m_sTrsf.m_ucBehaviour = kA3DTransformationIdentity;

	ASInt32 iRet = A3DSurfCylinderCreate(&sData, &pp);

	return pp;
}

A3DCrvNurbs* createCircle(A3DDouble radius)
{
	A3DCrvNurbs* pp = NULL;
	A3DCrvNurbsData sData;
	A3D_INITIALIZE_DATA(sData);
	double dSqrt2_2 = sqrt(2.0) / 2.0;

	/****************************************************************************
		Definition of a NURBs representing a circle in the parametric space:
		   Radius must be comprised between 0 and 1
		   Degree = 2, Dimension = 2
			9  control points (first and last are identical)
			9  weights
			12 knots

                 3         |2        1
                  +--------+--------+
                  |        |        |
                  |        |        |
                 4|        |        |0
                --+--------+--------+-- 
                  |        |        |8
                  |        |        |
                  |        |        |
                  +--------+--------+
                 5         6         7

	****************************************************************************/
	double adCtrlPoints[18] =
	{
		 1.0,  0.0, // 0
		 1.0,  1.0, // 1
		 0.0,  1.0, // 2
		-1.0,  1.0, // 3
		-1.0,  0.0, // 4
		-1.0, -1.0, // 5
		 0.0, -1.0, // 6
		 1.0, -1.0, // 7
		 1.0,  0.0  // 8
	};
	double adWeights[9] =
	{
		1.0,        // 0
		dSqrt2_2,	// 1
		1.0,			// 2
		dSqrt2_2,	// 3
		1.0,			// 4
		dSqrt2_2,	// 5
		1.0,			// 6
		dSqrt2_2,	// 7
		1.0			// 8
	};
	double adKnots[12]  =
	{
		0.0, 0.0, 0.0,
		0.25, 0.25,
		0.50, 0.50,
		0.75, 0.75,
		1.00, 1.00, 1.00
	};

	A3DVector3dData asControlPoints[9];
	ASInt32 i;
	for (i = 0; i < 9; i++)
		A3D_INITIALIZE_DATA(asControlPoints[i]);
	for (i = 0; i < 9; i++) {
		asControlPoints[i].m_dX = adCtrlPoints[i*2] * radius;
		asControlPoints[i].m_dY = adCtrlPoints[i*2+1] * radius;
		asControlPoints[i].m_dZ = 0;
	}

	sData.m_uiDimension = 2;
	sData.m_bRational = TRUE;
	sData.m_uiDegree = 2;
	sData.m_uiCtrlSize = 9;
	sData.m_pCtrlPts = asControlPoints;
	sData.m_uiWeightSize = 9;
	sData.m_pdWeights = adWeights;
	sData.m_uiKnotSize = 12;
	sData.m_pdKnots = adKnots;
	sData.m_eCurveForm = kA3DBSplineCurveFormCircularArc;
	sData.m_eKnotType = kA3DKnotTypeUnspecified;


	ASInt32 iRet = A3DCrvNurbsCreate(&sData, &pp);

	return pp;
}


A3DTopoEdge* createTopoEdge(void)
{
	A3DTopoEdge* pp = NULL;
	A3DTopoEdgeData sData;
	A3D_INITIALIZE_DATA(sData);

	ASInt32 iRet = A3DTopoEdgeCreate(&sData, &pp);

	return pp;
}

A3DTopoCoEdge* createTopoCoEdge(A3DCrvBase* p)
{
	A3DTopoCoEdge* pp = NULL;

	A3DTopoCoEdge* q = createTopoEdge();
	if (p != NULL && q != NULL) {
		A3DTopoCoEdgeData sData;
		A3D_INITIALIZE_DATA(sData);
		sData.m_pUVCurve = p;
		sData.m_pEdge = q;
		sData.m_ucOrientationWithLoop = 1;
		sData.m_ucOrientationUVWithLoop = 1; 
		ASInt32 iRet = A3DTopoCoEdgeCreate(&sData, &pp);
	}

	return pp;
}

A3DTopoLoop* createTopoLoop(A3DDouble radius)
{
	A3DTopoLoop* pp = NULL;
	A3DTopoLoopData sData;
	A3D_INITIALIZE_DATA(sData);

	A3DCrvBase* p = createCircle(radius);
	A3DTopoCoEdge* q = createTopoCoEdge(p);

	sData.m_ppCoEdges = &q;
	sData.m_uiCoEdgeSize = 1;
	sData.m_ucOrientationWithSurface = 1;

	ASInt32 iRet = A3DTopoLoopCreate(&sData, &pp);

	return pp;
}

A3DTopoFace* createTopoFace(void)
{
	A3DTopoFace* pp = NULL;
	A3DTopoFaceData sData;
	A3D_INITIALIZE_DATA(sData);

	sData.m_pSurface = createSurface();

	A3DDouble outerradius = 0.5;
	A3DDouble innerradius = 0.4;
	A3DTopoLoop* loops[2];
	loops[0] = createTopoLoop(outerradius);
	loops[1] = createTopoLoop(innerradius);

	sData.m_ppLoops = loops;
	sData.m_uiLoopSize = 2;
	sData.m_uiOuterLoopIndex = 0;

	ASInt32 iRet = A3DTopoFaceCreate(&sData, &pp);

	return pp;
}

A3DTopoShell* createTopoShell(void)
{
	A3DTopoShell* pp = NULL;

	A3DTopoFace* p = createTopoFace();
	if (p != NULL) {
		A3DTopoShellData sData;
		A3D_INITIALIZE_DATA(sData);

		ASUns8 orient = 1;
		sData.m_bClosed = FALSE;
		sData.m_ppFaces = &p;
		sData.m_uiFaceSize = 1;
		sData.m_pucOrientationWithShell = &orient;

		ASInt32 iRet = A3DTopoShellCreate(&sData, &pp);
	}

	return pp;
}

A3DTopoConnex* createTopoConnex(void)
{
	A3DTopoConnex* pp = NULL;

	A3DTopoShell* p = createTopoShell();
	if (p != NULL) {
		A3DTopoConnexData sData;
		A3D_INITIALIZE_DATA(sData);

		sData.m_ppShells = &p;
		sData.m_uiShellSize = 1;

		ASInt32 iRet = A3DTopoConnexCreate(&sData, &pp);
	}

	return pp;
}

A3DTopoBrepData* createTopoBrep(void)
{
	A3DTopoBrepData* pp = NULL;

	A3DTopoConnex* p = createTopoConnex();
	if (p != NULL) {
		A3DTopoBrepDataData sData;
		A3D_INITIALIZE_DATA(sData);

		sData.m_ppConnexes = &p;
		sData.m_uiConnexSize = 1;

		ASInt32 iRet = A3DTopoBrepDataCreate(&sData, &pp);
	}

	return pp;
}

A3DRiRepresentationItem* createRIBrep(void)
{
	A3DRiBrepModel* pp = NULL;

	A3DRiRepresentationItem* p = createTopoBrep();
	if (p != NULL) {
		A3DRiBrepModelData sData;
		A3D_INITIALIZE_DATA(sData);

		sData.m_pBrepData = p;
		sData.m_bSolid = FALSE;

		ASInt32 iRet = A3DRiBrepModelCreate(&sData, &pp);
	}

	return pp;
}

A3DAsmPartDefinition* createPart(void)
{
	A3DAsmPartDefinition* pp = NULL;

	A3DRiRepresentationItem* p = createRIBrep();
	if (p != NULL) {
		A3DAsmPartDefinitionData sData;
		A3D_INITIALIZE_DATA(sData);

		sData.m_uiRepItemsSize = 1;
		sData.m_ppRepItems = &p;

		ASInt32 iRet = A3DAsmPartDefinitionCreate(&sData, &pp);
	}

	return pp;
}

// write attributes
A3DVoid setAttributes(A3DEntity* p)
{
	A3DMiscAttribute* pAttr[3];

	A3DMiscSingleAttributeData Single;
	A3D_INITIALIZE_DATA(Single);

	Single.m_eType = kA3DModellerAttributeTypeString;
	Single.m_pcTitle = "Title";
	Single.m_pcData = "Simple Brep building demonstration";

	A3DMiscAttributeData sAttribs;
	A3D_INITIALIZE_DATA(sAttribs);

	sAttribs.m_pcTitle = Single.m_pcTitle;
	sAttribs.m_pSingleAttributesData = &Single;
	sAttribs.m_uiSize = 1;
	ASInt32 iRet = A3DMiscAttributeCreate(&sAttribs, &pAttr[0]);

	Single.m_pcTitle = "Author";
	Single.m_pcData = "Acrobat SDK";
	sAttribs.m_pcTitle = Single.m_pcTitle;
	iRet = A3DMiscAttributeCreate(&sAttribs, &pAttr[1]);

	Single.m_pcTitle = "Company";
	Single.m_pcData = "Adobe Systems Incorporated";
	sAttribs.m_pcTitle = Single.m_pcTitle;
	iRet = A3DMiscAttributeCreate(&sAttribs, &pAttr[2]);

	A3DRootBaseData sRootData;
	A3D_INITIALIZE_DATA(sRootData);

	sRootData.m_pcName = "Trimmed surface";
	sRootData.m_ppAttributes = pAttr;
	sRootData.m_uiSize = 3;
	iRet = A3DRootBaseSet(p, &sRootData);
}

A3DAsmProductOccurrence* createOccurence(void)
{
	A3DAsmProductOccurrence* pp = NULL;

	A3DAsmPartDefinition* p = createPart();
	if (p != NULL) {
		A3DAsmProductOccurrenceData sData;
		A3D_INITIALIZE_DATA(sData);

		sData.m_pPart = p;

		ASInt32 iRet = A3DAsmProductOccurrenceCreate(&sData, &pp);
		if(iRet == A3D_SUCCESS)
			setAttributes(pp);
	}

	return pp;
}

// create model file
A3DAsmModelFile* createModel(void)
{
	A3DAsmModelFile* pp = NULL;

	A3DAsmProductOccurrence* p = createOccurence();
	if (p != NULL) {
		A3DAsmModelFileData sData;
		A3D_INITIALIZE_DATA(sData);

		sData.m_uiPOccurrencesSize = 1;
		sData.m_dUnit = 1.0;
		sData.m_ppPOccurrences = &p;

		ASInt32 iRet = A3DAsmModelFileCreate(&sData, &pp);
	}

	return pp;
}

// write model file into physical file
A3DVoid createPrcFile(const A3DUTF8Char* pcPRCName)
{
	A3DAsmModelFile* p = createModel();
	if (p != NULL) {
		if (_access(pcPRCName, 00) != -1)
			_unlink(pcPRCName);

		/* Save the PRC File */
		A3DAsmModelFileWriteParametersData sParametersData;
		A3D_INITIALIZE_DATA(sParametersData);
		sParametersData.m_eFormatChoice = kA3DPRC;

		ASInt32 iRet = A3DAsmModelFileWriteToFile(p, &sParametersData, pcPRCName);
		A3DAsmModelFileDelete(p);
	}
}

// CWritePdfPrcVC initialization

BOOL CWritePdfPrcVC::InitInstance()
{
	CWinApp::InitInstance();

	const A3DUTF8Char* prcName = "C:\\WritePdfPrcVC.prc";

	HMODULE hModuleA3DPRCSDK = A3DPRCLoadLibrary();

	if (!hModuleA3DPRCSDK) {
		AfxMessageBox(TEXT("Failed to load A3DLIB.dll!"));
		return FALSE;
	}

	A3DPRCFunctionPointersInitialize(hModuleA3DPRCSDK);

	ASInt32 iMajorVersion = 0, iMinorVersion = 0; 
	A3DDllGetVersion(&iMajorVersion, &iMinorVersion);

	ASInt32 iRet = A3D_SUCCESS; 
	if(iMajorVersion != A3D_DLL_MAJORVERSION) 
		iRet = A3D_ERROR;
	else if(iMinorVersion < A3D_DLL_MINORVERSION) 
		iRet = A3D_ERROR;

	if(iRet != A3D_SUCCESS) {
		AfxMessageBox(TEXT("The PRC library version on this system is not supported!"));
		A3DPRCUnloadLibrary(hModuleA3DPRCSDK);
		return FALSE;
	}

	iRet = A3DDllInitialize(iMajorVersion, iMinorVersion);
	if(iRet == A3D_SUCCESS) {
		createPrcFile(prcName);

		// === Initialize OLE libraries. 
		if (!AfxOleInit())	{
			AfxMessageBox(TEXT("OLE initialization failed."));
			A3DDllTerminate();
			A3DPRCUnloadLibrary(hModuleA3DPRCSDK);
			return FALSE;
		}

		COleException e;

		CAcroApp * pAcroApp = new CAcroApp();
		pAcroApp->CreateDispatch(TEXT("AcroExch.App"), &e);

		CAcroAVDoc * pAcroAvDoc = new CAcroAVDoc();
		pAcroAvDoc->CreateDispatch(TEXT("AcroExch.AVDoc"), &e);

		if(!pAcroAvDoc->Open(TEXT("C:\\WritePdfPrcVC.prc"), TEXT("WritePdfPrcVC.pdf"))) {
			AfxMessageBox(TEXT("Failed to open file C:\\WritePdfPrcVC.prc!"));

			_unlink(prcName);
			delete pAcroAvDoc;

			pAcroApp->Exit();
			delete pAcroApp;
			A3DDllTerminate();
			A3DPRCUnloadLibrary(hModuleA3DPRCSDK);
			return FALSE;
		}

		CAcroPDDoc acroPdDoc(pAcroAvDoc->GetPDDoc());
		acroPdDoc.Save(PDSaveFull, TEXT("C:\\WritePdfPrcVC.pdf"));

		pAcroAvDoc->Close(1);
		delete pAcroAvDoc;
		pAcroApp->CloseAllDocs();
		pAcroApp->Exit();
		delete pAcroApp;

		_unlink(prcName);
		A3DDllTerminate();

		AfxMessageBox(TEXT("The generated PDF file has been saved as C:\\WritePdfPrcVC.pdf."));
	} else {
		CString strErr = TEXT("A3DDllInitialize() failed.");
		AfxMessageBox(strErr);
	}

	A3DPRCUnloadLibrary(hModuleA3DPRCSDK);

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
