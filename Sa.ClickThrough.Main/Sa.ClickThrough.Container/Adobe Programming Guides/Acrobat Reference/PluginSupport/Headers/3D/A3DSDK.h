/*******************************************************************************
* 
*  ADOBE SYSTEMS INCORPORATED
*  Copyright 2008 Adobe Systems Incorporated
*  All Rights Reserved
*  
*  NOTICE:  Adobe permits you to use, modify, and distribute this file in 
*  accordance with the terms of the Adobe license agreement accompanying it.  
*  If you have received this file from a source other than Adobe, then your use, 
*  modification, or distribution of it requires the prior written permission of 
*  Adobe.
* 
*******************************************************************************/

/*!
\file
\brief	Top-level header file of the \COMPONENT_A3D_API
\author	ADOBE SYSTEMS INCORPORATED
\version	2.0
\date		February 2009
\par		(c) Copyright 2009 Adobe Systems Incorporated. All rights reserved.
*/

/*! 
\mainpage \COMPONENT_A3D_API Reference, version 2.1


<b>(c) Copyright 2009 &reg;Adobe Systems Incorporated.</b> All rights reserved.\n
This Software and Related Documentation are proprietary to Adobe Systems Incorporated. \n


Restricted Rights Legend: This commercial computer software and related documentation are
provided with restricted rights. Use, duplication, or disclosure by the U.S. Government is subject to
the protections and restrictions as set forth in the Adobe Systems Incorporated commercial license
for the software, documentation, and related materials as prescribed in DOD FAR 227-7202-3(a), or for
Civilian agencies, in FAR 27.404(b)(2)(i), and any successor or similar regulation, as applicable. \n
 
This documentation is provided under license from Adobe Systems Incorporated. This documentation
is, and shall remain, the exclusive property of Adobe Systems Incorporated. Its use is governed by the
terms of the applicable license agreement. Any copying of this documentation, except as permitted
in the applicable license agreement, is expressly prohibited.
The information contained in this document is subject to change without notice and should not be
construed as a commitment by Adobe Systems Incorporated. 
Adobe Systems Incorporated assumes no responsibility for any
errors or omissions that may appear in this documentation. \n

All other trademarks are the property of their respective owners. \n

Adobe Systems Incorporated, 345 Park Avenue, San Jose, California 95110, USA \n

\section Product

The \COMPONENT_A3D_API is the public interface for the \COMPONENT_A3D_LIBRARY. 

The \COMPONENT_A3D_LIBRARY is a dynamic libraries package 
that  lets you  develop &reg;Acrobat plug-ins and PDF Library applications 
that create and access data in a PRC stream. 
Such a stream can be embedded within a PDF document as a 3D annotation. 

\section Contents

\par Main Page (this page)

Contains copyright notices and describes the reference's contents.

\par Overview 

Describes overview and non-normative content. 
This section provides the following introductory information on the \COMPONENT_A3D_LIBRARY. 

\li \ref a3d_overview
\li \ref a3d_capabilities
\li \ref a3d_functions
\li \ref a3d_naming_conventions
\li \ref a3d_base_attributes
\li \ref a3d_misc_cascaded_attributes
\li \ref a3d_faq

\par Modules

Shows the hierarchy of the PRC entities that comprise the \COMPONENT_A3D_API.
These entities correspond to the serialized objects described by the \REF_PRC_SPEC.
A PRC entity is an opaque C structure that contains highly compressed PRC data.
The \COMPONENT_A3D_API provides functions that let you parse and create PRC entities.

Click the entries in the Modules page to see module descriptions 
or to see the functions, structures, or enumerations in the module.

\par Data Structures

Describes the public data structures that you use when parsing or creating a PRC entity. 

\par Files

Lists the header files that make up the \COMPONENT_A3D_API.

\par Index

Alphabetic listings of all globally defined declarations.

*/

/*!
\page a3d_overview General Overview

The \COMPONENT_A3D_LIBRARY lets you develop Acrobat plug-ins and PDF Library applications 
that create and access data in a PRC stream. 
Such a stream can be embedded within a PDF document as a 3D annotation. 

The ability to access PRC data is especially valuable to applications 
that consume 3D CAD data or that create 3D data to be shared with external systems.
Using \COMPONENT_A3D_LIBRARY, you can fully access the 3D data that comprise PRC files,
including geometry information, structure, and Product and Manufacturing Information (PMI).\n

The \COMPONENT_A3D_API defines the public interface with the \COMPONENT_A3D_LIBRARY. 
The API provides dedicated functions, types, and other API features 
that have self-documenting names. 

The \COMPONENT_A3D_API has these architectural characteristics:
<ul>
	<li>Supports Microsoft&reg; Windows&reg; only.</li>
	<li>Uses ANSI C DLL calling convention. 
		All exported functions are declared using \c extern  \c "C" statements.</li>
	<li>Uses 32-bit operators.</li>
	<li>Does not support multi-threading. </li>
	<li>Not licensed to run in a batch or server mode.</li>
	<li>See the EULA for additional restrictions that may apply.</li>
</ul>

Acrobat 3D API handles these tasks:
\li Determines and sets the structure size member present in every A3DEntityData structure.
\li Creates and manages the unique identifiers for all entities.
\li Recasts entity types when going from a specific type to a base for that type.

\sa \ref a3d_capabilities
\sa \ref a3d_functions
\sa \ref a3d_naming_conventions

*/

/*!
\page a3d_capabilities Capabilities

\section a3d_reading_capabilities \COMPONENT_A3D_LIBRARY reading capabilities
The \COMPONENT_A3D_LIBRARY enables a plug-in installed in Acrobat Pro Extended or a PDF Library application to load 
PRC files into memory, and then to access the data in specific PRC entities. 
Some restrictions may apply to a PDF Library application's use of \COMPONENT_A3D_LIBRARY.
See your Adobe representative for details.

The \COMPONENT_A3D_LIBRARY provides these features:

\li Reading operations that are optimized for speed and does not alter data in the original PRC file.
\li Filtering that enables your plug-in or application to focus on the PRC information of interest. 

For information about creating a plug-in or application that consumes a PRC file, see \REF_PLUGIN_DEV_GUIDE.

\section a3d_querying_capabilities \COMPONENT_A3D_API querying capabilities
The \COMPONENT_A3D_API provides a range of functions you can use to obtain information about the PRC data. 
Here are examples of the types of PRC data you can access: 
\li General information, such as version, tolerances, units, and RGB color information. 
See \ref a3d_base_functions and \ref a3d_global_data_module.
\li Geometry, such as points, directions, axis systems, curves and surfaces definition. See \ref a3d_geometry_module.
\li Topology, such as solid definition by connexes, shells, faces, loops, edges and vertices. See \ref a3d_topology_module.
\li Structure data, such as assemblies and instances. See \ref a3d_structure_module.
\li Graphic attributes, such as colors, layers, line type, and blanked entities. See \ref a3d_graphics_module.
\li Markups, such as 3D notes, texts, and dimensions. See \ref a3d_markup_module.
\li Tessellation, such as triangles, normals, and textures. See \ref a3d_tessellation_module.

The \COMPONENT_A3D_API does not provide access to arbitrary user data embedded in the PRC data.

\section a3d_writing_capabilities \COMPONENT_A3D_API writing capabilities
The \COMPONENT_A3D_API provides functions that let you create 3D models, 
which you then export to PRC files. 
These functions let you create the following types of entities:
\li General entities. See \ref a3d_global_data_module.
\li Geometrical entities. See \ref a3d_geometry_module.
\li Topological entities. See \ref a3d_geometry_module.
\li Structure of these entities. See \ref a3d_structure_module.
\li Graphic attributes. See \ref a3d_graphics_module.
\li Markup entities. See \ref a3d_tessellation_module.
\li Tessellation entities. See \ref a3d_tessellation_module.

You can embed the PRC files you produce in PDF files 
and create 3D annotations that reference those embedded PRC files. 

\section memory_allocation Memory allocation

Memory allocation is done using the Acrobat 3D API memory allocation functions, which are the \ref A3DMiscAllocFunction and \ref A3DFreeFunction functions; 
however, you can override these with custom memory-allocation functions.


For information about creating a plug-in or application that produces a PRC file, see \REF_PLUGIN_DEV_GUIDE.
*/


/*!
\page a3d_functions Functions, Callback Functions, and PRC Entity References

\section a3d_types_and_structures "C" Structures, functions and function returns

The \COMPONENT_A3D_LIBRARY exposes functions and the structures and other types that those functions accept as arguments. 

\par C structures

The following table describes the structures defined for each PRC entity. 
The entries in this table use the <i>italics</i> font face to indicate placeholder names.

<TABLE rules="all" cellspacing="1">
<TR><TH>C structure name</TH><TH>Description</TH></TR>
<TR>
	<TD><CODE>A3D<i>Prefix<b>EntityName</b></i></CODE></TD>
	<TD>A \c void* type in which the \COMPONENT_A3D_LIBRARY stores a private copy of the entity
		(for example, the typedef for a planar surface is \ref A3DSurfPlane).</TD>
</TR>
<TR>
	<TD><CODE>A3D<i>Prefix<b>EntityName</b></i>Data</CODE></TD>
	<TD>Public data for the PRC entity, with the data made available as members 
		or as cascaded attributes (see \ref a3d_misc_cascaded_attributes).
		For example, the typedef for the public data in a planar surface is \ref A3DSurfPlaneData.</TD>
</TR>
</TABLE>

\par Functions

The following table describes the functions for use with most \c A3D<i>Prefix<b>EntityName</b></i> objects.

<TABLE rules="all" cellspacing="1">
<TR><TH>Function name</TH><TH>Description</TH></TR>
<TR>
	<TD><CODE>A3D<i>Prefix<b>EntityName</b></i>Create</CODE></TD>
	<TD>Given a pointer to an <CODE>A3D<i>Prefix<b>EntityName</b></i>Data</CODE> structure, 
		this category of function creates an <CODE>A3D<i>Prefix<b>EntityName</b></i></CODE> structure. 
		This type of function is used to create PRC data. 
		For example, given a populated \ref A3DSurfPlaneData, 
		the \ref A3DSurfPlaneCreate function creates the \ref A3DSurfPlane PRC entity.
		\attention The \COMPONENT_A3D_LIBRARY performs internal checks before creating the entity. 
		If it detects an error, it returns an error code to the 
		<CODE>A3d<i>Prefix<b>EntityName</b></i>Create()</CODE> function. 
		You must modify the input data accordingly. 
		<!-- What exactly should the program do? -->
	</TD>
</TR>
<TR>

	<TD><CODE>A3D<i>Prefix<b>EntityName</b></i>Get</CODE></TD>
	<TD>Given a pointer to the PRC entity, this type of function populates an <CODE>A3D<i>Prefix<b>EntityName</b></i>Data</CODE> structure 
		with data. This type of function is used to parse PRC data. 
		For example, given the \ref A3DSurfPlane, the \ref A3DSurfPlaneGet function populates the 
		\ref A3DSurfPlaneData structure with information about the planar surface.

		\attention Before invoking an <CODE>A3d<i>Prefix<b>EntityName</b></i>Get()</CODE> function, 
		you must initialize the structure that the data will be stored in by using 
		the macro \ref A3D_INITIALIZE_DATA. 
		If you do not initialize the structure, the function returns 
		\ref A3D_INVALID_DATA_STRUCT_SIZE and does not return 
		the requested data. See the following sample code.
	</TD>
</TR>
<TR>
	<TD><CODE>A3D<b>XXX</b>Create</CODE> or <CODE>A3D<b>XXX</b>Get</CODE></TD>
	<TD>No-op implementations of the functions described above. 
		These functions are used in examples of how to create or free a generic 
		\ref A3DXXX entity. In real use, <CODE><b>XXX</b></CODE> 
		is replaced with a specific <CODE><i>Prefix<b>EntityName</b></i></CODE>.</TD>
</TR>
</TABLE>

\par Function returns

Unless specified, all functions return an integer (ASInt32) with the value \ref A3D_SUCCESS 
in case of success and a 
negative value in case of error. All possible error codes are listed for each function and related meanings are 
in the A3DPRCErrorCodes.h file.

\section a3d_callback_functions Callback functions
You can define a set of callback functions to integrate \COMPONENT_A3D_LIBRARY 
into your own environment. These functions can handle the following issues:
\li Reporting progress indicators to the user (see \ref a3d_progress_functions)
\li Reporting messages, warnings and errors to the user (see \ref a3d_message_functions)
\li Handling memory allocation (see \ref a3d_malloc_functions)

\section prc_references References to PRC entities

The \COMPONENT_A3D_LIBRARY creates and interprets the unique identifiers 
the PRC format uses to to reference PRC entities.

The PRC format describes the use of unique identifiers to reference PRC entities.
Every PRC entity can be referenced by other PRC entities, provided those references
conform to the conventions specified in the \REF_PRC_SPEC.

The \COMPONENT_A3D_LIBRARY manages the references between PRC entities,
saving you from creating and resolving those unique identifiers. 
More specifically, a <CODE>A3D<i>Prefix<b>EntityName</b></i>Get</CODE> function takes as an argument
a pointer to another PRC entity. The pointer is an opaque structure that the 
\COMPONENT_A3D_LIBRARY resolves to the PRC entity of interest. <!-- check "opaque struct" -->

In the following sample code, the \ref A3DAsmPartDefinitionGet function 
takes as an argument a pointer to a PRC part definition. 
The pointer is an opaque structure that includes that part definition's unique identifier.
\ref A3DAsmPartDefinitionGet resolves the pointer into an object that represents that part definition.

\par Sample code

The following sample code shows the creation and initialization of the 
<CODE>A3D<i>Prefix<b>EntityName</b></i></CODE> and <CODE>A3D<i>Prefix<b>EntityName</b></i>Data</CODE> 
structures. It also shows the invocation of the \ref A3DAsmPartDefinitionGet function. 
The \ref A3D_INITIALIZE_DATA macro initializes the \ref A3DAsmPartDefinitionData  structure. 
You must use that macro to initialize any \COMPONENT_A3D_API data structure used in your application. 

\include TypesAndStructures.cpp
<!-- Just after the declaration of pPartDefinition, 
add a note to the sample code indicating that A3DAsmPartDefinition is earlier obtained 
from the m_pPart member of a product occurrance. -->


<!-- end of related page "Technical Overview" -->
*/ 

/*!
\page a3d_naming_conventions Naming Conventions

The \COMPONENT_A3D_API uses naming conventions that are self-documenting. 
The names of all declared items (types, functions, structures, and structure members) express 
the category of declaration they represent and
the type of PRC entity they represent or operate on.
By understanding the \COMPONENT_A3D_API naming conventions, 
your programming effort will be much easier.

\par Identifying items declared by the \COMPONENT_A3D_API

The \COMPONENT_A3D_API naming conventions use prefixes to identify all items it declares.
This convention distinguishes items declared by \COMPONENT_A3D_API from items declared
by any other libraries you are using. This convention also enables portability.

The following table summarizes the general prefix naming convention.

<TABLE rules="all" cellspacing="1">
<TR><TH>Category</TH><TH>Format</TH><TH>Description</TH></TR>
<TR>
	<TD>Basic types</TD>
	<TD><code>A3D<i>Type</i></code>
	<TD>Types unique to \COMPONENT_A3D_API are prefixed with \c A3D to facilitate portability 
		(for example the \ref A3DDouble type). Types adopted from the Acrobat SDK are prefixed with \c AS.</TD>
</TR>
<TR>
	<TD>Entities</TD>
	<TD><code>A3D<i>Prefix<b>EntityName</b></i></code></TD>
	<TD>An opaque structure containing compressed 3D data.
		The <code><i>Prefix</i></code> indicates the entity category
		(for example the \ref A3DCrvCircle entity defines a portion of a curve (an arc) 
		that is based on a circle). The prefix is \c Crv, indicating that the entity is grouped 
		with other entities that also define curves.</TD>
</TR>

<TR>
	<TD>Functions</TD>
	<TD><code>A3D<i>Prefix<b>EntityName</b>Action</i></code></TD>
	<TD><code>A3D<i>Prefix<b>EntityName</b></i></code> identifies the entity,
		and <code><i>Action</i></code> indicates the action performed by the function.

		Functions that apply to all PRC entities are prefixed with <CODE>A3DXXX</CODE>
		(for example, \ref A3DXXXCreate and \ref A3DXXXGet).</TD>
</TR>
<TR>
	<TD>"C" Structures</TD>
	<TD><code>A3D<i>Prefix<b>EntityName</b>Data</i></code></TD>
	<TD><code>A3D<i>Prefix<b>EntityName</b></i></code> identifies the entity, and
		<code><i>Data</i></code> indicates that the structure contains data associated with the entity. 
</TR>
<TR>
	<TD>Enumerates</TD>
	<TD><code>kA3D<i>Prefix<b>EntityName</b>Attribute</i></code></TD>
	<TD>The \c k indicates that the item is an enumerated value (for example \c kA3DGraphicsShow). </TD>
</TR>
</TABLE>

\par Structure members

All structures members are prefixed with \c m_ followed by one or more lowercase letters 
representing the type of data, as shown in the following table.

<TABLE rules="all" cellspacing="1">
<TR><TH>Prefix</TH><TH>Description</TH></TR>
<TR>
	<TD>\c m_b</TD>
	<TD>Value of type \c ASBool (for example the \ref A3DMiscCascadedAttributesData::m_bShow member).</TD>
</TR>
<TR>
	<TD>\c m_c</TD>
	<TD>Value of type \c ASInt8 (for example the \ref A3DFontKeyData::m_cAttributes member). 
		The \c ASInt8 type is equivalent to the \c char type in the C standard library.</TD>
</TR>
<TR>
	<TD>\c m_d</TD>
	<TD>Value of type \ref A3DDouble (for example the \ref A3DCrvHelixPitchVarData::m_dRatioU member).</TD>
</TR>
<TR>
	<TD>\c m_e</TD>
	<TD>Enumerated value (for example the \ref A3DMkpMarkupData::m_eType member).</TD>
</TR>
<TR>
	<TD>\c m_i</TD>
	<TD>Value of type \c ASInt32 (for example the \ref A3DFontKeyData::m_iFontFamilyIndex member).</TD>
</TR>
<TR>
	<TD>\c m_p</TD>
	<TD>Generic pointer (for example the \ref A3DMkpLeaderData::m_pLinkedItem member).</TD>
</TR>
<TR>
	<TD>\c m_s</TD>
	<TD>Generic structure) (for example the \ref A3DMiscCartesianTransformationData::m_sOrigin.</TD>
</TR>
<TR>
	<TD>\c m_uc</TD>
	<TD>Value of type \c ASUns8 (for example the \ref A3DGraphTextureDefinitionData::m_ucTextureDimension member).
		The \c ASUns8 type is equivalent to the \c unsigned \c char type in the C standard library.</TD>	
</TR>
<TR>
	<TD>\c m_ui</TD>
	<TD>Value of type \c ASUns32 (for example the \ref A3DGlobalData::m_uiColorsSize member).
		The \c ASUns32 type is equivalent to the \c unsigned \c int type in the C standard library.</TD>
</TR>
<TR>
	<TD>\c m_us</TD>
	<TD>Value of type \c ASUns16 (for example the \ref A3DTessFaceData::m_usStructSize member).
	The \c ASUns16 type is equivalent to the \c unsigned \c short type in the C standard library.	</TD>
</TR>
</TABLE>

\par Pointer and array structure members

Some pointer and array structure members include an additional prefix letter that 
indicates the type of structure they reference, as described in the following table. 

<TABLE rules="all" cellspacing="1">
<TR><TH>Prefix</TH><TH>Description</TH></TR>
<TR>
	<TD>\c m_ad</TD>
	<TD>Array of \ref A3DDouble values, for example the A3DMathFct3DLinearData::m_adMatrix member.</TD>
</TR>
<TR>
	<TD>\c m_pc</TD>
	<TD>Pointer to an \ref A3DUTF8Char character string
		(for example the \ref A3DFontData::m_pcFamilyName member).</TD>
	<!-- do such generic pointers inherit A3DPtr? -->
</TR>
<TR>
	<TD>\c m_pd</TD>
	<TD>Pointer to an \ref A3DDouble (for example the \ref A3DTessBaseData::m_pdCoords member).</TD>
</TR>
<!-- is there a type called m_pfunc? --> 
<TR>
	<TD>\c m_pp</TD>
	<TD>Pointer to an array of pointers (for example the \ref A3DRootBaseData::m_ppAttributes member).</TD>
</TR>
<TR>
	<TD>\c m_ps</TD>
	<TD>Pointer to a structure (for example the \ref A3DTess3DData::m_psFaceTessData member).</TD>
</TR>
</TABLE>
*/

/*!
\page a3d_base_attributes Entity Base Attributes

The attributes for a PRC entity includes data specific to the entity 
and data specified in base entities or abstract classes inherited by that entity.
You access the data specific to the entity using the get and set functions specific to the entity.
These functions have the form 
<code>A3D<i>Entity_name</i>Get</code> or <code>A3D<i>Entity_name</i>Set</code>. 
How you get or set the data specified in a base entity or an inherited class depends on the entity,
as described in this table.

<TABLE rules="all" cellspacing="1">
	<TR><TH>Name of Abstract Class</TH><TH>Application</TH><TH>Description</TH></TR>
	<TR>
		<TD>\ref A3DRootBase</TD>
		<TD>All PRC entities</TD>
		<TD>A root entity that represents a name and (optionally) 
			a hierarchy of names and/or modeler data. 
			\n \n Get this data with the \ref A3DRootBaseGet function.
			\n \n Set this data and associate it with the PRC entity by invoking the \ref A3DRootBaseSet function.
			</TD>
	</TR>
	<TR>
		<TD>\ref A3DRootBaseWithGraphics</TD>
		<TD>Any entity with graphics</TD>
		<TD>An abstract class that references an \ref A3DGraphics entity, 
			which in turn can specify a layer index, style index and inheritance behavior. 
			\n \n Get this data using the \ref a3d_misc_cascaded_attributes facility, 
			which uses the inheritance settings to determine whether to return the settings 
			for the entity, its parent, or its child. (See \ref a3d_misc_cascaded_attributes.)
			\n \n Set this data using the functions from derived classes.</TD>
	</TR>
	<TR>
		<TD>\ref A3DGlobal</TD>
		<TD>All PRC entities</TD>
		<TD>This entity is not a root entity or an abstract class.
			Instead, it is used for associating attributes with indexes. 
			Other entities use these indexes to specify the attribute characteristic.
			This ensures a standard definition for attributes and provides 
			a more efficient storage.</TD>
	</TR>
	<TR>
		<TD>\ref A3DRiRepresentationItem</TD>
		<TD>Entities in the \ref a3d_repitem</TD>
		<TD>A root entity that contains data common to all representation items.
			\n \n Get this data with the \ref A3DRiRepresentationItemGet function, 
			determine the type of the derived class it represents, 
			and then invoke the function for that representation item. 
			\n \n Set this data with the derived class such as the \ref A3DRiBrepModelCreate function. </TD>
	</TR>
	<TR>
		<TD>\ref A3DTopoBody</TD>
		<TD>Entities in the \ref a3d_topology_module</TD>
		<TD>A root entity that contains data common to all entities in the \ref a3d_topology_module module,  
			such as a coordinate system entity (A3DRiCoordinateSystem). 
			This class describes topology behavior (inheritance) 
			and context (granularity, thickness, and scale). 
			\n \n Get the data in this base class with the \ref A3DTopoBodyGet function. 
			\n \n Set the data in this base class with the derived class, such as the \ref A3DTopoSingleWireBodyCreate function.</TD> 
		</TD>
	</TR>
	<TR>
		<TD>\ref A3DTessBase</TD>
		<TD>Entities in the \ref a3d_tessellation_module</TD>
		<TD>An abstract class common to all tessellation entities.
			Unlike the other root entities, 
			this one does not provide data common to all tessellation entities
			The data returned by the \ref A3DRiRepresentationItemGet function contains a pointer 
			to this abstract class. Use the \ref A3DEntityGetType function to determine the 
			type of the derived class, and then use appropriate function to obtain the data.
			These specific functions are \ref A3DTess3DGet, \ref A3DTess3DWireData, 
			and \ref A3DTessMarkupData.
		</TD>
	</TR>

</TABLE>



<!-- End of Related Page on "Entity Base Attributes" -->
*/ 

/*!
\page a3d_misc_cascaded_attributes Miscellaneous Cascaded Attributes

Miscellaneous cascaded attributes is a facility that reconciles
the inheritable graphics data that can be applied to a PRC entity.
Such inheritable data includes the show and removed settings, 
style attributes (such as color and pattern), layer attributes, 
and coordinate system transformations.	

\section Graphics_attributes_and_inheritance Graphics attributes and inheritance

The \ref A3DGraphicsCreate function lets you set the graphics data applied to 
an entity. It also lets you set inheritance behavior for the attributes. 
These bits can specify that an attribute be inherited from a child entity
or from a parent entity. 

If the data supplied to the \ref A3DGraphicsCreate function specifies 
inheritance of a particular attribute, then the attribute specified for the entity is ignored 
and only the inherited attribute is used. 
For example, if a graphic entity has the \c kA3DGraphicsShow setting
and the \c kA3DGraphicsSonHeritShow setting, 
then the \c kA3DGraphicsShow setting is ignored. 

The \ref A3DGraphicsGet function obtains the graphics data for a particular 
PRC entity; however, it does not honor inheritance settings.
\sa a3d_graphics

\section Using_cascaded_attributes Using cascaded attributes to get graphics attributes for an entity

Because cascaded attributes include inheritable attributes, 
the \COMPONENT_A3D_LIBRARY implements a stack architecture 
in which you store miscellaneous cascaded attributes,
where the bottom entry in the stack is the model file. 
As your code recurses through the PRC entities that comprise the PRC model file,
you invoke the PRC function to add cascaded attributes to the stack,
as described in the following sample code. The first entry is for the model file
and the next is the first product occurrence entity.

\par Sample code

Here are the main tasks performed in the following sample code:
<ol>
	<li>\ref A3DMiscCascadedAttributesCreate creates an empty structure 
	in which the cascaded attributes are to be stored.</li>
	<li>\ref A3DMiscCascadedAttributesPush populates that structure
	with the miscellaneous cascaded attributes for the PRC entity
	and pushes the parent cascaded attribute structure onto the stack.
	One of the \ref A3DMiscCascadedAttributesPush arguments is a pointer to the parent entity's
	\ref A3DMiscCascadedAttributes stack location.</li>
	<li>\ref A3DMiscCascadedAttributesGet populates a structure with the miscellaneous cascaded attributes. 
	You can access members in the \ref A3DMiscCascadedAttributesData structure.
	<li>\ref A3DMiscCascadedAttributesDelete deletes the previously created \ref A3DMiscCascadedAttributes structure. </li>
</ol>

\include CascadedAttributes.cpp


<!-- End of Related Page on "Miscellaneous Cascaded Attributes" -->
*/ 

/*!
\page a3d_faq Frequently Asked Questions

\section faq_navigating_the_doc Navigating the \COMPONENT_A3D_API_REF (this reference)
\par Question

I can't find information about a entity that appears as an argument. When I click the name of an entity, I see only a void type declaration. I want to see the data the
entity represents. 
For example, when I click on the \c A3DAsmModelFile argument in the following declaration:
\code
ASInt32  A3DAsmModelFileCreate (const A3DAsmModelFileData *pData, A3DAsmModelFile **ppModelFile) 
\endcode

I see a void type definition, such as the one that follows. How can I see the data in this entity?
\code
typedef void A3DAsmModelFile
\endcode

\par Answer

The void type declarations specify a type whose structure is concealed. 
But you can see the data used to construct the type by examining the 
\c A3D<i>Entity-name</i>Data structure. 
You can find that structure in this reference by navigating to the Data Structures tab of this document and then selecting the structure of interest.

PRC entities also have other attributes depending on their membership to a module, 
as described in \ref a3d_misc_cascaded_attributes.

<!-- End of Related Page on "FAQ" -->
*/ 

#ifndef __A3DPRCSDK_H__
#define __A3DPRCSDK_H__

#ifndef _H_CoreExpT

typedef signed char			ASInt8;
typedef short int				ASInt16;

#if POINTER_64_BITS
typedef int						ASInt32;
#else
typedef long int				ASInt32;
#endif

typedef unsigned char		ASUns8;
typedef unsigned short int	ASUns16;

#if POINTER_64_BITS
typedef unsigned int			ASUns32;
#else
typedef unsigned long int	ASUns32;
#endif

typedef ASUns16 ASBool;

#endif

#ifndef A3D_API
#ifdef __cplusplus
#	define A3D_API(returnvalue,name,params) extern "C" returnvalue name params
#	define A3D_API_CALL extern "C"
#else
#	define A3D_API 
#	define A3D_API_CALL 
#endif
#endif

/*!
\brief DLL Major Version

Major.Minor = 1.0 = Version of the DLL that this header file corresponds to.
*/
#define A3D_DLL_MAJORVERSION 2 
/*!
\brief DLL Minor Version

\sa A3D_DLL_MAJORVERSION
*/
#define A3D_DLL_MINORVERSION 2 

/*!
\brief Official SDK name
*/
#define A3D_DLL_NAME "A3DLIB"

/*!
\brief Copyright SDK
*/
#define A3D_DLL_COPYRIGHT "(c) Copyright 2009 Adobe Systems Incorporated. All rights reserved."

/*!
\brief Generic type for entities 
*/
typedef void A3DXXX;	

/*!
\brief Generic structure
*/
typedef struct 
{
	unsigned short m_usStructSize;	/*!< Reserved; should be initialized by \ref A3D_INITIALIZE_DATA */
} A3DXXXData;	

/*!
\brief Generic Access data function 

This function is as a template for other more specific functions. It does nothing.

Functions of this form create or free the \ref A3DXXXData structure specified in the argument argument. 
Whether these function create or free structures depends on the value of the input pointer:
\li If the input pointer \ref A3DXXX is non-NULL, then the function fills the structure \ref A3DXXXData with apropriate 
informations stored in \ref A3DXXX entity.
\li If the input pointer \ref A3DXXX is NULL, then the function frees the whole structure \ref A3DXXXData. Every kind
of information below is then lost. 
*/
A3D_API (int,A3DXXXGet,(const A3DXXX*, A3DXXXData*));	

/*!
\brief Generic create function

This function is as a template for other more specific functions. It does nothing.

Functions of this form create an entity \ref A3DXXX from the structure \ref A3DXXXData given as an argument.

You are responsible for freeing the \ref A3DXXXData structure.
*/
A3D_API (int,A3DXXXCreate,(const A3DXXXData*, A3DXXX**));			

#endif	/*	__A3DPRCSDK_H__ */
