/*******************************************************************************
* 
*  ADOBE SYSTEMS INCORPORATED
*  Copyright 2008 Adobe Systems Incorporated
*  All Rights Reserved
*  
*  NOTICE:  Adobe permits you to use, modify, and distribute this file in 
*  accordance with the terms of the Adobe license agreement accompanying it.  
*  If you have received this file from a source other than Adobe, then your use, 
*  modification, or distribution of it requires the prior written permission of 
*  Adobe.
* 
*******************************************************************************/

/*!
\file
\brief	Header file for the draw module
\author	ADOBE SYSTEMS INCORPORATED
\version	2.0
\date		February 2009
\par		(c) Copyright 2009 Adobe Systems Incorporated. All rights reserved.
*/

#ifndef __A3DPRCDRAWMODELFILE_H__
#define __A3DPRCDRAWMODELFILE_H__

#ifndef __A3DPRCMARKUP_H__
#error  A3DSDKMarkup.h must be included before current file
#endif

/*!
\defgroup a3d_draw_module Draw Module
\brief Draws model file entities using callback functions you provide

The functions and callback functions in this module allow you to 
draw PRC model files using drawing functions that you provide.
*/

/*!
\defgroup a3d_draw_functions_pointers Callback-Function Type Definitions
\ingroup a3d_draw_module
*/

/*!
\brief Enumeration for characterizing material 
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef enum 
{
	kA3DDrawMaterialAmbient, /*!< Ambient color. RGBA */
	kA3DDrawMaterialDiffuse, /*!< Diffuse color. RGBA */
	kA3DDrawMaterialSpecular, /*!< Specular color. RGBA */
	kA3DDrawMaterialEmission, /*!< Emission color. RGBA */
	kA3DDrawMaterialShininess /*!< Shininess color. Single value */
} A3DEDrawMaterialType;

/*!
\brief Enumeration for characterizing begin and end callbacks
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef enum 
{
	kA3DDrawBeginEndProductOccurrence, /*!< Begins or ends drawing a \ref A3DAsmProductOccurrence */
	kA3DDrawBeginEndRepresentationItem, /*!< Begins or ends drawing a \ref A3DRiRepresentationItem */
	kA3DDrawBeginEndMarkup /*!< Begins or ends drawing a \ref A3DMkpMarkup */
} A3DEDrawBeginEndType;

/*!
\brief Pushes the current matrix onto the stack
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncPushMatrix)(void);
/*!
\brief Pops the matrix off the stack
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncPopMatrix)(void);
/*!
\brief Multiplies the matrix on the top of the stack by another matrix
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncMultMatrix)(const A3DDouble adMatrix[16]);
/*!
\brief Begins drawing
\version 2.0

The \c pcName argument can be NULL if there is no name.
The \c uiTriangleCount argument is meaningful only 
when the \c eType argument has a value of \c kA3DDrawBeginEndRepresentationItem; 
otherwise, its value is 0.
\todo Doc
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DPFuncBegin)(A3DEDrawBeginEndType eType, const A3DUTF8Char* pcName, ASUns32 uiTrianglesCount);
/*!
\brief End
\todo Doc
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncEnd)(A3DEDrawBeginEndType eType);
/*!
\brief Returns all the points of a representation item tessellation
\todo Doc
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncSetTessellationPoints)(const A3DVector3dData* pasPoint, ASUns32 uiPointsSize);
/*!
\brief Projects the point
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncProject)(const A3DVector3dData* psPoint, A3DVector3dData* psResult);
/*!
\brief Un-projects the point
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncUnProject)(const A3DVector3dData* psPoint, A3DVector3dData* psResult);
/*!
\brief Draws a list of triangles
\ingroup a3d_draw_functions_pointers
\version 2.0

Each point of each triangle has its own normal.

*/
typedef void (*A3DPFuncTriangle)(const A3DVector3dData* pasNormals, const A3DVector3dData* pasPoints, ASUns32 uiPointsSize);
/*!
\brief Draws a fan of triangles
\version 2.0

Each point of the fan has its own normal.
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DPFuncTriangleFan)(const A3DVector3dData* pasNormals, const A3DVector3dData* pasPoints, ASUns32 uiPointsSize);
/*!
\brief Draws a stripe of triangles
\version 2.0

Each point of the stripe has its own normal.
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DPFuncTriangleStripe)(const A3DVector3dData* pasNormals, const A3DVector3dData* pasPoints, ASUns32 uiPointsSize);
/*!
\brief Draws a list of triangles where each triangle has only one normal
\version 2.0

Each triangle has only one normal. Therefore, the number of normals is \c uiPointsSize/3.
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DPFuncTriangleOneNormal)(const A3DVector3dData* pasNormals, const A3DVector3dData* pasPoints, ASUns32 uiPointsSize);
/*!
\brief Draws a fan of triangles with one normal
\version 2.0

The fan has only one normal, psNormal.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncTriangleFanOneNormal)(const A3DVector3dData* psNormal, const A3DVector3dData* pasPoints, ASUns32 uiPointsSize);
/*!
\brief Draws a stripe of triangles with one normal
\version 2.0

The stripe has only one normal, which is identified by the \c psNormal argument.
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DPFuncTriangleStripeOneNormal)(const A3DVector3dData* psNormal, const A3DVector3dData* pasPoints, ASUns32 uiPointsSize);
/*!
\brief Draws a list of textured triangles 
\version 2.0

Each point of each triangle has its own normal.
\note Textures are not yet implemented
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DPFuncTriangleTextured)(const A3DVector3dData* pasNormals, const A3DVector3dData* pasPoints, ASUns32 uiPointsSize);
/*!
\brief Draws a fan of textured triangles
\version 2.0

Each point of the fan has its own normal
\note Textures are not yet implemented
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DPFuncTriangleFanTextured)(const A3DVector3dData* pasNormals, const A3DVector3dData* pasPoints, ASUns32 uiPointsSize);
/*!
\brief Draws a stripe of triangles
\version 2.0

Each point of the stripe has its own normal.
\note Textures are not yet implemented
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DPFuncTriangleStripeTextured)(const A3DVector3dData* pasNormals, const A3DVector3dData* pasPoints, ASUns32 uiPointsSize);
/*!
\brief Draws a list of textured triangles where each triangle has only one normal
\version 2.0

Each triangle has only one normal. Therefore, the number of normals is \c uiPointsSize/3.
\note Textures are not yet implemented
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DPFuncTriangleOneNormalTextured)(const A3DVector3dData* pasNormals, const A3DVector3dData* pasPoints, ASUns32 uiPointsSize);
/*!
\brief Draws a fan of textured triangles, where each triangle has only one normal
\version 2.0

The fan has only one normal, which is \c psNormal.
\note Textures are not yet implemented
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DPFuncTriangleFanOneNormalTextured)(const A3DVector3dData* psNormal, const A3DVector3dData* pasPoints, ASUns32 uiPointsSize);
/*!
\brief Draws a stripe of triangles with one normal
\version 2.0

The stripe has only one normal, which is returned in the \c psNormal argument.
\note Textures are not yet implemented
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DPFuncTriangleStripeOneNormalTextured)(const A3DVector3dData* psNormal, const A3DVector3dData* pasPoints, ASUns32 uiPointsSize);
/*!
\brief Defines the material to be used for all subsequent entities.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncMaterial)(A3DEDrawMaterialType eType, const A3DDouble* pdValues, ASUns32 uiValuesSize);
/*!
\brief Requests the projection, modelview matrix and the viewport (See classical OGL definition for more informations)
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncGetDrawContext)(A3DDouble adProjection[16], A3DDouble adModelView[16], ASInt32 aiViewport[4]);
/*!
\brief Draws a list of triangles without normals, for markups
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncMarkupTriangle)(const A3DDouble* pdPoints, ASUns32 uiPointSize);
/*!
\brief Sets the environment to draw with screen coordinates
\todo Doc
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncBeginFrameDraw)(const A3DVector3dData* psPoint3d, ASBool bIsZoomable, A3DDouble dFixedSize);
/*!
\brief Ends the draw with screen coordinates
\todo Doc
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncEndFrameDraw)(void);
/*!
\brief Sets the environment to draw with a fixed size
\todo Doc
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncBeginFixedSize)(const A3DVector3dData* psPoint3d);
/*!
\brief Ends the draw with fixed size
\todo Doc
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncEndFixedSize)(void);
/*!
\brief Draws a cylinder
\todo Doc
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncCylinder)(A3DDouble dBaseRadius, A3DDouble dTopRadius, A3DDouble dHeight);
/*!
\brief Draws a polygon
\todo Doc
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncPolygon)(const A3DDouble* pdPoints, ASUns32 uiPointSize);
/*!
\brief Sets the environment to draw with a line width
\todo Doc
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncBeginLineWidth)(A3DDouble dWidth);
/*!
\brief Ends the draw with a line width
\todo Doc
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncEndLineWidth)(void);
/*!
\brief Draws a list of points
\todo Doc
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncPoint)(const A3DDouble* pdPoints, ASUns32 uiPointSize);
/*!
\brief Defines a font
\todo Doc
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncFont)(const A3DFontKeyData* psFontKeyData);
/*!
\brief Sets the environment to draw with a line stipple
\todo Doc
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncBeginLineStipple)(const A3DGraphStyleData* psGraphStyleData);
/*!
\brief Ends the draw with a line stipple
\todo Doc
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncEndLineStipple)(void);
/*!
\brief Draws a symbol at the 3d position
\todo Doc
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncSymbol)(const A3DGraphVPicturePatternData* psPatternData, const A3DVector3dData* psPosition);
/*!
\brief Draws a polyline
\todo Doc
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncPolyLine)(const A3DDouble* pdPoints, ASUns32 uiPointSize);
/*!
\brief Draws a text at current position
\todo Doc
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncText)(const A3DUTF8Char* pcBuffer, A3DDouble dWidth, A3DDouble dHeight);
/*!
\brief Draws a pattern
\todo Doc
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncPattern)(ASUns32 uiLoopsSize, ASUns32 uiPatternId, ASUns32 uiFilledMode, ASUns32 uiBehaviour, const A3DDouble* pdPoints, const ASUns32* puiLoopsPointSize);
/*!
\brief Draws a picture at current position
\todo Doc
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DPFuncPicture)(const A3DGraphPictureData* psPictureData);

/*!
\defgroup drawcallbacks A3DDrawCallbacksData Structure 
\brief Structure for specifying callback functions for drawing
\ingroup a3d_draw_module
\version 2.0

Use this structure to define the callback functions \COMPONENT_A3D_LIBRARY will use to draw a model file entity.
*/
typedef struct
{
	ASUns16 m_usStructSize;	/*!< Reserved; must be initialized using the \ref A3D_INITIALIZE_DATA macro. */
	A3DPFuncPushMatrix m_pfuncPushMatrix; /*!< */
	A3DPFuncPushMatrix m_pfuncPopMatrix; /*!< */
	A3DPFuncMultMatrix m_pfuncMultMatrix; /*!< */
	A3DPFuncBegin m_pfuncBegin; /*!< */
	A3DPFuncEnd m_pfuncEnd; /*!< */
	A3DPFuncSetTessellationPoints m_pfuncSetTessellationPoints; /*!< */
	A3DPFuncTriangle m_pfuncTriangle; /*!< */
	A3DPFuncTriangleFan m_pfuncTriangleFan; /*!< */
	A3DPFuncTriangleStripe m_pfuncTriangleStripe; /*!< */
	A3DPFuncTriangleOneNormal m_pfuncTriangleOneNormal; /*!< */
	A3DPFuncTriangleFanOneNormal m_pfuncTriangleFanOneNormal; /*!< */
	A3DPFuncTriangleStripeOneNormal m_pfuncTriangleStripeOneNormal; /*!< */
	A3DPFuncTriangleTextured m_pfuncTriangleTextured; /*!< */
	A3DPFuncTriangleFanTextured m_pfuncTriangleFanTextured; /*!< */
	A3DPFuncTriangleStripeTextured m_pfuncTriangleStripeTextured; /*!< */
	A3DPFuncTriangleOneNormalTextured m_pfuncTriangleOneNormalTextured; /*!< */
	A3DPFuncTriangleFanOneNormalTextured m_pfuncTriangleFanOneNormalTextured; /*!< */
	A3DPFuncTriangleStripeOneNormalTextured m_pfuncTriangleStripeOneNormalTextured; /*!< */
	A3DPFuncMaterial m_pfuncMaterial; /*!< */
	A3DPFuncGetDrawContext m_pfuncGetDrawContext; /*!< */
	A3DPFuncMarkupTriangle m_pfuncMarkupTriangle; /*!< */
	A3DPFuncUnProject m_pfuncUnProject; /*!< */
	A3DPFuncBeginFrameDraw m_pfuncBeginFrameDraw; /*!< */
	A3DPFuncEndFrameDraw m_pfuncEndFrameDraw; /*!< */
	A3DPFuncBeginFixedSize m_pfuncBeginFixedSize; /*!< */
	A3DPFuncEndFixedSize m_pfuncEndFixedSize; /*!< */
	A3DPFuncCylinder m_pfuncCylinder; /*!< */
	A3DPFuncPolygon m_pfuncPolygon; /*!< */
	A3DPFuncBeginLineWidth m_pfuncBeginLineWidth; /*!< */
	A3DPFuncEndLineWidth m_pfuncEndLineWidth; /*!< */
	A3DPFuncPoint m_pfuncPoint; /*!< */
	A3DPFuncFont m_pfuncFont; /*!< */
	A3DPFuncBeginLineStipple m_pfuncBeginLineStipple; /*!< */
	A3DPFuncEndLineStipple m_pfuncEndLineStipple; /*!< */
	A3DPFuncSymbol m_pfuncSymbol; /*!< */
	A3DPFuncPolyLine m_pfuncPolyLine; /*!< */
	A3DPFuncText m_pfuncText; /*!< */
	A3DPFuncPattern m_pfuncPattern; /*!< */
	A3DPFuncPicture m_pfuncPicture; /*!< */
} A3DDrawCallbacksData;

/*!
\brief Initializes the callbacks used for drawing
\ingroup a3d_draw_module
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DDrawInitCallbacks,( A3DDrawCallbacksData* psCallbacks));

/*!
\defgroup a3d_drawflags Flags for Drawing Model File Entities
\version 2.0

These flags apply to the \ref A3DDraw and \ref A3DDrawGetBoundingBox functions.
\ingroup a3d_draw_module
@{
*/
#define kA3DDraw3D		0x1 /*!< Draws only 3D tessellation */
#define kA3DDrawMarkups	0x2 /*!< Draws only markups */
/*!
@} <!-- End of a3d_drawflags -->
*/

/*!
\brief Draws the model file entities, using the callbacks defined by \ref A3DDrawInitCallbacks
\ingroup a3d_draw_module
\version 2.0

To set the \c uiDrawFlags argument, use the flags defined in \ref a3d_drawflags.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DDraw,(const A3DAsmModelFile* pModelFile, ASUns32 uiDrawFlags));

/*!
\brief Calculates the bounding box of the model file entity, without using any callback functions
\version 2.0

To set the \c uiDrawFlags argument, use the flags defined in \ref a3d_drawflags.
\ingroup a3d_draw_module
\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DDrawGetBoundingBox,(const A3DAsmModelFile* pModelFile, A3DBoundingBoxData* psBoundingBox, ASUns32 uiDrawFlags));

#endif // #ifndef __A3DPRCDRAWMODELFILE_H__
