/*******************************************************************************
* 
*  ADOBE SYSTEMS INCORPORATED
*  Copyright 2008 Adobe Systems Incorporated
*  All Rights Reserved
*  
*  NOTICE:  Adobe permits you to use, modify, and distribute this file in 
*  accordance with the terms of the Adobe license agreement accompanying it.  
*  If you have received this file from a source other than Adobe, then your use, 
*  modification, or distribution of it requires the prior written permission of 
*  Adobe.
* 
*******************************************************************************/

/*!
\file
\brief	Header file for the error codes module
\author	ADOBE SYSTEMS INCORPORATED
\version	2.0
\date		February 2009
\par		(c) Copyright 2008 Adobe Systems Incorporated. All rights reserved.

As a general rule, every negative value for return code <b>must be handled as an
error</b>, and not simply passed by. When possible, a brief explanation on every 
case has been provided. 

Please inform Adobe Systems, Incorporated of any undocumented errors that you encounter.

*/

#ifndef __A3DPRCERRORCODES_H__
#define __A3DPRCERRORCODES_H__

/*******************************************************************************
* 
*  CONSTANTS
* 
*******************************************************************************/

#ifndef FALSE
#	define FALSE	0 /*!< Boolean standard false value. */
#endif
#ifndef TRUE
#	define TRUE		1 /*!< Boolean standard true value. */
#endif

#define A3D_MAX_BUFFER	2048	/*!< Maximum length for character string. */
#define A3D_MAX_PARAMETER	12345	/*!< Maximum parameter used for parametric space coordinates. */
#define A3D_NOT_IMPLEMENTED	-33	/*!< Local code, which is not yet implemented. */

/*******************************************************************************
*
*  MACROS
*
*******************************************************************************/
/*!
\attention Your code must include memset: malloc before this file.
*/
#define A3D_INITIALIZE_DATA( s ) {memset(&s,0,sizeof(s)); s.m_usStructSize = sizeof(s);}
#define A3D_INVALID_DATA_STRUCT_SIZE		-100	/*!< \ref A3D_INITIALIZE_DATA not called or bad alignment in input struct. */
#define A3D_INVALID_DATA_STRUCT_NULL		-101	/*!< Input struct function cannot be NULL. */
#define A3D_INVALID_ENTITY_NULL				-102	/*!< Input entity function cannot be NULL. */
#define A3D_INVALID_ENTITY_TYPE				-103	/*!< Input entity function is of bad type. */
#define A3D_INVALID_FILE_FORMAT				-104	/*!< Input file does not correspond to a valid PRC file. */

/*******************************************************************************
* 
*  GENERAL ERROR CODES
* 
*******************************************************************************/

#define A3D_SUCCESS	0				/*!< Successfull return value. */
#define A3D_ERROR	-1				/*!< General error. */
#define A3D_EXCEPTION	-666		/*!< Caught execption. */
#define A3D_ALLOC_FATAL_ERROR -10	/*!< Allocation has failed. Major error. */

/*******************************************************************************
* 
*  BASE ERROR CODES
* 
*******************************************************************************/

#define A3D_INITIALIZE_ALREADY_CALLED				-200	/*!< \ref A3DDllInitialize already called; bad sequence with the \ref A3DDllTerminate function. */
#define A3D_INITIALIZE_NOT_CALLED					-201	/*!< \ref A3DDllInitialize must be called before calling the \ref A3DDllTerminate function.*/
#define A3D_INITIALIZE_BAD_VALUES					-202	/*!< Initialization led to bad internal values for globals. Fatal error. Reinitialize program */
#define A3D_INITIALIZE_CANNOT_ACCESS_ACROBAT_ENV	-203	/*!< Cannot find main directory for Acrobat installation. */
#define A3D_INITIALIZE_CANNOT_ACCESS_ACROBAT_3D		-204	/*!< Cannot find main directory for \PRODUCT_ACRO3D installation */

/*******************************************************************************
* 
*  LOADING ERROR CODES
* 
*******************************************************************************/

#define A3D_LOAD_INITIALIZATION_FAILURE			-300	/*!< Cannot initialize reading context. */
#define A3D_LOAD_CANNOT_OPEN_FILE					-301	/*!< Cannot open file. */
#define A3D_LOAD_READING_ERROR						-302	/*!< Internal reading error. */
#define A3D_LOAD_BUILD_ERROR							-303	/*!< Internal reading error - Invalid PRC file. */
#define A3D_LOAD_PDFLINIT_ERROR						-304	/* Error while initializing PDFL. */
#define A3D_LOAD_INVALID_ANNOT_INDEX				-305	/* Input annotation index is invalid. */
#define A3D_LOAD_INVALID_ANNOT						-306	/* Input annotation is invalid. */
#define A3D_LOAD_EMPTY_FILE							-307	/*!< Input file is empty. */
#define A3D_LOAD_NO_3D									-308	/* Input file does not contains any 3D annotation. */

/*******************************************************************************
* 
*  WRITING ERROR CODES
* 
*******************************************************************************/

#define A3D_WRITE_ERROR									-400	/*!< Error while writing PRC file. */

/*******************************************************************************
* 
*  GEOMETRY ERROR CODES
* 
*******************************************************************************/

#define A3D_CRV_CANNOT_REPARAMETERIZE				-990	/*!< Input parameterization data cannot be used to reparameterize the curve  */

#define A3D_CRV_CANNOT_ACCESS_CANONICAL				-1000 	/*!< Cannot access the canonical form for an \ref A3DCrvBase. */
#define A3D_CRV_LINE_CANNOT_CREATE_CANONICAL		-1001 	/*!< Cannot create canonical form for an \ref A3DCrvLine. */

#define A3D_CRV_CIRCLE_CANNOT_CREATE_CANONICAL		-1011	/*!< Cannot create canonical form for an \ref A3DCrvCircle. */

#define A3D_CRV_ELLIPSE_CANNOT_CREATE_CANONICAL		-1021	/*!< Cannot create canonical form for an \ref A3DCrvEllipse. */

#define A3D_CRV_OFFSET_CANNOT_CREATE_CANONICAL		-1041	/*!< Cannot create canonical form for an \ref A3DCrvOffset. */

#define A3D_CRV_EQUATION_CANNOT_CREATE_CANONICAL	-1051	/*!< Cannot create canonical form for an \ref A3DCrvEquation. */
#define A3D_CRV_TRANSFORM_CANNOT_CREATE_CANONICAL	-1052	/*!< Cannot create canonical form for an \ref A3DCrvTransform. */
#define A3D_CRV_CRVONSURF_CANNOT_CREATE_CANONICAL	-1053	/*!< Cannot create canonical form for an \ref A3DCrvOnSurf. */
#define A3D_CRV_HELIX_CANNOT_CREATE_CANONICAL		-1054	/*!< Cannot create canonical form for an \ref A3DCrvHelix. */
#define A3D_CRV_POLYLINE_CANNOT_CREATE_CANONICAL	-1061	/*!< Cannot create canonical form for an \ref A3DCrvPolyLine. */

#define A3D_CRV_NURBS_CANNOT_ACCESS_INTERNAL		-1070	/*!< Cannot access the internal form for curve. */
#define A3D_CRV_NURBS_CANNOT_ACCESS_CONTROL_POINTS	-1072	/*!< Cannot access the control points for an \ref A3DCrvNurbs. */
#define A3D_CRV_NURBS_INCONSISTENT_DATA				-1073	/*!< NURBS curve data is inconsistent (control points number + degree + 1 != knots number ). */
#define A3D_CRV_NURBS_CANNOT_ACCESS_APPROX			-1074	/*!< Cannot access the NURBS approximation for curve. */
#define A3D_CRV_NURBS_TOO_TINY_TOLERANCE			-1075	/*!< Tolerance cannot be less then or equal to zero. */

#define A3D_CRV_COMPOSITE_CANNOT_CREATE_CANONICAL	-1081 	/*!< Cannot create the canonical form for an \ref A3DCrvComposite. */

#define A3D_SRF_CANNOT_ACCESS_CANONICAL				-1500 	/*!< Cannot access the canonical form for an \ref A3DSurfBase entity. */
#define A3D_SRF_CANNOT_CREATE_CANONICAL				-1501 	/*!< Cannot create the canonical form for an \ref A3DSurfBase entity. */
#define A3D_SRF_NURBS_CANNOT_ACCESS_APPROX			-1502 	/*!< Cannot access the NURBS approximation for a surface. */
#define A3D_SRF_NURBS_CANNOT_KEEP_PARAMETERIZATION	+1503 	/*!< Cannot keep the parameterization during the NURBS approximation for a surface 
																This is a warning. You must still free the structure. */
#define A3D_SRF_NURBS_TOO_TINY_TOLERANCE			-1504 	/*!< Tolerance for the NURBS approximation is too tight. Try with looser tolerance. */

#define A3D_SRF_PLANE_CANNOT_CREATE_CANONICAL		-1521 	/*!< Cannot create the canonical form for an \ref A3DSurfPlane entity. */
#define A3D_SRF_INVALID_PARAMETERS					-1522 	/*!< Input parameterization data is invalid. */

/*******************************************************************************
* 
*  ATTRIBUTES/GLOBAL DATA ERROR CODES
* 
*******************************************************************************/

#define A3D_DEFAULT_LAYER					((ASUns16)-1) /*!< */
#define A3D_DEFAULT_TRANSPARENCY			((ASUns8)-1) /*!< */
#define A3D_DEFAULT_LINE_WIDTH			((A3DDouble)0.1) /*!< */

#define A3D_DEFAULT_COLOR_INDEX			((ASUns32)-1) /*!< */
#define A3D_DEFAULT_PATTERN_INDEX		((ASUns32)-1) /*!< */
#define A3D_DEFAULT_STYLE_INDEX			((ASUns16)-1) /*!< */

#define A3D_DEFAULT_LINEPATTERN_INDEX				((ASUns32)-1) /*!< */
#define A3D_DEFAULT_MATERIAL_INDEX					((ASUns32)-1) /*!< */
#define A3D_DEFAULT_PICTURE_INDEX					((ASUns32)-1) /*!< */
#define A3D_DEFAULT_TEXTURE_DEFINITION_INDEX		((ASUns32)-1) /*!< */
#define A3D_DEFAULT_TEXTURE_APPLICATION_INDEX	((ASUns32)-1) /*!< */

#define A3D_EMPTY_STRING				-2000	/*!< Name is an empty string. */
#define A3D_DEFAULT_COLOR				-2001	/*!< Input index corresponds to the default color. */
#define A3D_DEFAULT_LINEPATTERN		-2002	/*!< Input index corresponds to the default line pattern. */
#define A3D_DEFAULT_STYLE				-2003	/*!< Input index corresponds to the default style. */
#define A3D_DEFAULT_MATERIAL			-2004	/*!< Input index corresponds to the default material. */
#define A3D_DEFAULT_PICTURE			-2005	/*!< Input index corresponds to the default picture. */
#define A3D_DEFAULT_PATTERN			-2006	/*!< Input index corresponds to the default pattern. */

#define A3D_BASE_BAD_ENTITY_TYPE						-2050	/*!< Cannot access base data for an \ref A3DEntity entity, probably because the type is incorrect. */
#define A3D_BASEWITHGRAPHICS_BAD_ENTITY_TYPE		-2051	/*!< Cannot access base with graphics data for an \ref A3DEntity entity, probably because the type is incorrect. */
#define A3D_BASEWITHGRAPHICS_INCONSISTENT_EMPTY	-2052	/*!< Input data corresponds to an empty \ref A3DGraphics. */

#define A3D_INVALID_COLOR_INDEX						-2101	/*!< Index does not correspond to a valid color. */
#define A3D_INVALID_LINEPATTERN_INDEX				-2102	/*!< Index does not correspond to a valid line pattern. */
#define A3D_INVALID_STYLE_INDEX						-2103	/*!< Index does not correspond to a valid style. */
#define A3D_INVALID_MATERIAL_INDEX					-2104	/*!< Index does not correspond to a valid material. */
#define A3D_INVALID_PICTURE_INDEX					-2105	/*!< Index does not correspond to a valid picture. */
#define A3D_INVALID_PATTERN_INDEX					-2106	/*!< Index does not correspond to a valid pattern. */
#define A3D_INVALID_TEXTURE_DEFINITION_INDEX		-2107	/*!< Index does not correspond to a texture definition. */
#define A3D_INVALID_TEXTURE_APPLICATION_INDEX	-2108	/*!< Index does not correspond to a texture application. */

#define A3D_INVALID_PICTURE_FORMAT					-2200	/*!<  Input picture format is invalid. */

/*******************************************************************************
* 
*  REPRESENTATION ITEMS ERROR CODES
* 
*******************************************************************************/

#define A3D_RI_CANNOT_ACCESS_TESS								-2500	/*!< Cannot access the \ref A3DTessBase for the current \ref A3DRiRepresentationItem. */
#define A3D_RI_CANNOT_ATTACH_BREPDATA							-2501	/*!< Cannot attach the current \ref A3DTopoBrepData to an \ref A3DRiRepresentationItem. */
#define A3D_RI_CANNOT_ATTACH_TESS								-2502	/*!< Cannot attach the current \ref A3DTessBase to an \ref A3DRiRepresentationItem. */

#define A3D_RI_CRV_INCONSISTENT_DATA			-2510	/*!< Input data corresponds to an empty \ref A3DRiCurve. */
#define A3D_RI_PLANE_INCONSISTENT_DATA			-2511 /*!< Input data corresponds to an empty \ref A3DRiPlane. */
#define A3D_RI_BREPMODEL_INCONSISTENT_DATA	-2512	/*!< Input data corresponds to an empty \ref A3DRiBrepModel. */

#define A3D_SET_INCONSISTENT_EMPTY								-2520	/*!< Input data corresponds to an empty \ref A3DRiSet. */

#define A3D_CSYS_INCONSISTENT_EMPTY								-2530	/*!< Input data corresponds to an empty \ref A3DRiCoordinateSystem. */

#define A3D_POINTSET_BADSIZE										-2540	/*!< Input data defines an \ref A3DRiPointSet with an incorrect size. */

/*******************************************************************************
* 
*  STRUCTURE ERROR CODES
* 
*******************************************************************************/

#define A3D_MODELFILE_INCONSISTENT_EMPTY						-3000	/*!< Input data corresponds to an empty \ref A3DAsmModelFile. Forbidden. */
#define A3D_MODELFILE_INCONSISTENT_UNIT						-3001	/*!< Input data corresponds to an invalid unit for an \ref A3DAsmModelFile. Unit must be greater than 0. */

#define A3D_PRODUCTOCCURENCE_INCONSISTENT_EMPTY				-3010	/*!< Input data corresponds to an empty \ref A3DAsmProductOccurrence. Forbidden. */
#define A3D_PRODUCTOCCURENCE_INCONSISTENT_PART_EXTERNAL	-3011 /*!< Input data cannot contain both an external part and a part.*/

#define A3D_PARTDEFINITION_INCONSISTENT_EMPTY				-3020	/*!< Input data corresponds to an empty \ref A3DAsmPartDefinition. Forbidden. */

#define A3D_ENTITYREFERENCE_INCONSISTENT_REFERENCE			-3030	/*!< Input data corresponds to an empty \ref A3DEntity. Forbidden. */
#define A3D_ENTITYREFERENCE_INCONSISTENT						-3031	/*!< Input data corresponds to bad \ref A3DMiscEntityReference */


/*******************************************************************************
* 
*  UTILS DATA ERROR CODES
* 
*******************************************************************************/

#define A3D_INTERVAL_INCONSISTENT_DATA						-3500	/*!< Interval is inconsistent for current operation. */

#define A3D_TRANSFORMATION3D_NON_ORTHO_NOR_UNIT			-3520	/*!< Input data defines an inconsistent \ref A3DMiscCartesianTransformation. */
#define A3D_TRANSFORMATION3D_INCONSISTENT					-3521	/*!< Inconsistent \ref A3DMiscCartesianTransformation. */

/*******************************************************************************
* 
*  TOPOLOGY ERROR CODES
* 
*******************************************************************************/

#define A3D_BREPDATA_INCONSISTENT_DATA								-4000	/*!< Input data corresponds to an empty \ref A3DTopoBrepData. */
#define A3D_BREPDATA_CANNOT_CREATE									-4001	/*!< Cannot create \ref A3DTopoBrepData with the input data. */
#define A3D_BREPDATA_CANNOT_GETBOUNDINGBOX						-4002	/*!< Cannot get bounding box. */
#define A3D_BREPDATA_CANNOT_SETBOUNDINGBOX						-4003	/*!< Cannot set bounding box. */
#define A3D_TOPOCONTEXT_INVALID										-4004	/*!< Cannot determine the associated \ref A3DTopoContext. */

#define A3D_CONNEX_INCONSISTENT_DATA								-4010	/*!< Input data corresponds to an empty \ref A3DTopoConnex. */

#define A3D_SHELL_INCONSISTENT_DATA									-4020	/*!< Input data corresponds to an empty \ref A3DTopoShell. */
#define A3D_SHELL_BAD_ORIENTATION_DATA								-4021	/*!< Input data for orientation is incorrect. */
#define A3D_SHELL_CANNOT_CREATE										-4022	/*!< Cannot create \ref A3DTopoShell with the input data. */

#define A3D_FACE_INCONSISTENT_DATA									-4030	/*!< Input data corresponds to an empty \ref A3DTopoFace. */
#define A3D_FACE_CANNOT_LIFT_CRV_INTERNAL1						-4031	/*!< Cannot access the internal data of an \ref A3DTopoFace. Internal Adobe information: Sector 1 */
#define A3D_FACE_CANNOT_LIFT_CRV_INTERNAL2						-4032	/*!< Cannot access the to internal data of an \ref A3DTopoFace. Internal Adobe information: Sector 2 */
#define A3D_FACE_CANNOT_LIFT_CRV_INTERNAL3						-4033	/*!< Cannot access to the internal data of an \ref A3DTopoFace. Internal Adobe information: Sector 3 */
#define A3D_FACE_INCONSISTENT_DOMAIN								-4034	/*!< Inconsistent domain for underlying surface of \ref A3DTopoFace. */

#define A3D_LOOP_UNKNOW_OUTER_INDEX									((ASUns32)-1) /*!< Outer loop is unknown. */
#define A3D_LOOP_INCONSISTENT_DATA									-4400	/*!< Input data corresponds to an empty \ref A3DTopoLoop. */
#define A3D_LOOP_BAD_ORIENTATION_DATA								-4401	/*!< Input data for orientation is incorrect. */
#define A3D_LOOP_CANNOT_COMPUTE_ORIENTATION						-4402	/*!< Cannot determine the loop orientation with the input data. */
#define A3D_LOOP_CANNOT_FIND_OUTER_LOOP							-4403	/*!< Cannot determine the outer loop index with the input data. */

#define A3D_COEDGE_INCONSISTENT_DATA								-4500	/*!< Input data corresponds to an empty \ref A3DTopoCoEdge. */
#define A3D_COEDGE_BAD_ORIENTATION_DATA							-4501	/*!< Input data for orientation is incorrect. */

#define A3D_EDGE_BAD_INTERVAL											-4600	/*!< Curve interval is not contained by the interval specified by the input data. */
#define A3D_EDGE_INCONSISTENT_DATA									-4601	/*!< Input data corresponds to an empty \ref A3DTopoWireEdge. */
#define A3D_EDGE_BAD_3DCURVE											-4602 /*!< Pointer to 3D space curve does not correspond to a curve. */

#define A3D_SINGLEWIREBODY_INCONSISTENT_DATA						-4700	/*!< Input data corresponds to an empty \ref A3DTopoSingleWireBody. */
#define A3D_SINGLEWIREBODY_CANNOT_CREATE							-4701	/*!< Cannot create an \ref A3DTopoSingleWireBody with the input data. */

/*******************************************************************************
* 
*  TESSELATION ERROR CODES
* 
*******************************************************************************/

#define A3D_TESS3D_NORMALS_INCONSISTENT_DATA						-7010 /*!< Input data corresponds to a null \ref A3DTess3DData::m_pdNormals. */
#define A3D_TESS3D_NORMALS_BAD_SIZE									-7011 /*!< Input data corresponds to an invalid size \ref A3DTess3DData::m_uiNormalSize. */
#define A3D_TESS3D_FACE_INCONSISTENT_DATA							-7020 /*!< Input data corresponds to null \ref A3DTess3DData::m_psFaceTessData. */

#define A3D_TESSFACE_TRIANGULATED_INCONSISTENT_DATA			-7030 /*!< Input data corresponds to null \ref A3DTessFaceData::m_uiStyleIndexesSize. */
#define A3D_TESSFACE_TRIANGULATED_INCONSISTENT_EMPTY			-7031 /*!< Input data corresponds to an empty \ref A3DTessFaceData::m_uiSizesTriangulatedSize. */
#define A3D_TESSFACE_USEDENTITIES_BAD_TYPE						-7032 /*!< Input data corresponds to an invalid type \ref A3DTessFaceData::m_usUsedEntitiesFlags. */
#define A3D_TESSFACE_STARTTRIANGULATED_INCONSISTENT_DATA		-7033 /*!< Input data corresponds to an invalid \ref A3DTessFaceData::m_uiStartTriangulated. */
#define A3D_TESSFACE_STARTWIRE_INCONSISTENT_DATA				-7034 /*!< Input data corresponds to an invalid \ref A3DTessFaceData::m_uiStartWire. */
#define A3D_TESSFACE_STYLEINDEXESSIZE_INCONSISTENT_DATA		-7035 /*!< Input data corresponds to an invalid \ref A3DTessFaceData::m_uiStyleIndexesSize. */
#define A3D_TESSFACE_RGBAVERTICESSIZE_INCONSISTENT_DATA		-7036 /*!< Input data corresponds to an invalid \ref A3DTessFaceData::m_uiRGBAVerticesSize. */

#define A3D_TESSWIRE_RGBAVERTICESSIZE_INCONSISTENT_DATA		-7052 /*!< Input data corresponds to an invalid \ref A3DTess3DWireData::m_uiRGBAVerticesSize. */

#define A3D_TESSMARKUP_HAS_INVALID_FONTKEY						-7060 /*!< Input data contains an invalid font key. Call \ref A3DGlobalFontKeyCreate. */
#define A3D_TESSMARKUP_HAS_INVALID_TEXT_INDEX					-7061 /*!< Input data contains an invalid text index. */

#define A3D_TESSBASE_POINTS_INCONSISTENT_DATA					-7070 /*!< Input data corresponds to null \ref A3DTessBaseData::m_pdCoords. */
#define A3D_TESSBASE_POINTS_BAD_SIZE								-7071 /*!< Input data corresponds to an invalid size \ref A3DTessBaseData::m_uiCoordSize. */
#define A3D_TESSBASE_INCONSISTENT									-7072 /*!< Input data corresponds to bad \ref A3DTessBase. */

#define A3D_TESSBASE_BAD_TYPE											-7100 /*!< Input data is not a valid value. */
#define A3D_TESSBASE_BAD_INIT											-7101 /*!< Tessellation facilities is not initialized. */
#define A3D_TESSBASE_BAD_ONENORMAL									-7102 /*!< Tessellation facilities is consistent. */
#define A3D_TESSBASE_BAD_INDEX										-7103 /*!< Input index is out of bounds. */
#define A3D_TESSBASE_BAD_VERTEX										-7104 /*!< Input indices are invalid for vertex. */

/*******************************************************************************
* 
*  TOOLS ERROR CODES
* 
*******************************************************************************/

#define A3D_TOOLS_NURBSCONVERT_SURFACE_FAILURE					-8000	/*!< Operation failed while converting a surface to NURBS. */
#define A3D_TOOLS_NURBSCONVERT_UV_FAILURE							-8001	/*!< Operation failed while computing a space parametric curve. */
#define A3D_TOOLS_NURBSCONVERT_3D_FAILURE							-8002	/*!< Operation failed while computing a 3D space curve. */
#define A3D_TOOLS_NURBSCONVERT_SPLIT_FAILURE						-8003	/*!< Operation failed while splitting a surface at seam. */
#define A3D_TOOLS_NURBSCONVERT_GENERAL_FAILURE					-8004	/*!< General failure on NURBS conversion operation. */

/*******************************************************************************
* 
*  MARKUPS ERROR CODES
* 
*******************************************************************************/

#define A3D_ANNOTATIONSET_INCONSISTENT_EMPTY						-9000	/*!< Input data corresponds to an empty \ref A3DMkpAnnotationSet. */
#define A3D_ANNOTATIONITEM_INCONSISTENT_EMPTY					-9005	/*!< Input data corresponds to an empty \ref A3DMkpAnnotationItem. */
#define A3D_MARKUP_CANNOT_ATTACH_TESS								-9020	/*!< Cannot attach current \ref A3DTessBase to \ref A3DMkpMarkup. */
#define A3D_MARKUP_CANNOT_ACCESS_TESS								-9021	/*!< Cannot access the \ref A3DTessBase for the current \ref A3DRiRepresentationItem. */

#define A3D_MARKUP_CANNOT_ACCESS_FONT								-9040 /*!< The font information cannot be retrieved. */
#define A3D_MARKUP_CANNOT_CREATE_FONTKEY							-9041 /*!< The font key cannot be created. */
#define A3D_MARKUP_INVALID_FONTKEY									-9042 /*!< The font key given or retrieved is invalid. */

/*******************************************************************************
* 
*  LEVEL 2 READING ERROR CODES
* 
*******************************************************************************/

#define A3D_ASMLEVEL2_CANNOT_CREATE_PRCFILE			-10000	/*!< Cannot create intermediate PRC file. */
#define A3D_ASMLEVEL2_CANNOT_ACCESS_CADFILE			-10001	/*!< Cannot access input CAD file. */
#define A3D_ASMLEVEL2_CANNOT_ACCESS_XMLSETTINGS		-10002	/*!< Cannot access input XML settings file. */
#define A3D_ASMLEVEL2_CANNOT_ACCESS_MULTIFILE		-10003	/*!< Cannot find definition for multimodels output file inside XML file. */
#define A3D_ASMLEVEL2_BAD_SYNTAX_XMLFILE				-10004	/*!< Bad syntax inside XML file. Check in documentation. */
#define A3D_ASMLEVEL2_CANNOT_ACCESS_A3DCONVERTER	-10010	/*!< Cannot find A3DConverter.exe in Acrobat installation. */

/*******************************************************************************
*
*	MATH FUNCTIONS
*
*******************************************************************************/

#define A3D_MATH_INCONSISTENT_DATA					-40000	/*!< Invalid input data. */
#define A3D_MATH_FRACTION_INCONSISTENT_NUMERATOR	-40001	/*!< Invalid function for numerator. */
#define A3D_MATH_FRACTION_INCONSISTENT_DENOMINATOR	-40002	/*!< Invalid function for denominator. */

/*******************************************************************************
* 
*  MISC FUNCTIONS ERROR CODES
* 
*******************************************************************************/

#define A3D_MISC_ALLOCATION_FUNCTIONS_ALREADY_SET		-900000	/*!< \ref A3DDllSetAllocationFunctions can be called only once per session. */
#define A3D_MISC_INVALID_BREAK_VALUE						-900001	/*!< \ref A3DDllSetProgressFunctions : lBreak must be a valid pointer (not NULL). */
#define A3D_MISC_INVALID_CONFIGURATION						-900002	/*!< Unsupported configuration. */

#endif	/*	__A3DPRCERRORCODES_H__ */
