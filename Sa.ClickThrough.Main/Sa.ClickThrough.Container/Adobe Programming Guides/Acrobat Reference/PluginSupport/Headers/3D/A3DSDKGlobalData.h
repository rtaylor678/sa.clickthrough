/*******************************************************************************
* 
*  ADOBE SYSTEMS INCORPORATED
*  Copyright 2008 Adobe Systems Incorporated
*  All Rights Reserved
*  
*  NOTICE:  Adobe permits you to use, modify, and distribute this file in 
*  accordance with the terms of the Adobe license agreement accompanying it.  
*  If you have received this file from a source other than Adobe, then your use, 
*  modification, or distribution of it requires the prior written permission of 
*  Adobe.
* 
*******************************************************************************/

/*!
\file
\brief	Header file for global data
\author	ADOBE SYSTEMS INCORPORATED
\version	2.0
\date		February 2009
\par		(c) Copyright 2009 Adobe Systems Incorporated. All rights reserved.
*/

#ifndef __A3DPRCGLOBALDATA_H__
#define __A3DPRCGLOBALDATA_H__

/*!
\defgroup a3d_global_data_module Global Data Module
\ingroup a3d_entitiesdata_module
\brief Accesses global definitions used by all PRC entities

This module defines the data for commonly used attributes, such as colors, textures, materials, and patterns.
Global data is stored in the session's global memory, separate from the \ref A3DAsmModelFile.
Global data can be accessed independently from \ref A3DAsmModelFile. 
Therefore, you can access colors, textures, materials, and patterns that may or may not be used 
by the entities that are used in an \ref A3DAsmModelFile. 
All these global arrays are filled when reading the PRC physical file and optimized to save place. 
For instance, if two entities uses the same color definition (same triplet RGB), only one color will be stored in the array. 
*/

#ifndef __GLOBALDATA__

/*!
\brief Gets the global object \ref A3DGlobal 
\ingroup a3d_global_data_module
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalGetPointer,(A3DGlobal** ppGlobal));

/*!
\brief Global data structure
\ingroup a3d_global_data_module
\version 2.0

This structure holds the sizes of stored graphical parameters for the current \ref A3DAsmModelFile entity. 
Graphical parameters are stored as arrays of following data :
\li RGB colors : the size is \ref m_uiColorsSize ; to access the content data, 
use \ref A3DGlobalGetGraphRgbColorData; to create, use \ref A3DGlobalInsertGraphRgbColor ; 
\li Pictures : the size is \ref m_uiPicturesSize ; to access the content data, 
use \ref A3DGlobalGetGraphPictureData; to create, use  \ref A3DGlobalInsertGraphPicture
\li Texture Definitions : the size is \ref m_uiTextureDefinitionsSize ; To access the content data, 
use \ref A3DGlobalGetGraphTextureDefinitionData; to create, use  \ref A3DGlobalInsertGraphTextureDefinition
\li Materials : material is a generic type for two kind of data : \ref A3DGraphMaterialData 
and \ref A3DGraphTextureApplicationData. The size of the array is \ref m_uiMaterialsSize. 
To get the type of the data, use \ref A3DGlobalIsMaterialTexture. To access the content data, 
use \ref A3DGlobalGetGraphMaterialData or \ref A3DGlobalGetGraphTextureApplicationData; to create, use \ref A3DGlobalInsertGraphMaterial or \ref A3DGlobalInsertGraphTextureApplication
\li Line Patterns : the size is \ref m_uiLinePatternsSize ; To access the content data, 
use \ref A3DGlobalGetGraphLinePatternData; to create, use \ref A3DGlobalInsertGraphLinePattern
\li Display Styles: The size is \ref m_uiStylesSize ; To access the content data, 
use \ref A3DGlobalGetGraphStyleData; to create, use \ref A3DGlobalInsertGraphStyle
\li Fill Patterns: This is a generic type for several kind of data. 
The size of the array is \ref m_uiFillPatternsSize. \n  
To determine the type of the data, use the \ref A3DGlobalGetFillPatternType function. \n
To access the content data, use the dedicated function for the pattern. The form for such functions is <code>A3DGlobalGetGraph<i>XXX</i>PatternData</code>,
where <code><i>XXX</i></code> is replaced with type of pattern. \n
To set the content data, use the dedicated function for the pattern. The form for such functions is <code>A3DGlobalInsertGraph<i>XXX</i>Pattern</code>. 

\sa a3d_graphics
\warning The access functions <code>A3DGlobalGetGraph<i>XXX</i>Data</code> and <code>A3DGlobalInsertGraph<i>XXX</i></code> are used with indexes instead of pointers.
\note Call the <code>A3DGlobalGet<i>XXX</i>Data</code> function with default index to free the data structure.
*/
typedef struct
{
	ASUns16 m_usStructSize;				/*!< Reserved; must be initialized with \ref A3D_INITIALIZE_DATA.*/
	ASUns32 m_uiColorsSize;					/*!< Number of RGB colors */
	ASUns32 m_uiPicturesSize;				/*!< Number of Pictures */
	ASUns32 m_uiTextureDefinitionsSize;	/*!< Number of Texture definitions */
	ASUns32 m_uiMaterialsSize;				/*!< Number of Materials */
	ASUns32 m_uiLinePatternsSize;			/*!< Number of LinePatterns */
	ASUns32 m_uiStylesSize;					/*!< Number of Styles */
	ASUns32 m_uiFillPatternsSize;			/*!< Number of FillPatterns */
} A3DGlobalData;

/*!
\brief Populates the \ref A3DGlobalData structure
\ingroup a3d_global_data_module
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalGet,(const A3DGlobal* pGlobal,
									  A3DGlobalData* pData));


/*!
\brief Determines whether the data at the index uiIndexMaterial in the global array of materials 
is an \ref A3DGraphTextureApplication or an \ref A3DGraphMaterialData
\ingroup a3d_global_data_module
\version 2.0

\sa a3dtexture_application
\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalIsMaterialTexture,(const ASUns32 uiIndexMaterial,
														 ASBool* pbIsTexture));


/*!
\ingroup a3d_global_data_module
\version 2.0
\brief Gives the FillPattern type of the uiIndexPattern index in the global array of fill patterns. 

A FillPattern type can be one of the following types:
\li \ref kA3DTypeGraphDottingPattern
\li \ref kA3DTypeGraphHatchingPattern
\li \ref kA3DTypeGraphSolidPattern
\li \ref kA3DTypeGraphVPicturePattern

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalGetFillPatternType,(const ASUns32 uiIndexPattern,
														  A3DEEntityType* pePatternType));

#endif	/*	__GLOBALDATA__ */																
						


#endif	/*	__A3DPRCGLOBALDATA_H__ */
