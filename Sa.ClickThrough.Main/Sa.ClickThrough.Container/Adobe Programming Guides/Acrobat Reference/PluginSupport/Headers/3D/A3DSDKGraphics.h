/*******************************************************************************
* 
*  ADOBE SYSTEMS INCORPORATED
*  Copyright 2008 Adobe Systems Incorporated
*  All Rights Reserved
*  
*  NOTICE:  Adobe permits you to use, modify, and distribute this file in 
*  accordance with the terms of the Adobe license agreement accompanying it.  
*  If you have received this file from a source other than Adobe, then your use, 
*  modification, or distribution of it requires the prior written permission of 
*  Adobe.
* 
*******************************************************************************/

/*!
\file
\brief	Header file for the graphics module
\author	ADOBE SYSTEMS INCORPORATED
\version	2.0
\date		February 2009
\par		(c) Copyright 2009 Adobe Systems Incorporated. All rights reserved.
*/

#ifndef __A3DPRCGRAPHICS_H__
#define __A3DPRCGRAPHICS_H__

#ifndef __A3DPRCGEOMETRY_H__
#error  A3DSDKGeometry.h must be included before current file
#endif

#ifndef __A3DPRCTESSELLATION_H__
#error  A3DSDKTessellation.h must be included before current file
#endif

/*!
\defgroup a3d_graphics_module Graphics Module
\ingroup a3d_entitiesdata_module
\brief Creates and accesses scene display attributes (such as camera and lighting) 
	and common graphic attributes (such as textures, patterns and RGB colors)
	
\note The conventions described here are similar to the common scene graphics conventions described for 
other 3D standards such as OpenGL and DirectX. This is true for lights, textures, linestyles, patterns, cameras.
*/


#ifndef __A3D_GRAPHIC__
/*!
\defgroup a3d_graphics Graphics
\ingroup a3d_graphics_module
Graphics data exists only on entities that have the base type \ref A3DRootBaseWithGraphics. 

Graphics data is stored as a set of arrays in the global object \ref A3DGlobalData.  
Graphics data includes the following attributes:
\li Display style attributes such as transparency and width (see \ref a3d_style)
\li Graphics attributes such as patterns, RGB color, and graphics materials 
	(\ref A3DGraphVPicturePatternData or \ref A3DGraphLinePatternData, 
	\ref A3DGraphRgbColorData, \ref A3DGraphMaterialData or \ref A3DGraphTextureApplicationData)

To process a graphical attribute for an entity, consider whether the attribute is expressed 
as an explicit value or as an index that reference global attributes, as described in the following steps:
<ol>
	<li>Use the \ref a3d_cascadedattributes feature to obtain an \ref A3DGraphStyleData structure for the entity.
		(You can also use the \ref A3DGraphicsGet function to obtain graphic data for the entity; 
		however, it does not resolve inheritance settings.)</li>
	<li>For \ref A3DGraphStyleData members that contain attribute values (instead of indexes), 
		use the values as is.</li>
	<li>For \ref A3DGraphStyleData members that contain indexes (patterns or colors), 
	 	obtain the value from the current default global settings or from the appropriate global array.</li>
</ol>
*/

/*!
\brief Graphics data
\ingroup a3d_graphics
\version 2.0

To get the graphical attributes of an entity that has  the
\ref A3DRootBaseWithGraphics entity as its base, do these tasks: 
<ol>
	<li>Use the \ref m_uiStyleIndex member to retrieve the index of style in the global style array.</li>
	<li>If the index matches the default value, no particular graphical style is applied on entity.
	<li>Otherwise, use the index on \ref A3DGlobalGetGraphStyleData 
		to get the \ref A3DGraphStyleData from the global object. </li>
		<ul>
			<li>Width and transparency are directly on the \ref A3DGraphStyleData object.</li>
			<li>Use the indexes for colors, materials, textures, or patterns 
				and dedicated functions to get these graphics objects from global arrays.</li>
		</ul>
	</li>
</ol>
<!-- this repeats info that appears in the parent page -->

\warning When initializing the \ref A3DGraphicsData structure with the \ref A3D_INITIALIZE_DATA macro, every 
field is set to NULL; thus, to create a graphic attribute, and <b>if you do not change the behavior, the object will be hidden</b>. 
Please refer to \ref a3d_graphicsbits for more information on possible values. 
*/
typedef struct
{
	ASUns16 m_usStructSize;	/*!< Reserved; must be initialized with \ref A3D_INITIALIZE_DATA. */
	ASUns32 m_uiLayerIndex;	/*!< Layer index. */
	ASUns32 m_uiStyleIndex;	/*!< Index of the display style in the global style array. \sa A3DGraphStyleData */
	ASUns16 m_usBehaviour;	/*!< Behavior. \sa a3d_graphicsbits. */
} A3DGraphicsData;

/*!
\brief Populates the \ref A3DGraphicsData structure
\ingroup a3d_graphics
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGraphicsGet,(const A3DGraphics* pGraphics,
										 A3DGraphicsData* pData));


/*!
\brief Creates a \ref A3DGraphics from \ref A3DGraphicsData structure
\ingroup a3d_graphics
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGraphicsCreate,(const A3DGraphicsData* pData, 
											 A3DGraphics** ppGraphics));

/*!
\defgroup a3d_graphicsbits Bit Field for Behavior on Graphics
\ingroup a3d_graphics
\version 2.0

This bit field defines the behavior of a given entity, given its position in the tree of entities.<BR>
Inheritance works as follows:<BR>
\li If father and son (in sense of tree position) have the same inheritance bit, the corresponding value comes from the son,
\li Otherwise, the value is the value corresponding to the entity having bit set to 1.
@{
*/
#define kA3DGraphicsShow					0x0001	/*!< Entity is shown. */ 
#define kA3DGraphicsSonHeritShow				0x0002	/*!< Show inheritance. */ 
#define kA3DGraphicsFatherHeritShow				0x0004	/*!< Show inheritance. */ 
#define kA3DGraphicsSonHeritColor			0x0008	/*!< Color / material inheritance. */ 
#define kA3DGraphicsFatherHeritColor			0x0010 	/*!< Color / material inheritance. */ 
#define kA3DGraphicsSonHeritLayer			0x0020 	/*!< Layer inheritance. */ 
#define kA3DGraphicsFatherHeritLayer			0x0040 	/*!< Layer inheritance. */ 
#define kA3DGraphicsSonHeritTransparency	0x0080 	/*!< Transparency inheritance. */ 
#define kA3DGraphicsFatherHeritTransparency	0x0100 	/*!< Transparency inheritance. */ 
#define kA3DGraphicsSonHeritLinePattern	0x0200 	/*!< Line pattern inheritance. */ 
#define kA3DGraphicsFatherHeritLinePattern	0x0400	/*!< Line pattern inheritance. */ 
#define kA3DGraphicsSonHeritLineWidth		0x0800 	/*!< Line width inheritance. */ 
#define kA3DGraphicsFatherHeritLineWidth		0x1000 	/*!< Line width inheritance. */ 
#define kA3DGraphicsRemoved		0x2000 	/*!< Entity is removed. As a result, the entity no longer appears in the tree. */ 
/*!
@} <!-- End of module a3d_graphicsbits -->
*/
#endif	/*	__A3D_GRAPHIC__ */


#ifndef __A3D_GRAPHIC_STYLE__
/*!
\defgroup a3d_style Display Style
\ingroup a3d_graphics
Entity's type is \ref kA3DTypeGraphStyle.
*/

/*!
\brief Style data
\ingroup a3d_style
\version 2.0


Some of the members in this structure have paired interactions 
in which one member influences the behavior of the next member.
See the member descriptions for details. 

Use the \ref A3DGlobalData and dedicated global access functions to get graphic settings in the global arrays.

\sa \ref a3d_global_data_module
*/
typedef struct
{
	ASUns16 m_usStructSize;		/*!< Reserved; must be initialized with \ref A3D_INITIALIZE_DATA.*/
	A3DDouble m_dWidth;			/*!< Line width in mm. */
	ASBool m_bVPicture;			/*!< A value of true indicates the  \ref m_uiLinePatternIndex member (next) 
									references a vpicture in the global arrays
									(see \ref A3DGraphVPicturePatternData).
									A value of false indicates it references a line pattern 
									(see \ref A3DGraphLinePatternData). */
	ASUns32 m_uiLinePatternIndex;	/*!< Index in the global array as indicated in the \ref m_bVPicture member (above). */
	ASBool m_bMaterial;			/*!< A value of true indicates the \ref m_uiLinePatternIndex member (next) 
									references a material in the global arrays 
									(see \ref A3DGraphMaterialData or \ref A3DGraphTextureApplicationData). 
									A value of false indicates it references a color 
									(see \ref A3DGraphRgbColorData). */
	ASUns32 m_uiRgbColorIndex;	/*!< Index in the global array as indicated in the \ref m_bMaterial member (above). */
	ASBool m_bIsTransparencyDefined;/*!< A value of true indicates 
										the \ref m_ucTransparency member (next) is defined. 
										A value of false indicates the transparency is the default value (255). */
	ASUns8 m_ucTransparency;	/*!< From 0 (transparent) to 255 (opaque). Default value is 255. */
	ASBool m_bSpecialCulling;	/*!< A value of true indicates special culling defined.
									See descriptions for \ref m_bFrontCulling, \ref m_bBackCulling,
									and m_bBackCulling.  */
	ASBool m_bFrontCulling;		/*!< If \ref m_bSpecialCulling is TRUE and if \ref m_bFrontCulling is TRUE, 
							 		font culling is active. */
	ASBool m_bBackCulling;		/*!< If \ref m_bSpecialCulling is TRUE and if \ref m_bBackCulling is TRUE, 
									back-culling is active. */
	ASBool m_bNoLight;			/*!< A value of true indicates material light is sensible. */
} A3DGraphStyleData;

/*!
\brief Populates the \ref A3DGraphStyleData structure
\ingroup a3d_style
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalGetGraphStyleData,(const ASUns32 uiIndexStyle,
														 A3DGraphStyleData* pData));

/*!
\brief Inserts a style from \ref A3DGraphStyleData structure into the global data
\ingroup a3d_style
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_DEFAULT_COLOR \n
\return \ref A3D_INVALID_COLOR_INDEX \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalInsertGraphStyle,(const A3DGraphStyleData* pData,
														ASUns32* puiIndexStyle));


#endif	/*	__A3D_GRAPHIC_STYLE__ */

#ifndef __A3D_GRAPHIC_COLOR__
/*!
\defgroup a3d_rgbcolor RGB Color
\ingroup a3d_graphics
*/

/*!
\brief RGB Color data
\ingroup a3d_rgbcolor
\version 2.0
*/
typedef struct
{
	ASUns16 m_usStructSize;	/*!< Reserved; must be initialized with \ref A3D_INITIALIZE_DATA. */
	A3DDouble m_dRed;	/*!< [0..1] red value. */
	A3DDouble m_dGreen;	/*!< [0..1] green value.  */
	A3DDouble m_dBlue;	/*!< [0..1] blue value.  */
} A3DGraphRgbColorData;

/*!
\brief Populates the \ref A3DGraphRgbColorData structure
\ingroup a3d_rgbcolor
\version 2.0

Structure of three doubles containing values for Red, Green and Blue intensities of current color. 
These values must be between 0.0 and 1.0.
\note The index must be a multiple of 3.
\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_DEFAULT_COLOR \n
\return \ref A3D_INVALID_COLOR_INDEX \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalGetGraphRgbColorData,(const ASUns32 uiIndexRgbColor,
															 A3DGraphRgbColorData* pData));

/*!
\brief Creates a color from \ref A3DGraphRgbColorData structure in global data
\ingroup a3d_rgbcolor
\version 2.0

This function returns the index (puiIndexRgbColor) of the color in global data.
The index returned is not a new index if the same color is already stored.
\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_DEFAULT_COLOR \n
\return \ref A3D_INVALID_COLOR_INDEX \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalInsertGraphRgbColor,(const A3DGraphRgbColorData* pData,
															ASUns32* puiIndexRgbColor));

#endif	/*	__A3D_GRAPHIC_COLOR__ */


#ifndef __A3D_GRAPHIC_LINEPATTERN__
/*!
\defgroup a3d_linepattern Line Pattern
\ingroup a3d_graphics

Entity's type is \ref kA3DTypeGraphLinePattern.
This structure contains an array of doubles defining the basic sequence used to 
describe a line pattern. The dash is defined as number pairs, where 
the first number specifies the length of the line and the second number specifies the length of the space.
The pair is repeated.

If additional number pairs are provided, they are added to the sequence before the pattern is repeated.

Line and space lengths can be defined by using the current units (typically millimeters) or as proportions.

Here are some examples:
\li {10000., 0.} specifies a continuous line ____________________
\li {3., 3.} specifies a dashed line - - - - - - - - - -
\li {1., 4.} specifies a dotted line .  .  .  .  .  .  .
\li {1., 2., 4., 2.} specifies a line with a mixed pattern  _ ____ _ ____ _ ____
*/

/*!
\brief Line Pattern data
\ingroup a3d_linepattern
\version 2.0

Here are characteristics of a line pattern:
\li Array describing a basic sequence is always starting with a black value (pen down).
\li Sequence can start with an offset from 0 ( \ref m_dPhase ).
\li Sequence lengths can be defined in real length or proportions ( \ref m_bRealLength ).
*/
typedef struct
{
	ASUns16 m_usStructSize;		/*!< Reserved; must be initialized with \ref A3D_INITIALIZE_DATA. */
	ASUns32 m_uiNumberOfLengths;	/*!< Size of next array. */
	A3DDouble* m_pdLengths;		/*!< Array of lengths describing basic sequence for pattern. */
	A3DDouble m_dPhase;			/*!< Starting point. Can be non-zero. */
	ASBool m_bRealLength;		/*!< Either proportion (OpenGL style) or real length (AutoCad DXF format style).*/
} A3DGraphLinePatternData;

/*!
\brief Populates the \ref A3DGraphLinePatternData structure
\ingroup a3d_linepattern
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalGetGraphLinePatternData,(const ASUns32 uiIndexLinePattern,
																 A3DGraphLinePatternData* pData));

/*!
\brief Creates a line pattern from \ref A3DGraphLinePatternData structure in global data
\ingroup a3d_linepattern
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_DEFAULT_COLOR \n
\return \ref A3D_INVALID_COLOR_INDEX \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalInsertGraphLinePattern,(const A3DGraphLinePatternData* pData,
																ASUns32* puiIndexLinePattern));

#endif	/*	__A3D_GRAPHIC_LINEPATTERN__ */


#ifndef __A3D_GRAPHIC_MATERIAL__
/*!
\defgroup a3d_material Material
\ingroup a3d_graphics
Entity's type is \ref kA3DTypeGraphMaterial.
*/

/*!
\brief Material data
\ingroup a3d_material
\version 2.0
\note Use \ref A3DGlobalGetGraphRgbColorData to get ambient, diffuse, emissive and specular colors.

\note The conventions described here are similar to the common scene graphics conventions described for 
other 3D standards such as OpenGL and DirectX. 
*/
typedef struct
{
	ASUns16 m_usStructSize;		/*!< Reserved; must be initialized with \ref A3D_INITIALIZE_DATA .*/
	ASUns32 m_uiAmbient;			/*!< Ambient color.*/
	ASUns32 m_uiDiffuse;			/*!< Diffuse color.*/
	ASUns32 m_uiEmissive;			/*!< Emissive color.*/
	ASUns32 m_uiSpecular;			/*!< Specular color.*/
	A3DDouble m_dAmbientAlpha;	/*!< Ambient alpha value. Not yet implemented. Use \ref A3DGraphStyleData::m_ucTransparency instead. */
	A3DDouble m_dDiffuseAlpha;	/*!< Diffuse alpha value. Not yet implemented. Use \ref A3DGraphStyleData::m_ucTransparency instead. */
	A3DDouble m_dEmissiveAlpha;	/*!< Emissive alpha value. Not yet implemented. Use \ref A3DGraphStyleData::m_ucTransparency instead. */
	A3DDouble m_dSpecularAlpha;	/*!< Specular alpha value. Not yet implemented. Use \ref A3DGraphStyleData::m_ucTransparency instead. */
	A3DDouble m_dShininess;		/*!< Shininess. Valid values are between 0.0 and 1.0. */
} A3DGraphMaterialData;

/*!
\brief Populates the \ref A3DGraphMaterialData structure
\ingroup a3d_material
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalGetGraphMaterialData,(const ASUns32 uiIndexMaterial,
															 A3DGraphMaterialData* pData));

/*!
\brief Creates a material from \ref A3DGraphMaterialData structure in global data
\ingroup a3d_material
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_DEFAULT_COLOR \n
\return \ref A3D_INVALID_COLOR_INDEX \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalInsertGraphMaterial,(const A3DGraphMaterialData* pData, 
															ASUns32* puiIndexMaterial));

#endif	/*	__A3D_GRAPHIC_MATERIAL__ */

#ifndef __A3D_GRAPHIC_PICTURE__
/*!
\defgroup a3d_picture Picture
\ingroup a3d_graphics
Entity's type is \ref kA3DTypeGraphPicture.

The \COMPONENT_A3D_API does not interpret or otherwise process picture data (image data).
Instead, it transmits picture data directly to the \PRODUCT_ACRO3D rendering engine. 

For information about the picture data formats that \PRODUCT_ACRO3D supports, see 
the "Filters" section in the \REF_PDF_REFERENCE.
*/

/*!
\ingroup a3d_picture
\brief Image format
\version 2.0

\note The conventions described here are similar to the common scene graphics conventions described for 
other 3D standards such as OpenGL and DirectX.

*/
typedef enum
{	
	kA3DPicturePng,					/*!< \todo To be documented */
	kA3DPictureJpg,					/*!< \todo To be documented */
	kA3DPictureBmp,					/*!< \todo To be documented */
	kA3DPictureBitmapRgbByte,		/*!< \todo To be documented */
	kA3DPictureBitmapRgbaByte,		/*!< \todo To be documented */
	kA3DPictureBitmapGreyByte,		/*!< \todo To be documented */
	kA3DPictureBitmapGreyaByte,	/*!< \todo To be documented */
} A3DEPictureDataFormat;

/*!
\brief Picture data
\ingroup a3d_picture
\version 2.0
*/
typedef struct
{
	ASUns16 m_usStructSize;	/*!< Reserved; must be initialized with \ref A3D_INITIALIZE_DATA.*/
	A3DEPictureDataFormat m_eFormat;	/*!< Format for image.*/
	ASUns32 m_uiSize;	/*!< Size for next array. */
	ASUns8* m_pucBinaryData;	/*!< Binary data for image. */
	ASUns32 m_uiPixelWidth;	/*!< \todo To be documented. */
	ASUns32 m_uiPixelHeight;	/*!< \todo To be documented. */
} A3DGraphPictureData;

/*!
\brief Populates the \ref A3DGraphPictureData structure
\ingroup a3d_picture
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalGetGraphPictureData,(const ASUns32 uiIndexPicture,
															A3DGraphPictureData* pData));

/*!
\brief Creates a picture from \ref A3DGraphPictureData structure in global data
\ingroup a3d_picture
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_DEFAULT_COLOR \n
\return \ref A3D_INVALID_COLOR_INDEX \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalInsertGraphPicture,(const A3DGraphPictureData* pData,
														  ASUns32* puiPictureIndex));

/*!
@}
*/

#endif	/*	__A3D_GRAPHIC_PICTURE__ */

#ifndef __A3D_GRAPHIC_DOTTINGPATTERN__
/*!
\defgroup a3d_dottingpattern Dotting Pattern
\ingroup a3d_graphics
Entity's type is \ref kA3DTypeGraphDottingPattern. It is one of the four Fill Patterns.
*/

/*!
\ingroup a3d_dottingpattern
\brief Dotting Pattern Structure
\version 2.0

By default, this pattern describes a regular grid of point spaced with the \ref m_dPitch member.
If \ref m_bZigZag is TRUE, then points are offset in X by \ref m_dPitch /2.0 for the odd row.
*/
typedef struct
{
	ASUns16	m_usStructSize;			/*!< Reserved; must be initialized with \ref A3D_INITIALIZE_DATA .*/
	A3DDouble	m_dPitch;					/*!< \todo To be documented */
	ASBool		m_bZigZag;					/*!< \todo To be documented */
	ASUns32		m_uiColorIndex;			/*!< \todo To be documented */
	ASUns32		m_uiNextPatternIndex;	/*!< \todo To be documented */
} A3DGraphDottingPatternData;

/*!
\brief Populates the \ref A3DGraphDottingPatternData structure
\ingroup a3d_dottingpattern
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalGetGraphDottingPatternData,(const ASUns32 uiIndexDottingPattern,
																	 A3DGraphDottingPatternData* pData));

/*!
\brief Creates a Dotting Pattern from \ref A3DGraphDottingPatternData structure in global data
\ingroup a3d_dottingpattern
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_DEFAULT_COLOR \n
\return \ref A3D_INVALID_COLOR_INDEX \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalInsertGraphDottingPattern,(const A3DGraphDottingPatternData* pData,
																	ASUns32* puiIndexDottingPattern));

#endif	/*	__A3D_GRAPHIC_DOTTINGPATTERN__ */

#ifndef __A3D_GRAPHIC_HATCHINGPATTERN__

/*!
\defgroup a3d_hatchingpattern Hatching Pattern
\ingroup a3d_graphics
Entity's type is \ref kA3DTypeGraphHatchingPattern. It is one of the four Fill Patterns.
*/

/*!
\defgroup a3d_hatchingpatternline Hatching Pattern Line
\ingroup a3d_hatchingpattern
*/
/*!
\ingroup a3d_hatchingpatternline
\brief A HatchingPatternLine is a group of infinite lines in XY space that are defined 
by a start line and an offset between sequential lines.
\version 2.0

Start line is defined by a start point and an angle in radians from the X Axis.
*/
typedef struct
{
	ASUns16			m_usStructSize;		/*!< Reserved; must be initialized with \ref A3D_INITIALIZE_DATA .*/
	A3DVector2dData	m_sStart;				/*!< \todo To be documented */
	A3DVector2dData	m_sOffset;				/*!< \todo To be documented */
	A3DDouble			m_dAngle;				/*!< \todo To be documented */
	ASUns32				m_uiStyleIndex;		/*!< \todo To be documented */
} A3DGraphHatchingPatternLineData;

/*!
\ingroup a3d_hatchingpattern
\brief Hatching Pattern Structure
\version 2.0
*/
typedef struct
{
	ASUns16				m_usStructSize;			/*!< Reserved; must be initialized with \ref A3D_INITIALIZE_DATA .*/
	ASUns32				m_uiSize;		/*!< Size of next array. */
	A3DGraphHatchingPatternLineData*	m_psHatchLines;			/*!< \todo To be documented */
	ASUns32				m_uiNextPatternIndex;	/*!< \todo To be documented */
} A3DGraphHatchingPatternData;

/*!
\brief Populates the \ref A3DGraphHatchingPatternData structure
\ingroup a3d_hatchingpattern
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalGetGraphHatchingPatternData,(const ASUns32 uiIndexHatchingPattern,
																	  A3DGraphHatchingPatternData* pData));

/*!
\brief Creates a Hatching Pattern from \ref A3DGraphHatchingPatternData structure in global data
\ingroup a3d_hatchingpattern
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_DEFAULT_COLOR \n
\return \ref A3D_INVALID_COLOR_INDEX \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalInsertGraphHatchingPattern,(const A3DGraphHatchingPatternData* pData,
																	 ASUns32* puiIndexHatchingPattern));

#endif	/*	__A3D_GRAPHIC_HATCHINGPATTERN__ */

#ifndef __A3D_GRAPHIC_SOLIDPATTERN__
/*!
\defgroup a3d_solidpattern Solid Pattern
\ingroup a3d_graphics
Entity's type is \ref kA3DTypeGraphSolidPattern. It is one of the four Fill Patterns.
*/

/*!
\ingroup a3d_solidpattern
\brief Solid Pattern Structure
\version 2.0
*/
typedef struct
{
	ASUns16	m_usStructSize;			/*!< Reserved; must be initialized with \ref A3D_INITIALIZE_DATA .*/
	ASBool		m_bMaterial;			/*!< Is next index related to \ref A3DGraphMaterialData or \ref A3DGraphRgbColorData in \ref A3DGlobalData? */
	ASUns32		m_uiRgbColorIndex;		/*!< Index in array as indicated above. */
	ASUns32		m_uiNextPatternIndex;	/*!< \todo To be documented */
} A3DGraphSolidPatternData;

/*!
\brief Populates the \ref A3DGraphSolidPatternData structure
\ingroup a3d_solidpattern
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalGetGraphSolidPatternData,(const ASUns32 uiIndexSolidPattern,
																  A3DGraphSolidPatternData* pData));

/*!
\brief Creates a Solid Pattern from \ref A3DGraphSolidPatternData structure in global data
\ingroup a3d_solidpattern
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_DEFAULT_COLOR \n
\return \ref A3D_INVALID_COLOR_INDEX \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalInsertGraphSolidPattern,(const A3DGraphSolidPatternData* pData,
																 ASUns32* puiIndexSolidPattern));

#endif	/*	__A3D_GRAPHIC_SOLIDPATTERN__ */

#ifndef __A3D_GRAPHIC_VPICTUREPATTERN__

/*!
\defgroup a3d_vpicturepattern Vectorized Picture Pattern
\ingroup a3d_graphics
Entity's type is \ref kA3DTypeGraphVPicturePattern. It is one of the four Fill Patterns.

This pattern uses a tessellation to define a vectorized picture.
*/

/*!
\ingroup a3d_vpicturepattern
\brief VPicture Pattern Structure
\version 2.0

\warning \ref m_pMarkupTess is a restricted version of \ref kA3DTypeTessMarkup.
The authorized entities are as follows:
- Polyline
- Triangles
- Color
- Line stipple
- Points
- Polygon
- Line width
*/
typedef struct
{
	ASUns16 m_usStructSize;	/*!< Reserved; must be initialized with \ref A3D_INITIALIZE_DATA.*/
	ASUns32		m_uiNextPatternIndex;	/*!< \todo To be documented */
	A3DTessMarkup* m_pMarkupTess;/*!< Tessellation used as pattern*/
} A3DGraphVPicturePatternData;

/*!
\brief Populates the \ref A3DGraphVPicturePatternData structure
\ingroup a3d_vpicturepattern
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalGetGraphVPicturePatternData,(const ASUns32 uiIndexVPicturePattern,
																	  A3DGraphVPicturePatternData* pData));

/*!
\brief Creates a VPicture Pattern from \ref A3DGraphVPicturePatternData structure in global data
\ingroup a3d_vpicturepattern
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_DEFAULT_COLOR \n
\return \ref A3D_INVALID_COLOR_INDEX \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGlobalInsertGraphVPicturePattern,(const A3DGraphVPicturePatternData* pData,
																	 ASUns32* puiIndexVPicturePattern));

#endif	/*	__A3D_GRAPHIC_VPICTUREPATTERN__ */

#ifndef __A3D_CAMERA__
/*!
\defgroup a3d_camera Camera
\ingroup a3d_graphics_module
Entity's type is \ref kA3DTypeGraphCamera.
*/
/*!
\ingroup a3d_camera
\brief Camera Structure
\version 2.0

The \ref m_bOrthographic influences the interpretation of the \ref m_dXFovy and \ref m_dYFovy members, 
as described in the member descriptions.
*/
typedef struct
{
	ASUns16 m_usStructSize;		/*!< Reserved; must be initialized with \ref A3D_INITIALIZE_DATA. */
	ASBool m_bOrthographic;		/*!< A value of true indicates an orthographic projection.
									A value of false indicates a perspective projection. */
	A3DVector3dData m_sLocation;/*!< Position of the camera. */
	A3DVector3dData m_sLookAt;	/*!< Look at point. */
	A3DVector3dData m_sUp;		/*!< Up vector. */
	A3DDouble m_dXFovy;			/*!< If \ref m_bOrthographic is true, 
										this member specifies the view angle in radians in the X direction.
										Otherwise, it specifies a perspective projection in ScaleX.  */
	A3DDouble m_dYFovy;			/*!< If \ref m_bOrthographic is true, 
										this member specifies the view angle in radians in the Y direction.
										Otherwise, it specifies a perspective projection in ScaleY.   */
	A3DDouble m_dAspectRatio;	/*!< Ratio of X to Y. */
	A3DDouble m_dZNear;			/*!< Near clipping plane distance from the viewer (positive value). */
	A3DDouble m_dZFar;			/*!< Far clipping plane distance from the viewer (positive value). */
	A3DDouble m_dZoomFactor;	/*!< Zoom factor. */
} A3DGraphCameraData;

/*!
\brief Populates the \ref A3DGraphCameraData structure
\ingroup a3d_camera
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGraphCameraGet,(const A3DGraphCamera* pCamera,
											 A3DGraphCameraData* pData));


/*!
\brief Creates a \ref A3DGraphCamera from \ref A3DGraphCameraData structure
\ingroup a3d_camera
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGraphCameraCreate,(const A3DGraphCameraData* pData, 
												 A3DGraphCamera** ppCamera));

#endif	/*	__A3D_CAMERA__ */

/*!
\defgroup a3d_light Light
\ingroup a3d_graphics_module
\brief Creates and accesses light sources 

This module explains how to create and access the PRC entities that specify light sources.
Light sources provide the lighting for a 3D scene. 
A single scene can specify multiple different light sources.

*/

#ifndef __A3D_AMBIENT_LIGHT__
/*!
\defgroup a3d_light_ambient Ambient Light
\ingroup a3d_light
\brief Creates and accesses ambient light attributes

This module explains how to create and access a PRC entity that specifies an ambient light source.
Ambient light is light that is generally scattered about the scene to the extent that the source
of the light cannot be determined.
 
Entity's type is \ref kA3DTypeGraphAmbientLight.

*/

/*!
\ingroup a3d_light_ambient
\brief Light Structure. Ambient light
\version 2.0
*/
typedef struct
{
	ASUns16 m_usStructSize;	/*!< Reserved; must be initialized with \ref A3D_INITIALIZE_DATA. */
	ASUns32 m_uiAmbientColorIndex; /*!< */
	ASUns32 m_uiDiffuseColorIndex; /*!< */
	ASUns32 m_uiSpecularColorIndex; /*!< */
} A3DGraphAmbientLightData;

/*!
\brief Populates the \ref A3DGraphAmbientLightData structure
\ingroup a3d_light_ambient
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGraphAmbientLightGet,(const A3DGraphAmbientLight* pLight,
			A3DGraphAmbientLightData* pData));

/*!
\brief Creates a \ref A3DGraphAmbientLight from an \ref A3DGraphAmbientLightData structure
\ingroup a3d_light_ambient
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGraphAmbientLightCreate,(const A3DGraphAmbientLightData* pData, 
			A3DGraphAmbientLight** ppLight));

#endif	/*	__A3D_AMBIENT_LIGHT__ */

#ifndef __A3D_POINT_LIGHT__
/*!
\defgroup a3d_light_point Point Light
\ingroup a3d_light
\brief Creates and accesses point light attributes

This module explains how to create and access a PRC entity that specifies a point light source.
Point light radiates equally in all directions, similar to a light bulb.
 
Entity's type is \ref kA3DTypeGraphPointLight.
*/
/*!
\ingroup a3d_light_point
\brief A structure representing the settings for a point light
\version 2.0
*/
typedef struct
{
	ASUns16 m_usStructSize;	/*!< Reserved; must be initialized with \ref A3D_INITIALIZE_DATA. */
	ASUns32 m_uiAmbientColorIndex; /*!< */
	ASUns32 m_uiDiffuseColorIndex; /*!< */
	ASUns32 m_uiSpecularColorIndex; /*!< */
	A3DVector3dData m_sLocation; /*!< */
	A3DDouble m_dConstantAttenuation; /*!< */
	A3DDouble m_dLinearAttenuation; /*!< */
	A3DDouble m_dQuadraticAttenuation; /*!< */
} A3DGraphPointLightData;

/*!
\brief Populates the \ref A3DGraphPointLightData structure
\ingroup a3d_light_point
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGraphPointLightGet,(const A3DGraphPointLight* pLight,
			A3DGraphPointLightData* pData));

/*!
\brief Creates a \ref A3DGraphPointLight from \ref A3DGraphPointLightData structure
\ingroup a3d_light_point
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGraphPointLightCreate,(const A3DGraphPointLightData* pData, 
			A3DGraphPointLight** ppLight));

#endif	/*	__A3D_POINT_LIGHT__ */

#ifndef __A3D_SPOT_LIGHT__
/*!
\defgroup a3d_light_spot Spotlight
\ingroup a3d_light
\brief Creates and accesses spotlight attributes

This module explains how to create and access a PRC entity that specifies a spotlight source.
A spotlight source radiates in a conical direction, similar to a car headlight.

Entity's type is \ref kA3DTypeGraphSpotLight.
*/
/*!
\ingroup a3d_light_spot
\brief Spotlight structure

For version 2.1, additional fields for attenuation settings were added to this structure. 
These additional fields are identified with version tags.

\version 2.0
*/
typedef struct
{
	ASUns16 m_usStructSize;	/*!< Reserved; must be initialized with \ref A3D_INITIALIZE_DATA. */
	ASUns32 m_uiAmbientColorIndex; /*!< */
	ASUns32 m_uiDiffuseColorIndex; /*!< */
	ASUns32 m_uiSpecularColorIndex; /*!< */
	A3DVector3dData m_sDirection; /*!< */
	A3DDouble m_dFallOffAngle; /*!< */
	A3DDouble m_dFallOffExponent; /*!< */
	A3DVector3dData m_sLocation; /*!< \version 2.1 */
	A3DDouble m_dConstantAttenuation; /*!< \version 2.1 */
	A3DDouble m_dLinearAttenuation; /*!< \version 2.1  */
	A3DDouble m_dQuadraticAttenuation; /*!< \version 2.1  */
} A3DGraphSpotLightData;

/*!
\brief Populates the \ref A3DGraphSpotLightData structure
\ingroup a3d_light_spot
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGraphSpotLightGet,(const A3DGraphSpotLight* pLight,
			A3DGraphSpotLightData* pData));

/*!
\brief Creates a \ref A3DGraphSpotLight from \ref A3DGraphSpotLightData structure
\ingroup a3d_light_spot
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGraphSpotLightCreate,(const A3DGraphSpotLightData* pData, 
			A3DGraphSpotLight** ppLight));

#endif	/*	__A3D_SPOT_LIGHT__ */

#ifndef __A3D_DIRECTIONAL_LIGHT__
/*!
\defgroup a3d_light_directional Directional Light
\ingroup a3d_light
\brief Creates and accesses a directional light attributes

This module explains how to create and access a PRC entity that specifies a directional light source.
A directional light (specular light) emanates from a particular direction and bounces off the surface in a preferred direction. Specular light gives a surface a shiny appearance.

Entity's type is \ref kA3DTypeGraphDirectionalLight.
*/
/*!
\ingroup a3d_light_directional
\brief Light Structure. Directional light
\version 2.0
*/
typedef struct
{
	ASUns16 m_usStructSize;	/*!< Reserved; must be initialized with \ref A3D_INITIALIZE_DATA. */
	ASUns32 m_uiAmbientColorIndex; /*!< */
	ASUns32 m_uiDiffuseColorIndex; /*!< */
	ASUns32 m_uiSpecularColorIndex; /*!< */
	A3DDouble m_dIntensity; /*!< */
	A3DVector3dData m_sDirection; /*!< */
} A3DGraphDirectionalLightData;

/*!
\brief Populates the \ref A3DGraphDirectionalLightData structure
\ingroup a3d_light_directional
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGraphDirectionalLightGet,(const A3DGraphDirectionalLight* pLight,
			A3DGraphDirectionalLightData* pData));

/*!
\brief Creates a \ref A3DGraphDirectionalLight from \ref A3DGraphDirectionalLightData structure
\ingroup a3d_light_directional
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGraphDirectionalLightCreate,(const A3DGraphDirectionalLightData* pData, 
			A3DGraphDirectionalLight** ppLight));

#endif	/*	__A3D_DIRECTIONAL_LIGHT__ */

#ifndef __A3D_SCENEDISPPARAMS__
/*!
\defgroup a3d_scenedisplayparameters SceneDisplayParameters
\ingroup a3d_graphics_module
Entity's type is \ref kA3DTypeGraphSceneDisplayParameters.
*/
/*!
\ingroup a3d_scenedisplayparameters
\brief SceneDisplayParameters Structure
\version 2.0
*/

typedef struct
{
	ASUns16 m_usStructSize;
	ASBool m_bIsActive; /*!< */
	A3DGraphCamera* m_pCamera; /*!< */
	ASUns32 m_uiLightSize; /*!< */
	A3DEntity** m_ppLights; /*!< */
	ASUns32 m_uiPlaneSize; /*!< */
	A3DSurfPlane** m_ppClippingPlanes; /*!< */
	ASBool m_bHasRotationCenter; /*!< */
	A3DVector3dData m_sRotationCenter; /*!< */
	ASUns32 m_uiBackgroundStyleIndex; /*!< */
	ASUns32 m_uiDefaultStyleIndex; /*!< */
	ASUns32 m_uiDefaultPerTypeIndexSize; /*!< */
	ASUns32* m_puiDefaultStyleIndexesPerType; /*!< */
	A3DEEntityType* m_puiTypesOfDefaultStyleIndexes; /*!< */

} A3DGraphSceneDisplayParametersData;

/*!
\brief Populates the \ref A3DGraphSceneDisplayParametersData structure
\ingroup a3d_scenedisplayparameters
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGraphSceneDisplayParametersGet,(const A3DGraphSceneDisplayParameters* pSceneDisplayParameters,
			A3DGraphSceneDisplayParametersData* pData));


/*!
\brief Creates a \ref A3DGraphSceneDisplayParameters from \ref A3DGraphSceneDisplayParametersData structure
\ingroup a3d_scenedisplayparameters
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DGraphSceneDisplayParametersCreate,(const A3DGraphSceneDisplayParametersData* pData, 
			A3DGraphSceneDisplayParameters** ppSceneDisplayParameters));

#endif	/*	__A3D_SCENEDISPPARAMS__ */

#ifndef __A3D_CASCADEDATTRIBUTES__
/*!
\defgroup a3d_cascadedattributes Miscellaneous Cascaded Attributes
\ingroup a3d_misc_module
\brief Access inheritable PRC attributes such as visibility, color, and coordinate system

These functions provide tools for browsing the model file and for determining 
inheritable attributes such as visibility, color, and coordinate system. 
The functions and structures in this module simplify the determination of inherited attributes. 
\ref a3d_misc_cascaded_attributes provides a complete explanation and sample code.

\sa \ref a3d_graphicsbits
\sa \ref a3d_misc_cascaded_attributes
*/

/*!
\ingroup a3d_cascadedattributes
\brief Cascaded Attributes Structure
\version 2.0
*/
typedef struct
{
	ASUns16 m_usStructSize;								/*!< Reserved; must be initialized with \ref A3D_INITIALIZE_DATA. */
	ASBool m_bShow;										/*!< The object is to be shown. */
	ASBool m_bRemoved;									/*!< The object or its reference has been removed. */
	ASUns16 m_usLayer;									/*!< Layer \todo To be documented. */
	A3DGraphStyleData m_sStyle;						/*!< Style to use for the object. */
	A3DRiCoordinateSystem* m_pCoordinateSystem;	/*!< Coordinate system to use for the object. */
} A3DMiscCascadedAttributesData;

/*!
\brief Creates a \ref A3DMiscCascadedAttributes
\ingroup a3d_cascadedattributes
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DMiscCascadedAttributesCreate,(A3DMiscCascadedAttributes** ppAttr));

/*!
\brief Delete a \ref A3DMiscCascadedAttributes
\ingroup a3d_cascadedattributes
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DMiscCascadedAttributesDelete,(A3DMiscCascadedAttributes* pAttr));

/*!
\brief Populate the \ref A3DMiscCascadedAttributesData structure
\ingroup a3d_cascadedattributes
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DMiscCascadedAttributesGet,(	const A3DMiscCascadedAttributes* pAttr,
														A3DMiscCascadedAttributesData* psData));


/*!
\brief Push a \ref A3DMiscCascadedAttributes
\ingroup a3d_cascadedattributes
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DMiscCascadedAttributesPush,(A3DMiscCascadedAttributes* pAttr,
														const A3DRootBaseWithGraphics* pBase,
														const A3DMiscCascadedAttributes* pFather));

/*!
\brief Push a \ref A3DMiscCascadedAttributesData for a \ref A3DTessFaceData
\ingroup a3d_cascadedattributes
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DMiscCascadedAttributesPushTessFace,(A3DMiscCascadedAttributes* pAttr,
																  const A3DRiRepresentationItem* pRepItem,
																  const A3DTessBase* pTessBase,
																  const A3DTessFaceData* psTessFaceData,
																  ASUns32 uiFaceIndex,
																  const A3DMiscCascadedAttributes* pFather));

#endif	/*	__A3D_CASCADEDATTRIBUTES__ */



#endif	/*	__A3DPRCGRAPHICS_H__ */
