/*********************************************************************

ADOBE SYSTEMS INCORPORATED
Copyright(C)1998-2008 Adobe Systems Incorporated
All rights reserved.

NOTICE: Adobe permits you to use,modify,and distribute this file
in accordance with the terms of the Adobe license agreement
accompanying it. If you have received this file from a source other
than Adobe,then your use,modification,or distribution of it
requires the prior written permission of Adobe.

*********************************************************************/

// Acrobat Headers.
#ifndef MAC_PLATFORM
#include <PIHeaders.h>
#endif

#include <math.h>
#include <io.h>

#define A3D_API(returntype,name,params)\
typedef returntype (*PF##name) params;\
static PF##name name = NULL;

#include <A3DSDK.h>
#include <A3DSDKTypes.h>
#include <A3DSDKBase.h>
#include <A3DSDKErrorCodes.h>
#include <A3DSDKGeometry.h>
#include <A3DSDKTessellation.h>
#include <A3DSDKGraphics.h>
#include <A3DSDKStructure.h>
#include <A3DSDKRootEntities.h>
#include <A3DSDKRepItems.h>
#include <A3DSDKTessellation.h>
#include <A3DSDKMarkup.h>
#include <A3DSDKGlobalData.h>
#include <A3DSDKTexture.h>
#include <A3DSDKMisc.h>
#include <A3DSDKGeometryCrv.h>
#include <A3DSDKGeometrySrf.h>
#include <A3DSDKTopology.h>

#undef __A3DPRCSDK_H__
#undef __A3DPRCTYPES_H__
#undef __A3DPRCBASE_H__
#undef __A3DPRCERRORCODES_H__
#undef __A3DPRCGEOMETRY_H__
#undef __A3DPRCTESSELLATION_H__
#undef __A3DPRCGRAPHICS_H__
#undef __A3DPRCSTRUCTURE_H__
#undef __A3DPRCROOTENTITIES_H__
#undef __A3DPRCREPITEMS_H__
#undef __A3DPRCMARKUP_H__
#undef __A3DPRCGLOBALDATA_H__
#undef __A3DPRCTEXTURE_H__
#undef __A3DPRCMISC_H__
#undef __A3DPRCGEOMETRYCRV_H__
#undef __A3DPRCGEOMETRYSRF_H__
#undef __A3DPRCTOPOLOGY_H__

#undef A3D_API

void A3DPRCFunctionPointersInitialize(HMODULE hModule)
{
#define A3D_API(returntype,name,params) name = (PF##name)GetProcAddress(hModule,#name);
#include <A3DSDK.h>
#include <A3DSDKTypes.h>
#include <A3DSDKBase.h>
#include <A3DSDKErrorCodes.h>
#include <A3DSDKGeometry.h>
#include <A3DSDKTessellation.h>
#include <A3DSDKGraphics.h>
#include <A3DSDKStructure.h>
#include <A3DSDKRootEntities.h>
#include <A3DSDKRepItems.h>
#include <A3DSDKTessellation.h>
#include <A3DSDKMarkup.h>
#include <A3DSDKGlobalData.h>
#include <A3DSDKTexture.h>
#include <A3DSDKMisc.h>
#include <A3DSDKGeometryCrv.h>
#include <A3DSDKGeometrySrf.h>
#include <A3DSDKTopology.h>
#undef A3D_API
}

HMODULE A3DPRCLoadLibrary()
{
	HMODULE hModuleA3DPRCSDK;

	wchar_t acFilePath[MAX_PATH];
	GetModuleFileNameW(NULL, acFilePath, MAX_PATH);
	wchar_t* backslash = wcsrchr(acFilePath, L'\\');

	if (backslash)
		acFilePath[backslash - acFilePath] = 0;

	wcscat(acFilePath, L"\\A3DLIB.dll");

#ifdef UNICODE 
	hModuleA3DPRCSDK = LoadLibraryExW(acFilePath, NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
#else
	hModuleA3DPRCSDK = LoadLibraryExA(acFilePath, NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
#endif
	if (hModuleA3DPRCSDK)
		return hModuleA3DPRCSDK;

	return NULL;
}

void A3DPRCUnloadLibrary(HMODULE hModule)
{
	FreeLibrary(hModule);
}


/////////////////////////////////////////////////////////////////////////
//
// Build a PRC file on the fly
//
//////////////////////////////////////////////////////////////////////////
A3DSurfCylinder* createSurface(void)
{
	A3DSurfCylinder* pp = NULL;
	A3DSurfCylinderData sData;
	A3D_INITIALIZE_DATA(sData);
	A3D_INITIALIZE_DATA(sData.m_sParam);
	A3D_INITIALIZE_DATA(sData.m_sTrsf);

	sData.m_dRadius = 10.0;
	// A 40 mm high closed cylinder (360�) 
	// Make the cylinder parameterized between -1 and +1 in both directions ((0, 0) is the center)
	// The sUVDomain indicates the domain you want to use
	// Coefficients have to be set so that computed parameters are defined inside classical parameterization for cylinders [0.0, 360.0]
	sData.m_sParam.m_sUVDomain.m_sMin.m_dX = -1.0;
	sData.m_sParam.m_sUVDomain.m_sMin.m_dY = -1.0;
	sData.m_sParam.m_sUVDomain.m_sMax.m_dX = 1.0;
	sData.m_sParam.m_sUVDomain.m_sMax.m_dY = 1.0;
	A3DDouble dX = sData.m_sParam.m_sUVDomain.m_sMax.m_dX - sData.m_sParam.m_sUVDomain.m_sMin.m_dX;
	A3DDouble dY = sData.m_sParam.m_sUVDomain.m_sMax.m_dY - sData.m_sParam.m_sUVDomain.m_sMin.m_dY;
	sData.m_sParam.m_dUCoeffA = 360.0/dX;
	sData.m_sParam.m_dUCoeffB = 360.0/dX * sData.m_sParam.m_sUVDomain.m_sMin.m_dX;
	sData.m_sParam.m_dVCoeffA = 40.0/dY;
	sData.m_sParam.m_dVCoeffB = 40.0/dY * sData.m_sParam.m_sUVDomain.m_sMin.m_dY;
	sData.m_sParam.m_bSwapUV = FALSE;
	sData.m_sTrsf.m_ucBehaviour = kA3DTransformationIdentity;

	ASInt32 iRet = A3DSurfCylinderCreate(&sData, &pp);

	return pp;
}

A3DCrvNurbs* createCircle(A3DDouble radius)
{
	A3DCrvNurbs* pp = NULL;
	A3DCrvNurbsData sData;
	A3D_INITIALIZE_DATA(sData);
	double dSqrt2_2 = sqrt(2.0) / 2.0;

	/****************************************************************************
		Definition of a NURBs representing a circle in the parametric space:
		   Radius must be comprised between 0 and 1
		   Degree = 2, Dimension = 2
			9  control points (first and last are identical)
			9  weights
			12 knots

                 3         |2        1
                  +--------+--------+
                  |        |        |
                  |        |        |
                 4|        |        |0
                --+--------+--------+-- 
                  |        |        |8
                  |        |        |
                  |        |        |
                  +--------+--------+
                 5         6         7

	****************************************************************************/
	double adCtrlPoints[18] = 
	{
		 1.0,  0.0, // 0
		 1.0,  1.0, // 1
		 0.0,  1.0, // 2
		-1.0,  1.0, // 3
		-1.0,  0.0, // 4
		-1.0, -1.0, // 5
		 0.0, -1.0, // 6
		 1.0, -1.0, // 7
		 1.0,  0.0  // 8
	};
	double adWeights[9] =
	{
		1.0,        // 0
		dSqrt2_2,	// 1
		1.0,			// 2
		dSqrt2_2,	// 3
		1.0,			// 4
		dSqrt2_2,	// 5
		1.0,			// 6
		dSqrt2_2,	// 7
		1.0			// 8
	};
	double adKnots[12]  =
	{
		0.0, 0.0, 0.0,
		0.25, 0.25,
		0.50, 0.50,
		0.75, 0.75,
		1.00, 1.00, 1.00
	};

	A3DVector3dData asControlPoints[9];
	ASInt32 i;
	for (i = 0; i < 9; i++)
		A3D_INITIALIZE_DATA(asControlPoints[i]);
	for (i = 0; i < 9; i++) {
		asControlPoints[i].m_dX = adCtrlPoints[i * 2] * radius;
		asControlPoints[i].m_dY = adCtrlPoints[i * 2 + 1] * radius;
		asControlPoints[i].m_dZ = 0;
	}

	sData.m_uiDimension = 2;
	sData.m_bRational = TRUE;
	sData.m_uiDegree = 2;
	sData.m_uiCtrlSize = 9;
	sData.m_pCtrlPts = asControlPoints;
	sData.m_uiWeightSize = 9;
	sData.m_pdWeights = adWeights;
	sData.m_uiKnotSize = 12;
	sData.m_pdKnots = adKnots;
	sData.m_eCurveForm = kA3DBSplineCurveFormCircularArc;
	sData.m_eKnotType = kA3DKnotTypeUnspecified;


	ASInt32 iRet = A3DCrvNurbsCreate(&sData, &pp);

	return pp;
}

A3DTopoEdge* createTopoEdge(void)
{
	A3DTopoEdge* pp = NULL;
	A3DTopoEdgeData sData;
	A3D_INITIALIZE_DATA(sData);

	ASInt32 iRet = A3DTopoEdgeCreate(&sData, &pp);

	return pp;
}

A3DTopoCoEdge* createTopoCoEdge(A3DCrvBase* p)
{
	A3DTopoCoEdge* pp = NULL;

	A3DTopoCoEdge* q = createTopoEdge();
	if (p != NULL && q != NULL) {
		A3DTopoCoEdgeData sData;
		A3D_INITIALIZE_DATA(sData);
		sData.m_pUVCurve = p;
		sData.m_pEdge = q;
		sData.m_ucOrientationWithLoop = 1;	
		sData.m_ucOrientationUVWithLoop = 1; 
		ASInt32 iRet = A3DTopoCoEdgeCreate(&sData, &pp);
	}

	return pp;
}

A3DTopoLoop* createTopoLoop(A3DDouble radius)
{
	A3DTopoLoop* pp = NULL;
	A3DTopoLoopData sData;
	A3D_INITIALIZE_DATA(sData);

	A3DCrvBase* p = createCircle(radius);
	A3DTopoCoEdge* q = createTopoCoEdge(p);

	sData.m_ppCoEdges = &q;
	sData.m_uiCoEdgeSize = 1;
	sData.m_ucOrientationWithSurface = 1;

	ASInt32 iRet = A3DTopoLoopCreate(&sData, &pp);

	return pp;
}

A3DTopoFace* createTopoFace(void)
{
	A3DTopoFace* pp = NULL;
	A3DTopoFaceData sData;
	A3D_INITIALIZE_DATA(sData);

	sData.m_pSurface = createSurface();

	A3DDouble outerradius = 0.5;
	A3DDouble innerradius = 0.4;
	A3DTopoLoop* loops[2];
	loops[0] = createTopoLoop(outerradius);
	loops[1] = createTopoLoop(innerradius);

	sData.m_ppLoops = loops;
	sData.m_uiLoopSize = 2;
	sData.m_uiOuterLoopIndex = 0;

	ASInt32 iRet = A3DTopoFaceCreate(&sData, &pp);

	return pp;
}

A3DTopoShell* createTopoShell(void)
{
	A3DTopoShell* pp = NULL;

	A3DTopoFace* p = createTopoFace();
	if (p != NULL) {
		A3DTopoShellData sData;
		A3D_INITIALIZE_DATA(sData);

		ASUns8 orient = 1;
		sData.m_bClosed = FALSE;
		sData.m_ppFaces = &p;
		sData.m_uiFaceSize = 1;
		sData.m_pucOrientationWithShell = &orient;

		ASInt32 iRet = A3DTopoShellCreate(&sData, &pp);
	}

	return pp;
}

A3DTopoConnex* createTopoConnex(void)
{
	A3DTopoConnex* pp = NULL;

	A3DTopoShell* p = createTopoShell();
	if (p != NULL) {
		A3DTopoConnexData sData;
		A3D_INITIALIZE_DATA(sData);

		sData.m_ppShells = &p;
		sData.m_uiShellSize = 1;

		ASInt32 iRet = A3DTopoConnexCreate(&sData, &pp);
	}

	return pp;
}

A3DTopoBrepData* createTopoBrep(void)
{
	A3DTopoBrepData* pp = NULL;

	A3DTopoConnex* p = createTopoConnex();
	if (p != NULL) {
		A3DTopoBrepDataData sData;
		A3D_INITIALIZE_DATA(sData);

		sData.m_ppConnexes = &p;
		sData.m_uiConnexSize = 1;

		ASInt32 iRet = A3DTopoBrepDataCreate(&sData, &pp);
	}

	return pp;
}

A3DRiRepresentationItem* createRIBrep(void)
{
	A3DRiBrepModel* pp = NULL;

	A3DTopoBrepData* p = createTopoBrep();
	if (p != NULL) {
		A3DRiBrepModelData sData;
		A3D_INITIALIZE_DATA(sData);

		sData.m_pBrepData = p;
		sData.m_bSolid = FALSE;

		ASInt32 iRet = A3DRiBrepModelCreate(&sData, &pp);
	}

	return pp;
}

A3DAsmPartDefinition* createPart(void)
{
	A3DAsmPartDefinition* pp = NULL;

	A3DRiRepresentationItem* p = createRIBrep();
	if (p != NULL) {
		A3DAsmPartDefinitionData sData;
		A3D_INITIALIZE_DATA(sData);

		sData.m_uiRepItemsSize = 1;
		sData.m_ppRepItems = &p;

		ASInt32 iRet = A3DAsmPartDefinitionCreate(&sData, &pp);
	}

	return pp;
}

// write attributes
A3DVoid setAttributes(A3DEntity* p)
{
	A3DMiscAttribute* pAttr[3];

	A3DMiscSingleAttributeData Single;
	A3D_INITIALIZE_DATA(Single);

	Single.m_eType = kA3DModellerAttributeTypeString;
	Single.m_pcTitle = "Title";
	Single.m_pcData = "Simple Brep building demonstration";

	A3DMiscAttributeData sAttribs;
	A3D_INITIALIZE_DATA(sAttribs);

	sAttribs.m_pcTitle = Single.m_pcTitle;
	sAttribs.m_pSingleAttributesData = &Single;
	sAttribs.m_uiSize = 1;
	ASInt32 iRet = A3DMiscAttributeCreate(&sAttribs, &pAttr[0]);

	Single.m_pcTitle = "Author";
	Single.m_pcData = "Acrobat SDK";
	sAttribs.m_pcTitle = Single.m_pcTitle;
	iRet = A3DMiscAttributeCreate(&sAttribs, &pAttr[1]);

	Single.m_pcTitle = "Company";
	Single.m_pcData = "Adobe Systems Incorporated";
	sAttribs.m_pcTitle = Single.m_pcTitle;
	iRet = A3DMiscAttributeCreate(&sAttribs, &pAttr[2]);

	A3DRootBaseData sRootData;
	A3D_INITIALIZE_DATA(sRootData);

	sRootData.m_pcName = "Trimmed surface";
	sRootData.m_ppAttributes = pAttr;
	sRootData.m_uiSize = 3;
	iRet = A3DRootBaseSet(p, &sRootData);
}

A3DAsmProductOccurrence* createOccurence(void)
{
	A3DAsmProductOccurrence* pp = NULL;

	A3DAsmPartDefinition* p = createPart();
	if (p != NULL) {
		A3DAsmProductOccurrenceData sData;
		A3D_INITIALIZE_DATA(sData);

		sData.m_pPart = p;

		ASInt32 iRet = A3DAsmProductOccurrenceCreate(&sData, &pp);
		if(iRet == A3D_SUCCESS)
			setAttributes(pp);
	}

	return pp;
}

// create model file
A3DAsmModelFile* createModel(void)
{
	A3DAsmModelFile* pp = NULL;

	A3DAsmProductOccurrence* p = createOccurence();
	if (p != NULL) {
		A3DAsmModelFileData sData;
		A3D_INITIALIZE_DATA(sData);

		sData.m_uiPOccurrencesSize = 1;
		sData.m_dUnit = 1.0;
		sData.m_ppPOccurrences = &p;

		ASInt32 iRet = A3DAsmModelFileCreate(&sData, &pp);
	}

	return pp;
}

// write model file into physical file
A3DVoid createPrcFile(const A3DUTF8Char* pcPRCName)
{
	A3DAsmModelFile* p = createModel();
	if (p != NULL) {
		if (_access(pcPRCName, 00) != -1)
			_unlink(pcPRCName);

		/* Save the PRC File */
		A3DAsmModelFileWriteParametersData sParametersData;
		A3D_INITIALIZE_DATA(sParametersData);
		sParametersData.m_eFormatChoice = kA3DPRC;

		ASInt32 iRet = A3DAsmModelFileWriteToFile(p, &sParametersData, pcPRCName);
		A3DAsmModelFileDelete(p);
	}
}


//////////////////////////////////////////////////////////////////////////
//
//	build the apperance stream in its AP entry that has a normal appearance 
//	(the N entry) for the 3D annotation
//
//////////////////////////////////////////////////////////////////////////
PDDoc Create1PageDocWithWatermark(void)
{
	// create new document
	PDDoc pdDoc = PDDocCreate();

	// Set the media box of the new document pages
	ASFixedRect mediaBox = {fixedZero, Int16ToFixed(792), Int16ToFixed(612), fixedZero};

	// Create page with the mediabox and insert as first page
	PDPage pdPage = PDDocCreatePage(pdDoc, PDBeforeFirstPage, mediaBox);

	ASText astextSrcText = ASTextFromScriptText("Import To PRC", kASRomanScript);

	PDColorValueRec pdColorValue = {PDDeviceRGB, {fixedZero, fixedZero, fixedOne, fixedZero}};

	PDDocWatermarkTextParamsRec pdDocWatermarkTextParamsRec;
	memset(&pdDocWatermarkTextParamsRec, 0, sizeof(pdDocWatermarkTextParamsRec));
	pdDocWatermarkTextParamsRec.size = sizeof(PDDocAddWatermarkParamsRec);
	pdDocWatermarkTextParamsRec.srcText     = astextSrcText;
	pdDocWatermarkTextParamsRec.textAlign   = kPDHorizCenter; 
	pdDocWatermarkTextParamsRec.pdeFont     = NULL; // use sysFontName only
	pdDocWatermarkTextParamsRec.sysFontName = ASAtomFromString("Courier");
	pdDocWatermarkTextParamsRec.fontSize    = 20.0f;
	pdDocWatermarkTextParamsRec.color       = pdColorValue; 

	PDPageRange pdPageRange = {0, 0, PDAllPages};

	PDDocAddWatermarkParamsRec  pdDocAddWatermarkParamsRec;
	memset(&pdDocAddWatermarkParamsRec, 0, sizeof(pdDocAddWatermarkParamsRec));
	pdDocAddWatermarkParamsRec.size = sizeof(PDDocAddWatermarkParamsRec);
	pdDocAddWatermarkParamsRec.targetRange = pdPageRange;
	pdDocAddWatermarkParamsRec.fixedPrint = false;
	pdDocAddWatermarkParamsRec.zOrderTop = true; // false for background
	pdDocAddWatermarkParamsRec.showOnScreen = true;
	pdDocAddWatermarkParamsRec.showOnPrint = true;
	pdDocAddWatermarkParamsRec.horizAlign = kPDHorizCenter;
	pdDocAddWatermarkParamsRec.vertAlign = kPDVertCenter;
	pdDocAddWatermarkParamsRec.horizValue = 0.0f;
	pdDocAddWatermarkParamsRec.vertValue = 1.0f;
	pdDocAddWatermarkParamsRec.percentageVals = false;
	pdDocAddWatermarkParamsRec.scale = 1.0f;
	pdDocAddWatermarkParamsRec.rotation = 0.0f;
	pdDocAddWatermarkParamsRec.opacity = 1.0f;
	pdDocAddWatermarkParamsRec.progMonData = NULL;
	pdDocAddWatermarkParamsRec.cancelProcData = NULL;
	pdDocAddWatermarkParamsRec.progMon = NULL;
	pdDocAddWatermarkParamsRec.cancelProc = NULL;
	PDDocAddWatermarkFromText(pdDoc, &pdDocWatermarkTextParamsRec, &pdDocAddWatermarkParamsRec);

	ASTextDestroy(astextSrcText);

	return pdDoc;
}

CosObj ExtractFormXObj(PDDoc pdDoc)
{
	CosObj cosObj = CosNewNull();
	ASInt32 i, numElems, pdeType, pdeType2, numSubElems;
	PDEContent pdeContent = NULL, pdeContent2 = NULL;
	PDPage pdPage = NULL;
	PDEElement pdeElement, pdeSubElem;
	PDEForm pdeForm;

DURING
	// go through the first page to find the Form XObject
	pdPage = PDDocAcquirePage(pdDoc, 0);
	pdeContent = PDPageAcquirePDEContent(pdPage, 0);
	numElems = PDEContentGetNumElems(pdeContent);
	for (i = 0; i < numElems; i++) {
		pdeElement = PDEContentGetElem(pdeContent, i); 
		pdeType = PDEObjectGetType((PDEObject) pdeElement);

		// if found, get its Cos object
		if(pdeType == kPDEForm) {
			pdeForm = (PDEForm)pdeElement;
			PDEFormGetCosObj (pdeForm, &cosObj);
			if(!CosObjEqual(cosObj, CosNewNull()))
				break;
		} else if(pdeType == kPDEContainer) {
			ASBool bFound = false;
			pdeContent2 = PDEContainerGetContent ((PDEContainer) pdeElement);
			numSubElems = PDEContentGetNumElems (pdeContent2);
			for (i = 0; i < numSubElems; i++){
				pdeSubElem = PDEContentGetElem(pdeContent2, i);
				pdeType2 = PDEObjectGetType((PDEObject) pdeSubElem);

				// if found, get its Cos object
				if(pdeType2 == kPDEForm) {
					pdeForm = (PDEForm) pdeSubElem;
					PDEFormGetCosObj (pdeForm, &cosObj);
					if(!CosObjEqual(cosObj, CosNewNull())){
						bFound = true;
						break;
					}
				}
			}
			if(bFound)
				break;
		}
	}
HANDLER
	ASInt32 iErrCode = ASGetExceptionErrorCode();
	char errBuf[A3D_MAX_BUFFER];
	memset(errBuf, 0, A3D_MAX_BUFFER);
	ASGetErrorString(iErrCode, errBuf, A3D_MAX_BUFFER);
	if(strlen(errBuf))
		AVAlertNote(errBuf);
	else
		AVAlertNote("Unknown error");
END_HANDLER
	
	if (pdeContent != NULL)
		PDPageReleasePDEContent(pdPage, 0);
	if (pdPage != NULL)
		PDPageRelease(pdPage);

	// return COS object of the PDEForm XObject
	return cosObj;
}

CosObj BuildNEntry(PDDoc pdDoc)
{
	PDDoc newDoc = Create1PageDocWithWatermark();
	if (!newDoc) {
		return CosNewNull();
	}

	CosObj formXObj = ExtractFormXObj(newDoc);

	PDDocClose(newDoc);

	return CosObjCopy(formXObj, PDDocGetCosDoc(pdDoc), true);
}

//////////////////////////////////////////////////////////////////////////
//
//	insert prc file into 3D annot
//
//////////////////////////////////////////////////////////////////////////

A3DVoid addPrcToAnnot(PDDoc pdDoc, PDPage thePage, PDAnnot theAnnot,
						const A3DUTF8Char* prcName, const A3DUTF8Char* jsName)
{
	//	Insert PRC data
	ASFile prcFile = NULL;
	ASPathName prcPathName = ASFileSysCreatePathName(NULL, ASAtomFromString("Cstring"), prcName, 0);
	ASFileSysOpenFile(ASGetDefaultFileSys(), prcPathName, ASFILE_READ, &prcFile);
	ASFileSysReleasePath(ASGetDefaultFileSys(), prcPathName);

	// create a new stream and set it under 3DD key in the annotation dictionary
	CosObj cosAnnot = PDAnnotGetCosObj(theAnnot);
	CosDoc cosDoc = CosObjGetDoc(cosAnnot);
	ASStm prcStm = ASFileStmRdOpen(prcFile, 0);
	CosObj stm3D = CosNewStream(cosDoc, true, prcStm, 0, false, CosNewNull(), CosNewNull(), -1);
	ASStmClose(prcStm);
	ASFileClose(prcFile);

	// create a new stream and set it under 3DD key in the annotation dictionary
	CosObj attrObj = CosStreamDict(stm3D);
	CosDictPutKeyString(attrObj, "Type", CosNewNameFromString(cosDoc, false, "3D"));
	CosDictPutKeyString(attrObj, "Subtype", CosNewName(cosDoc, false, ASAtomFromString("PRC")));
	CosDictPutKeyString(cosAnnot, "3DD", stm3D);

	// Insert JavaScript
	ASFile jsFile = NULL;
	ASPathName jsPathName = ASFileSysCreatePathName(NULL, ASAtomFromString("Cstring"), jsName, 0);
	ASFileSysOpenFile(ASGetDefaultFileSys(), jsPathName, ASFILE_READ, &jsFile);
	ASFileSysReleasePath(ASGetDefaultFileSys(), jsPathName);

	ASStm jsStm = ASFileStmRdOpen(jsFile, 0);
	CosObj jsCos = CosNewDict(cosDoc, false, 1);
	CosDictPutKeyString(jsCos, "Filter", CosNewNameFromString(cosDoc, false, "FlateDecode"));
	CosObj stm3Djscode = CosNewStream(cosDoc, true, jsStm, 0, true, jsCos, CosNewNull(), -1);
	CosDictPutKeyString(attrObj, "OnInstantiate", stm3Djscode);
	ASStmClose(jsStm);
	ASFileClose(jsFile);

	CosDictPutKeyString(cosAnnot, "P", PDPageGetCosObj(thePage));
	CosDictPutKeyString(cosAnnot, "Contents", CosNewString(cosDoc, false, "3D Model", strlen("3D Model")));

	//	Set optionnal activation
	CosObj activationDict = CosNewDict(cosDoc, false, 1);
	if(CosObjGetType(activationDict)== CosDict) {
		CosDictPutKeyString(cosAnnot, "3DA", activationDict);
		CosDictPutKeyString(activationDict, "DIS", CosNewName(cosDoc, false, ASAtomFromString("I")));
		CosDictPutKeyString(activationDict, "A", CosNewName(cosDoc, false, ASAtomFromString("PO")));
	}

	CosObj formXObj = BuildNEntry(pdDoc);
	if(CosObjCmp(formXObj, CosNewNull())) {
		// build the required key "AP"
		CosObj apprDict = CosNewDict(cosDoc, false, 1);
		CosDictPutKeyString(apprDict, "N", formXObj);
		CosDictPutKeyString(cosAnnot, "AP", apprDict);
	}

	PDTextAnnotSetOpen(theAnnot, true);
}


//////////////////////////////////////////////////////////////////////////
//
//	Add meta data informations to current doc
//
//////////////////////////////////////////////////////////////////////////

A3DVoid addMetaDataToDoc(PDDoc theDoc)
{
	// Producer 
	PDDocSetXAPMetadataProperty(theDoc,
		ASTextFromPDText("http://ns.adobe.com/pdf/1.3/"),
		ASTextFromPDText("pdf"),
		ASTextFromPDText("Producer"),
		ASTextFromPDText(A3D_DLL_COPYRIGHT));

	// Creator 
	PDDocSetXAPMetadataProperty(theDoc,
		ASTextFromPDText("http://ns.adobe.com/xap/1.0/"),
		ASTextFromPDText("xmp"),
		ASTextFromPDText("CreatorTool"),
		ASTextFromPDText(A3D_DLL_NAME));
} 

//////////////////////////////////////////////////////////////////////////
//
//	Build a js file on the fly
//
//////////////////////////////////////////////////////////////////////////

A3DVoid buildJsFile(const A3DUTF8Char* jsName)
{
	FILE* jsFile = fopen(jsName, "w");
	if(jsFile) {
		A3DVector3dData vUp = {0,0.57735, 0.57735, 0.57735};
		A3DDouble roll = 0;

		fprintf(jsFile, "runtime.overrideViewChange = true\n");
		fprintf(jsFile, "activeCamera = scene.cameras.getByIndex( 0 );\n");
		fprintf(jsFile, "bounds = scene.computeBoundingBox();\n");
		fprintf(jsFile, "bounds.center = bounds.max.blend( bounds.min, .5 );\n");
		fprintf(jsFile, "diag = bounds.max.subtract( bounds.min ).length;\n");
		fprintf(jsFile, "dist =(diag * 0.5) / Math.tan(0.6 * activeCamera.fov);\n");
		fprintf(jsFile, "activeCamera.position.set3(bounds.center.x, bounds.center.y - dist, bounds.center.z);\n");
		fprintf(jsFile, "activeCamera.targetPosition.set(bounds.center);\n");
		fprintf(jsFile, "activeCamera.up.set3(%g, %g, %g);\n", vUp.m_dX, vUp.m_dY, vUp.m_dZ);
		fprintf(jsFile, "activeCamera.roll = %g;\n",roll);
		fprintf(jsFile, "scene.lightScheme = scene.LIGHT_MODE_HEADLAMP;\n");
		fprintf(jsFile, "scene.renderMode = scene.RENDER_MODE_SHADED_SOLID_WIRFRAME;\n");
		fprintf(jsFile, "\n");
		fprintf(jsFile, "\n");
		fprintf(jsFile, "\n");
		fclose(jsFile);
	}
}

//////////////////////////////////////////////////////////////////////////
//
//	Main part of the application
//
//////////////////////////////////////////////////////////////////////////

ACCB1 void ACCB2 MyPluginCommand(void *clientData)
{
	const A3DUTF8Char* prcName = "C:\\tmp.prc";
	const A3DUTF8Char* jsName = "C:\\tmp.js";
	PDDoc theDoc = PDDocCreate();
	ASFixedRect theBox = {fixedZero, ASInt32ToFixed(612), ASInt32ToFixed(792), fixedZero};
	PDPage thePage = PDDocCreatePage(theDoc, PDBeforeFirstPage, theBox);
	AVDoc avDoc = AVDocOpenFromPDDoc(theDoc, NULL);

	addMetaDataToDoc(theDoc);

	HMODULE hModuleA3DPRCSDK = A3DPRCLoadLibrary();

	if (!hModuleA3DPRCSDK) {
		AVAlertNote("Failed to load A3DLIB.dll!");
		_unlink(prcName);
		_unlink(jsName);
		return;
	}

	A3DPRCFunctionPointersInitialize(hModuleA3DPRCSDK);

	ASInt32 iMajorVersion = 0, iMinorVersion = 0; 
	A3DDllGetVersion(&iMajorVersion, &iMinorVersion);

	ASInt32 iErr = A3D_SUCCESS; 
	if(iMajorVersion != A3D_DLL_MAJORVERSION) 
		iErr = A3D_ERROR;
	else if(iMinorVersion < A3D_DLL_MINORVERSION) 
		iErr = A3D_ERROR;

	if(iErr == A3D_SUCCESS) {
		ASInt32 iRet = A3DDllInitialize(A3D_DLL_MAJORVERSION, A3D_DLL_MINORVERSION);
		if(iRet == A3D_SUCCESS) {
			buildJsFile(jsName);
			createPrcFile(prcName);

			ASFixedRect theAnnotBox = {ASInt32ToFixed(108), ASInt32ToFixed(612 - 108),
				ASInt32ToFixed(792 - 108), ASInt32ToFixed(108)};
			PDAnnot theAnnot = PDPageAddNewAnnot(thePage, -2, ASAtomFromString("3D"), &theAnnotBox);
			PDAnnotSetFlags(theAnnot, pdAnnotPrint | pdAnnotReadOnly);

			//	load prc stream inside 3d annotation
			addPrcToAnnot(theDoc, thePage, theAnnot, prcName, jsName);

			_unlink(prcName);
			_unlink(jsName);
			A3DDllTerminate();
			A3DPRCUnloadLibrary(hModuleA3DPRCSDK);
		} else {
			char strMsg[128];
			sprintf(strMsg, "A3DDllInitialize returned %d\n", iRet);
			AVAlertNote(strMsg); 
			_unlink(prcName);
			_unlink(jsName);
			A3DPRCUnloadLibrary(hModuleA3DPRCSDK);
		}
	} else {
		AVAlertNote("PRC library version on this system is not supported!");
		_unlink(prcName);
		_unlink(jsName);
		A3DPRCUnloadLibrary(hModuleA3DPRCSDK);
	}

	PDPageRelease(thePage);
	return;
}

