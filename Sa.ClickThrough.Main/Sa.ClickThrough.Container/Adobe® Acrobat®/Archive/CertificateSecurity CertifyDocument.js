// Copyright � 2009 HSB Solomon Associates LLC
// Solomon Associates
// Two Galleria Tower
// 13455 Noel Road, Suite 1500
// Dallas, Texas 75240

// Prefixes:
// sol - Solomon
// cli - Client (Company)
// ref - Refnum (Facility)
// pfx - Digital ID (Private Key)

var curDoc = this;

// Certificate (Public key) paths and file names for document encryption (security)
var solPath = "/C/Documents and Settings/rrh/Desktop/Sample/";
var cliPath = "/C/Documents and Settings/rrh/Desktop/Sample/";
var refPath = "/C/Documents and Settings/rrh/Desktop/Sample/";

var solCertFile = "TESTCERT.cer";
var cliCertFile = curDoc.info.Refnum + ".cer";
var refCertFile = curDoc.info.Refnum + ".cer";

// Digital ID (Private key) path, file name, and password for document certification
var pfxPath = "/Dallas1/DATA/COMMONP/SA ClickThrough Agreement/Adobe� Acrobat�/";
var pfxCertFile = "TESTCERT.pfx";
var pfxPassWord = "enigma21";


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Encrypt Document for Recipients (Certificate / Public key)

// Get certificates for each recipients
var solCert = security.importFromFile({cType:"Certificate", cDIPath:solPath + solCertFile});
var cliCert = security.importFromFile({cType:"Certificate", cDIPath:cliPath + cliCertFile});
var refCert = security.importFromFile({cType:"Certificate", cDIPath:refPath + refCertFile});

// Set user entities for each recipient
var solEntity = [{defaultEncryptCert:solCert}];
var cliEntity = [{defaultEncryptCert:cliCert}];
var refEntity = [{defaultEncryptCert:refCert}];

// Set document permissions that correspond to each user entity
var solPermissions = 
{
    allowAll: false
    , allowSecuritySettings: false
    , allowAccessibility: true                  // Available to screen readers
    , allowContentExtraction: true              // Copy and extract content
    , allowChanges: "editNotesFillAndSign"      // Make annotations, sign, etc
    , allowPrinting: "highQuality"              // Print in high quality
};
var cliPermissions = 
{
    allowAll: false
    , allowSecuritySettings: false
    , allowAccessibility: false
    , allowContentExtraction: false
    , allowChanges: "none"
    , allowPrinting: "lowQuality"
};
var refPermissions = 
{
    allowAll: false
    , allowSecuritySettings: false
    , allowAccessibility: false
    , allowContentExtraction: false
    , allowChanges: "none"
    , allowPrinting: "lowQuality"
};

// Encrypt document for the recipients
curDoc.encryptForRecipients(
{
    oGroups:
    [
        {userEntities: solEntity, permissions: solPermissions}
        , {userEntities: cliEntity, permissions: cliPermissions}
        , {userEntities: refEntity, permissions: refPermissions}
    ],
    bMetaData: true,
    bUI: false
});


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Certify Document by Solomon (Digital ID / Private key)

// Add signature field
var f = curDoc.addField({cName:"CertifyDocument", cFieldType:"signature", nPageNum:0, oCoords:[0,0,0,0]});
f.display = display.hidden;

// Set the Modification Detection and Prevention (MDP) property; this prohibits changes to the document
f.signatureSetSeedValue({mdp: "allowNone"});

// Set the prepared for string
if (curDoc.info.Refnum != "")
{
    var strPreparedFor = curDoc.info.Client + " (" + curDoc.info.Refnum + ")"
}
else
{
    var strPreparedFor = curDoc.info.Client
};

// Create the signature
var s = (
{
    password: pfxPassWord
    , reason: "This document created specifically for " + strPreparedFor + "."
    , location: "HSB Solomon Associates LLC, Dallas, Texas, USA"
});

// Create the security handler that will sign the document
var sh = security.getHandler("Adobe.PPKLite", false);
sh.login(pfxPassWord, pfxPath + pfxCertFile);
sh.signInvisible = true;
sh.signAuthor = true;

// Change the document path to allow different suffixes
var strPath = curDoc.path.replace(".pdf", "");

// Save the document to apply encryption. This must be completed in code to properly
// set the encryption and security parameters;  One must also save the document before
// certifying the document.
curDoc.saveAs(strPath + " (Encrypted).pdf");

// Certify the document
f.signatureSign(
{
    oSig: sh
    , oInfo: s
    , cDIPath: strPath + " (Certified).pdf"
    , bUI: false
    , cLegalAttest:"Information in this document is proteced by copyright, patents, and contracts with " + strPreparedFor + "."
});

sh.logout();
