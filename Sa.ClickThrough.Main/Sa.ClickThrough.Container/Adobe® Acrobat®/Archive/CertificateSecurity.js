// Copyright � 2009 HSB Solomon Associates LLC
// Solomon Associates
// Two Galleria Tower
// 13455 Noel Road, Suite 1500
// Dallas, Texas 75240

// Prefixes:
// sol - Solomon
// cli - Client (Company)
// ref - Refnum (Facility)

var curDoc = this;

// Certificate (Public key) paths and file names for document encryption (security)
var solPath = "/C/Documents and Settings/rrh/Desktop/Sample/";
var cliPath = "/C/Documents and Settings/rrh/Desktop/Sample/";
var refPath = "/C/Documents and Settings/rrh/Desktop/Sample/";

var solCertFile = "TESTCERT.cer";
var cliCertFile = curDoc.info.Refnum + ".cer";
var refCertFile = curDoc.info.Refnum + ".cer";


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Encrypt Document for Recipients (Certificate / Public key)

// Get certificates for each recipients
var solCert = security.importFromFile({cType:"Certificate", cDIPath:solPath + solCertFile});
var cliCert = security.importFromFile({cType:"Certificate", cDIPath:cliPath + cliCertFile});
var refCert = security.importFromFile({cType:"Certificate", cDIPath:refPath + refCertFile});

// Set user entities for each recipient
var solEntity = [{defaultEncryptCert:solCert}];
var cliEntity = [{defaultEncryptCert:cliCert}];
var refEntity = [{defaultEncryptCert:refCert}];

// Set document permissions that correspond to each user entity
var solPermissions = 
{
    allowAll: false
    , allowSecuritySettings: false
    , allowAccessibility: true                  // Available to screen readers
    , allowContentExtraction: true              // Copy and extract content
    , allowChanges: "editNotesFillAndSign"      // Make annotations, sign, etc
    , allowPrinting: "highQuality"              // Print in high quality
};
var cliPermissions = 
{
    allowAll: false
    , allowSecuritySettings: false
    , allowAccessibility: false
    , allowContentExtraction: false
    , allowChanges: "none"
    , allowPrinting: "lowQuality"
};
var refPermissions = 
{
    allowAll: false
    , allowSecuritySettings: false
    , allowAccessibility: false
    , allowContentExtraction: false
    , allowChanges: "none"
    , allowPrinting: "lowQuality"
};

// Encrypt document for the recipients
curDoc.encryptForRecipients(
{
    oGroups:
    [
        {userEntities: solEntity, permissions: solPermissions}
        , {userEntities: cliEntity, permissions: cliPermissions}
        , {userEntities: refEntity, permissions: refPermissions}
    ],
    bMetaData: true,
    bUI: false
});

// Change the document path to allow different suffixes
var strPath = curDoc.path.replace(".pdf", "");

// Save the document to apply encryption. This must be completed in code to properly
// set the encryption and security parameters;  One must also save the document before
// certifying the document.
curDoc.saveAs(strPath + " (Encrypted).pdf");
