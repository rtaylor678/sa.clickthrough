// Copyright � 2009 HSB Solomon Associates LLC
// Solomon Associates
// Two Galleria Tower
// 13455 Noel Road, Suite 1500
// Dallas, Texas 75240

// Prefixes:
// pfx - Digital ID (Private Key)

var curDoc = this;

// Digital ID (Private key) path, file name, and password for document certification
var pfxPath = "/Dallas1/DATA/COMMONP/SA ClickThrough Agreement/Adobe� Acrobat�/";
var pfxCertFile = "TESTCERT.pfx";
var pfxPassWord = "enigma21";


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Certify Document by Solomon (Digital ID / Private key)

// Add signature field
var f = curDoc.addField({cName:"CertifyDocument", cFieldType:"signature", nPageNum:0, oCoords:[0,0,0,0]});
f.display = display.hidden;

// Set the Modification Detection and Prevention (MDP) property; this prohibits changes to the document
f.signatureSetSeedValue({mdp: "allowNone"});

// Set the prepared for string
if (curDoc.info.Refnum != "")
{
    var strPreparedFor = curDoc.info.Client + " (" + curDoc.info.Refnum + ")"
}
else
{
    var strPreparedFor = curDoc.info.Client
};

// Create the signature
var s = (
{
    password: pfxPassWord
    , reason: "This document created specifically for " + strPreparedFor + "."
    , location: "HSB Solomon Associates LLC, Dallas, Texas, USA"
});

// Create the security handler that will sign the document
var sh = security.getHandler("Adobe.PPKLite", false);
sh.login(pfxPassWord, pfxPath + pfxCertFile);
sh.signInvisible = true;
sh.signAuthor = true;

// Certify the document
f.signatureSign(
{
    oSig: sh
    , oInfo: s
    , cDIPath: strPath + " (Certified).pdf"
    , bUI: false
    , cLegalAttest:"Information in this document is proteced by copyright, patents, and contracts with " + strPreparedFor + "."
});

sh.logout();
